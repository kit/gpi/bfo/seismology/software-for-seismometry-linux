this is <README>
============================================================================
Files in subdirectory calex
===========================

Source code
-----------
calex.f
  in CONTENTS-ls-lR.txt:  62160 12. Feb 2013  calex.f
  Evaluates the electrical calibration of seismographs or filters from their
  response to arbitrary input signals (which must have been recorded).
  Identifies corner frequencies and damping coefficients where appropriate.
  Reads data in SEIFE and ASL format. The program also determines the poles
  and zeros of the transfer function. 

qcalex.f
  in CONTENTS-ls-lR.txt:  44235 11. Jul 2012  qcalex.f
  This version of CALEX includes quadratic and cubic nonlinearities
  the 'sub' and 'til' parameters are replaced by '2nd' and '3rd'.
  It, however, misses several features, which were implemented in calex.f

trical.f
  in CONTENTS-ls-lR.txt:  51437 22. Apr 22:32 trical.f
  A combination of TRIAX and CALEX, performing a simultaneous calibration of
  the XYZ output signals of a homogeneous-triaxial seismometer whose UVW
  calibration coils receive identical stimuli. Although such an input
  represents a vertical ground acceleration and a substantial output signal
  appears only at the Z output, small residual signals also appear at the X
  and Y outputs when the UVW sensors are not precisely equal. According to a
  method introduced by Peter Davis of UCSD they can be used to determine all
  transfer functions, both of the UVW sensors and the XYZ outputs, with the
  same accuracy as if the three components were separately calibrated. 

makecalexhelp.f
  A utility program which is used during program compilation only. It provides
  online usage information in the binary executable, which is taken from the
  source code automatically during compilation.

Shell scripts supporting the application
----------------------------------------
calex.sh
  A shell script, which runs calex based on a given control parameter file and
  subsequently produces a graphical display of signals as well as a summary
  report of the iterative optimization.

calexiterextract.awk
  An awk-script which extracts essential summary information from the textual
  output of calex.

calexoutextract.awk
  An awk-script which extracts numerical values of the iterative optimization
  process as reported in the textual output of calex. It can be used to
  display the convergence properties of the iterative process.

Example control files
---------------------
calex-pz.par
calex.par
calex1.par
trical.par
two-tone.par

Program description and documentation
-------------------------------------
EX_5.4.doc
  Exercise from the NMSOP (New Manual os Seismological Observatory Practice)

EX_5.4_rev1.doc
  Exercise from the NMSOP (New Manual os Seismological Observatory Practice),
  revised version.

calex.doc
  Users manual for CALEX.

trical.doc
trical.txt
  User instructions for TRICAL.

Toy examples
------------
data/Makefile
data/README
data/bpdeconv.gpt
data/calex.bp.par
data/rtein.f

----- END OF README ----- 
