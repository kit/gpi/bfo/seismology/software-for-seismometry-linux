
      PROGRAM CALEX12

C  System calibration using arbitrary test signals
C  Simulation using impulse-invariant recursive filters
C  Reference: H.W.Schuessler, A signalprocessing approach to simulation,
C  Frequenz 35 (1981), 174-184

c  This software is only applicable to calibration procedures where both
c  the stimulus and the response are digitally recorded. Since normally
c  both signals will be recorded with identical digitizers, the digitizer
c  response is irrelevant and will not be modelled.

c  According to Schuessler, the method can only be applied to systems
c  whose passband lies entirely within the Nyquist bandwidth. That is,
c  the impulse response of the system must have negligible energy outside
c  this bandwidth. Seismometers do in general not satisfy this condition,
c  especially when only the lower corner frequency is modelled so that
c  the seismometer appears as a high-pass filter. We therefore limit the 
c  bandwidth of the system numerically with a recursive filter whose 
c  corner period is specified by the alias parameter, and model the
c  real system plus the filter. Since the filter is known, its parameters
c  do not appear in the inversion as active parameters, and the user of
c  this program may not notice its presence except for a slight smoothing
c  of the synthetic output signal. To maintain visual consistency, the 
c  plottable input and output signals, calex.einf and calex.ausf, are also 
c  filtered with the anti-alias filter; however the inversion uses the 
c  original, unfiltered input signal.

c  It is strongly recommended to plot and inspect the signals
c  calex.einf, calex.ausf, calex.synt and calex.rest after each run.
c  calex.synt is the synthetic output signal and calex.rest is the
c  misfit. Under MS-Windows, the plot routine WINPLOT will automatically
c  produce a suitable plot. For details, see the program description
c  calex.doc .

c  The meaning of the 'amp' parameter is different for different experiments
c  (electrical or mechanical input, etc.). If a broadband-velocity seismometer
c  with the asymptotic (practically, mid-band) generator constant G is tested 
c  on a shake table that has a displacement transducer, two possible cases are: 

c  a) the displacement transducer has an electric output of D volts per meter
c     and its output signal is recorded with the same digitizer as the seismo- 
c     meter output. Then amp = G / D.
c  b) The displacement transducer is digital, or has a separate digitizer, with
c     a responsivity A counts per meter. The seismometer output is digitized 
c     with C counts per volt. Then amp = C * G / A.

c  Revisions:

c  04 Dec 1998
c  partial systems defined by keywords hp1, lp1, hp2, bp2, lp2
c  anti-alias-filtered 'ausg' signal written into file 'calex.ausf'
c  output in free format

c  22 Feb 1999
c  delay parameter alternatively renamed 'sub' and (ab)used for 
c  compensation of direct input-to-output coupling (for calibration 
c  of single-coil geophones in a half bridge).
c
c  06 Mar 2002
c  the delay parameter is alternatively used for the compensation of tilt
c  effects on a shake table.
c
c  12 Mar 2002
c  In place of using the delay parameter alternatively for the compensation
c  of galvanic coupling or tilt, the program now expects to find four 
c  parameters with fixed meaning: amp for the gain, del for the delay, sub 
c  for galvanic coupling, and til for the tilt. The sub parameter causes the 
c  fraction sub of the input signal to be subtracted from the output signal. 
c  The til parameter causes the twice-integrated signal to be added with 
c  amplitude til (actually this is done with the output signal, but could be 
c  done with the input signal as well, which would correspond better to the 
c  physical situation on a shake table).
c  The parameter file is no longer compatible with that of the original 
c  version. It must contain two additional lines with 'sub' and 'til' after 
c  the 'del' line. 
c
c  til is in units of microradians per millimeter of table motion provided 
c  that the motion of the table, the gravitational force coupled in by tilt, 
c  and the sensitive axis of the seismometer have the same direction. More 
c  generally, til is (tilt along the sensitive axis) per (table displacement
c  along the sensitive axis).
c
c  18 Sep 05
c  names of input and output files are read from calex.par
c  The .par file must contain two additional lines with the file names
c  before all other parameters, and is no longer compatible with
c  earlier versions. File names should be enclosed in simple quotes
c  (apostrophs) especially when they contain spaces or the line contains
c  comments after the file names. Lines starting with a space are ignored.
c
c  May 2006: ASL format is recognized. (Unreliably, as I found out later.)

c  July 2006: Name of parameter file can be entered as runstring.
c  Output file named after parameter file.
c
c  August 2007: poles and zeros are listed after the inversion.
c
c  October 2007: delay can be up to +-24*dt. The synthetic signal is computed
c  from sample ns1-24 to sample ns2+24 to allow for the time shift. DEL and
c  SUB can no longer be used at the same time.
c
c  October 2008: the old "spaghetti" version of subroutine mini was replaced
c  by an equivalent but more comprehensible code.
c
c  March - May 2009: several minor improvements. ns1 and ns2 are written into
c  the plot-parameter file so that only the modelled part of the signal is 
c  plotted. The output files contain howewer the whole signal.
c
c  March 2010: Reading ASL format did not work any more because the unreliable
c  FORTRAN free-format read statement was used. It was replaced by the reading
c  routine from SEIFE. Other programs had been updated earlier.
c
c----------------------------------------------------------------------
c
c  25.06.2012     increased resolution of output sample format to
c                 (5g15.7) in subroutine output (T.F.)
c
c----------------------------------------------------------------------
c
c  July 2012: CALEX can now handle zeros at nonzero frequencies. Each
c  zero (except at s=0) must be paired with a pole so that the two
c  together do not change the asymptotic behaviour of the rest of
c  transfer function. The corresponding subsystems are labelled in the
c  parameter file as pz1 (for one real pole-zero pair) and pz2 (for a
c  double pair characterized by a period and arbitrary damping). 
c
c  The option of setting m0=-1 to describe an integrator has been
c  discontinued. You can still include an integration by replacing a
c  first-order high-pass by a low-pass, a second-order high-pass by a
c  bandpass, a bandpass by a low-pass, or by introducing an additional
c  first-order low-pass with a very long corner period.
c 
c  Jan. 2013: Only a quadratic (rather than third-order) trend is removed.
c
c  the 'use' statement is compiler specific, and not required for Linux
c      use nas_systemc
c
c  11.10.2016:    support UNIX path names for eing and ausg
c
c  03.07.2020:    print final system parameters a second time
c                 with exponential format optionally (needed when
c                 calibrating response of air-lock at BFO)
c
c ----------------------------------------------------------------------
c
c definition of bp2 response
c --------------------------
c   if f is frequency in Hz then a filter stage
c  
c   bp2
c   per     period in seconds
c   dmp     damping as a fraction of critical damping
c  
c   in calex is defined by the response function
c  
c     T(f) = per*f/(1+2*dmp*i*per*f-per*per*f*f),
c  
c   where i is the imaginary unit.
c
c end of calex info.c
c ----------------------------------------------------------------------
c
      parameter (mpar=12,msys=36,ns=800000)
c  needs heap 22000 kBytes for ns=480000, 40000 for ns=800000
      implicit double precision (a-h,o-z)
      character endtxt*9, titel*72
      character*50 eing,ausg,nam3,nam4,pathname
      character*3 nam,typ,znam,ztyp,pnam
      logical pzmode
      dimension x(mpar)
      dimension ein(ns),aus(ns),sum(ns),einf(ns),sta(ns)
      common /aux/ step,finac,axi,qn,dt,ns1,ns2,mq,init,m
      common /par/ x0(msys),rho(msys),x00(mpar),r00(mpar),
     &  typ(msys),nam(mpar),pnam(msys)
c      common /zpar/ zx0(msys),zrho(msys),zx00(mpar),zr00(mpar),
c     &  ztyp(msys),znam(mpar),mz1,mz2,jp,jpm,jpole(msys)
      common /mode/ pzmode
      data zero,one/0.d0,1.d0/
      data endtxt /'converged'/
      data x/mpar*0.d0/
      data eing /'                                                  '/
      data ausg /'                                                  '/
      pzmode=.false.
      if(iargc().eq.0) then
        nam3='calex.par'
      else if(iargc().eq.1) then
        call getarg(1,nam3)
        if (nam3(1:4).eq.'info') then
          call calexinfo
          stop
        endif
      else
      write(6,*) 'calex must be called with 0 or 1 runstring parameter'
      endif 
      ipunkt=index(nam3,'.')
      nam4=nam3(1:ipunkt)//'out'
      open(3,file=nam3,err=999,status='old')


c  read header line and file names
      read(3,'(a)') titel
      if(titel(1:8).eq.'<pzmode>') pzmode=.true.
  201 read(3,'(a)') pathname
      if(pathname(1:1).eq.' '.or.pathname(1:1).eq.'') goto 201
      eing=pathname(1:index(pathname, ' ')-1)
  202 read(3,'(a)') pathname
      if(pathname(1:1).eq.' '.or.pathname(1:1).eq.'') goto 202
      ausg=pathname(1:index(pathname, ' ')-1)
      close(3)

C  read output (ausg)  and input (eing) signals, prefilter output signal
      open(4,file=nam4)
      write(6,*)
      write(6,*)
      write(4,'(/,1x,a,/)') '=======> '//titel
      write(6,'(/,1x,a,/)') '=======> '//titel
      init=-1
      mq=0
      n=ns
      call input(ausg,n,dt,ein,tma,tsa)

c  corner period and order of the anti-alias filter
      call inpar(nam3,m,maxit,qac,finac,init,dt,ns1,ns2)
      ns1=1
      ns2=n
c  note: aus is ignored in quadr because init<0
      call quadr(x,q,ein,aus,sum,einf,sta)
      do 28 i=1,n
   28   aus(i)=sum(i)
c  aus is now the recorded and anti-alias-filtered output signal
c  that is later fitted with the synthetics
      call output('calex.ausf','ausg with anti-alias',n,dt,aus,tma,tsa)
      ne=ns
      call input(eing,ne,dtein,ein,tme,tse)
      if(ne.ne.n) then
        write(6,*) 'the time series have different lengths - stop'
        stop
      endif
      if(dtein.ne.dt) then
        write(6,*) ' sampling of input and output data inconsistent'
        stop
      endif
      if(abs(60.*(tma-tme)+tsa-tse).gt.0.01) then
        write(6,*)
        write(6,*) 'WARNING: time tags of input and output data are inco
     &nsistent'
        write(6,*)
      endif
      ein0=ein(1)
      do 30 i=1,n
   30   ein(i)=ein(i)-ein0
      call quadr(x,qq,ein,aus,sum,einf,sta)
      ein0=0.d0
      do i=1,n
        ein0=ein0+sum(i)
      enddo
      ein0=ein0/n
      do 31 i=1,n
   31   einf(i)=sum(i)-ein0
      call output('calex.einf','eing with anti-alias',n,dt,einf,tme,tse)

C  prepare iteration, read start parameters

      iter=0
      step=one
      noimp=0
      init=0

      call inpar(nam3,m,maxit,qac,finac,init,dt,ns1,ns2)
      if(ns1.le.0) ns1=1
      if(ns2.le.0) ns2=n
      ns1=max(ns1,25)
      ns2=min(ns2,n-23)
      if(ns2.le.ns1) then
        write(6,'(a)') ' incorrect time window - check ns1, ns2'
        stop
      endif
        write(6,*)
        write(4,*)
        write(6,101) ns1,ns2
        write(4,101) ns1,ns2
  101   format(' data will be fitted from sample',i7,' to',i7)
        write(6,*)
        write(4,*)

c  start model

      qnorm=zero
      do 4 j=ns1,ns2
    4   qnorm=qnorm+aus(j)**2
      qn=qnorm
c  note: from now on (init=0) sum from quadr will be the misfit
      call quadr(x,q,ein,aus,sum,einf,sta)
      do 96 i=1,n
   96   sta(i)=zero
      do 97 i=ns1,ns2
   97   sta(i)=aus(i)-sum(i)
      write(6,98)
      write(4,98)
   98 format(' synthetics from start model:')
      call output('calex.synt','synth. for startmod.',n,dt,sta,tme,tse)
      write(6,99)
      write(4,99)
   99 format(' misfit from start model:')
      call output('calex.rest','misfit for startmod.',n,dt,sum,tme,tse)
      write(6,103) (nam(k),k=1,m)
      write(4,103) (nam(k),k=1,m)
  103 format(/' iter',9x,'RMS',5(9x,a3)/(17x,5(9x,a3)))
      write(6,104)
      write(4,104)
      if(m.le.5) then
        write(6,106) iter,dsqrt(q),(x00(k),k=1,m)
        write(4,106) iter,dsqrt(q),(x00(k),k=1,m)
      else
        write(6,104) iter,dsqrt(q),(x00(k),k=1,m)
        write(4,104) iter,dsqrt(q),(x00(k),k=1,m)
      endif
  104 format(i5,6(1x,f11.6)/(17x,5(1x,f11.6)))
  107 format(i5,6(1x,g11.6)/(17x,5(1x,g11.6)))
  106 format(i5,6(1x,f11.6))
      write(6,105) (r00(k),k=1,m)
      write(4,105) (r00(k),k=1,m)
  105 format((15x,'+-',5(1x,f11.6)))
      write(6,104)
      write(4,104)

C  iteration with the conjugate-gradient method

      do 2 iter=1,maxit

        qalt=q

        call congrd(x,q,gnorm,iter,ein,aus,sum,einf,sta)

        if(m.eq.5) then
          write(6,106) iter,dsqrt(q),(x(k),k=1,m)
          write(4,106) iter,dsqrt(q),(x(k),k=1,m)
        else
          write(6,104) iter,dsqrt(q),(x(k),k=1,m)
          write(4,104) iter,dsqrt(q),(x(k),k=1,m)
        endif

c  the iteration stops when in m steps, the fit was improved by less
c  than qac and the parameter vector was changed by less than finac. 
c  The rms error is relative to the rms amplitude of the output signal.
c  Parameter corrections are relative to the search interval, rho.

        noimp=noimp+1
        if((dsqrt(qalt)-dsqrt(q)).gt.qac.or.axi.gt.finac) noimp=0
        if(noimp.ge.m) goto 5
    2 continue

      endtxt='not conv.'
      iter=maxit

    5 write(6,104)
      write(4,104)
      write(6,104)
      write(4,104)
      write(6,109)
      write(4,109)
  109 format(' final system parameters:')
      write(6,103) (nam(k),k=1,m)
      write(4,103) (nam(k),k=1,m)
      write(6,104) iter,dsqrt(q),(x00(k)+r00(k)*x(k),k=1,m)
      write(4,104) iter,dsqrt(q),(x00(k)+r00(k)*x(k),k=1,m)
      write(6,107) iter,dsqrt(q),(x00(k)+r00(k)*x(k),k=1,m)
      write(4,107) iter,dsqrt(q),(x00(k)+r00(k)*x(k),k=1,m)
      write(6,108) mq,endtxt
      write(4,108) mq,endtxt
  108 format(/' QUAD called',i5,' times: ',a)

c      if(iter.lt.maxit) then
      do 196 i=1,n
  196   sta(i)=zero
      do 197 i=ns1,ns2
  197   sta(i)=aus(i)-sum(i)
      write(6,'(a)') ' output signal for final model...'
      write(4,'(a)') ' output signal for final model...'

      call output('calex.synt','synth. for final mod',n,dt,sta,tme,tse)
c      endif

      do 6 i=1,n
    6   aus(i)=zero
      do 7 i=ns1,ns2
    7   aus(i)=sum(i)
      write(6,'(a)') ' residual error...'
      write(4,'(a)') ' residual error...'
      call output('calex.rest','obs.-synth.'//endtxt,n,dt,aus,tme,tse)

      call polz

      write(6,*) 'this protocol was saved as file ',nam4
c  generate plot-parameter file
      open(2,file='winplot.par')
      write(2,*) '0, 4, 24, 15,', ns1, ',', ns2,', 0.8'
      write(2,8) 'calex.einf'
      write(2,8) 'calex.ausf'
      write(2,8) 'calex.synt'
      write(2,8) 'calex.rest'
      write(2,8) 'data file: ',ausg
    8 format(a,a)
      close(2)
      write(6,*) 'a plot-parameter file winplot.par was generated'
      write(4,*) 'a plot-parameter file winplot.par was generated'
      write(6,*)
      write(4,*)
      close(4)
      stop
  999 print *,'cannot open ',nam3
      stop 'call ''calex info'' for usage information'
      end


c ==================================================================================
c ==================================================================================

      subroutine inpar(nam3,m,maxit,qac,finac,init,dt,ns1,ns2)
c  read parameters from the parameter file
      parameter (mpar=12,msys=36)
      implicit double precision (a-h,o-z)
      integer zm1,zm2,jpz(msys)
      character*55 zeile
      character*3 nam,name,typ,type,znam,ztyp,atyp(mpar),pnam
      character*50 nam3
      character*4 act
      logical nexttyp,trace,pzmode
      common /par/ x0(msys),rho(msys),x00(mpar),r00(mpar),
     &  typ(msys),nam(mpar),pnam(msys)
      common /zpar/ zx0(msys),zrho(msys),zx00(mpar),zr00(mpar),
     &  ztyp(msys),znam(mpar),mz1,mz2,jp,jpm,jpole(msys)
      common /inp/ m0i,m1i,m2i,m10,m20,mz1i,mz2i,m00
      common /ali/ mali,malias,alias,mal
      common /mode/ pzmode
      data zero,one,two/0.d0,1.d0,2.d0/
      data pnam /msys*'-?-'/
      data jpole /msys*0/
c  NOTE: jpole (the parameter number of a pole associated with a zero) 
c  is presently unused, we keep it just in case...
      jz=0
      ipar=0
      nexttyp=.true.
      type='   '
      zm1=0
      zm2=0
      open(3,file=nam3,status='old')
    1 format(a)
c   ******************************  read inversion parameters

      trace=.false.

      read(3,1) zeile
  201 read(3,1) zeile
      if(zeile(1:1).eq.' '.or.zeile(1:1).eq.'') goto 201
  202 read(3,1) zeile
      if(zeile(1:1).eq.' '.or.zeile(1:1).eq.'') goto 202
    2 read(3,1) zeile
      if(zeile(1:1).eq.' '.or.zeile(1:1).eq.'') goto 2
      read(zeile,*) alias
    3 read(3,1) zeile
      if(zeile(1:1).eq.' '.or.zeile(1:1).eq.'') goto 3
      read(zeile,*) m
    4 read(3,1) zeile
      if(zeile(1:1).eq.' '.or.zeile(1:1).eq.'') goto 4
      read(zeile,*) m0i
    5 read(3,1) zeile
      if(zeile(1:1).eq.' '.or.zeile(1:1).eq.'') goto 5
      read(zeile,*) m1i
    6 read(3,1) zeile
      if(zeile(1:1).eq.' '.or.zeile(1:1).eq.'') goto 6
      read(zeile,*) m2i

      if(pzmode) then
   25 read(3,1) zeile     
      if(zeile(1:1).eq.' '.or.zeile(1:1).eq.'') goto 25
      read(zeile,*) mz1i
   26 read(3,1) zeile
      if(zeile(1:1).eq.' '.or.zeile(1:1).eq.'') goto 26
      read(zeile,*) mz2i      
      endif
      
    7 read(3,1) zeile
      if(zeile(1:1).eq.' '.or.zeile(1:1).eq.'') goto 7
      read(zeile,*) maxit
    8 read(3,1) zeile
      if(zeile(1:1).eq.' '.or.zeile(1:1).eq.'') goto 8
      read(zeile,*) qac
    9 read(3,1) zeile
      if(zeile(1:1).eq.' '.or.zeile(1:1).eq.'') goto 9
      read(zeile,*) finac
   10 read(3,1) zeile
      if(zeile(1:1).eq.' '.or.zeile(1:1).eq.'') goto 10
      read(zeile,*) ns1
   11 read(3,1) zeile
      if(zeile(1:1).eq.' '.or.zeile(1:1).eq.'') goto 11
      read(zeile,*) ns2
      if(m.eq.0) maxit=0
      if(finac.gt.one) finac=one
   
      if(init.eq.0) then
c  note that the second case of this if statement (the 'else') is executed first
c  *******************************************   read system parameters
      write(6,*)
      write(4,*)
      write(6,1) ' reading start parameters (* = active):'
      write(4,1) ' reading start parameters:'
      write(6,*) '   name      value     range     type     math'
      write(4,*) '   name      value     range     type     math'
      write(6,*)
      write(4,*)
      
      type='req'
      do 154 j=1,msys
   23   read(3,1,end=14) zeile
        if(zeile(1:5).eq.'trace') then
        trace=.true.
        goto 23
        endif
        if(zeile(1:1).eq.' '.or.zeile(1:1).eq.'') goto 23
        if(zeile(1:3).eq.'end') goto 14
        
        if(j.gt.4.and.nexttyp) then
          read(zeile,'(a3)') type
          write(6,'(4x,a4)') type//':'
          write(4,'(4x,a4)') type//':'
          
          if(type.ne.'lp1'.and.type.ne.'hp1'.and.
     &    type.ne.'pz1'.and.type.ne.'pz2'.and.
     &    type.ne.'lp2'.and.type.ne.'bp2'.and.type.ne.'hp2') then
            write(6,31) type
            write(4,31) type
   31     format('subsystem ',a3,' not recognized - must be lp1, hp1, pz
     &1, lp2, bp2, hp2, or pz2')
            stop
          endif ! 'type.ne.'lp1' etc.
        
        read(3,1,end=14) zeile
        if(zeile(1:1).eq.' '.or.zeile(1:1).eq.'') goto 23
        if(zeile(1:3).eq.'END'.or.zeile(1:3).eq.'end') goto 14       
        endif ! j.gt.4 etc.
        
        read(zeile,*) name,x0(j),rho(j)
        pnam(j)=name

        if(j.eq.1.and.name.ne.'amp') then
          write(6,'(a)') 'first name must be amp'
          write(4,'(a)') 'first name must be amp'
          stop 1
        else if(j.eq.2.and.name.ne.'del') then
            write(6,'(a)') 'second name must be del'
            write(4,'(a)') 'second name must be del'
            stop 2
        else if(j.eq.3.and.name.ne.'sub') then
            write(6,'(a)') 'third name must be sub'
            write(4,'(a)') 'third name must be sub'
            stop 3
        else if(j.eq.4.and.name.ne.'til') then
            write(6,'(a)') 'fourth name must be til'
            write(4,'(a)') 'fourth name must be til'
            stop 4
        endif ! j = 1 to 4

        if(rho(j).gt.zero)then
          act=' *  '
        else
          act='    '
        endif
        if(j.le.4) then
          write(6,16) act,name,x0(j),rho(j),type,'    '
          write(4,16) act,name,x0(j),rho(j),type,'    '
        else
          write(6,16) act,name,x0(j),rho(j),type,'pole'
          write(4,16) act,name,x0(j),rho(j),type,'pole'
        endif
   16   format(a4,a3,2x,2f10.3,6x,a3,5x,a4)


        if( (x0(2).ne.zero.or.rho(2).ne.zero).and.
     &      (x0(3).ne.zero.or.rho(3).ne.zero) ) then
          write(6,17)
          write(4,17)
   17 format(/" Parameters DEL and SUB should not be used at the same ti
     &me"/)
        endif ! x0(2).ne.zero etc.

        if(rho(j).ne.zero) then
          ipar=ipar+1
          nam(ipar)=name
          x00(ipar)=x0(j)
          r00(ipar)=rho(j)
          atyp(ipar)=type
        endif
        typ(j)=type
        
        if(type(1:2).eq.'pz') then
          if(type.eq.'pz1') zm1=zm1+1
          if(type.eq.'pz2'.and.nexttyp) zm2=zm2+1
c  read parameters for extra zeros into znam, zx0, zrho etc. at index jz
          jz=jz+1
  123     read(3,1,end=14) zeile
          if(zeile(1:1).eq.' '.or.zeile(1:1).eq.'') goto 123
          if(zeile(1:3).eq.'END'.or.zeile(1:3).eq.'end') goto 14
          read(zeile,*) znam(jz),zx0(jz),zrho(jz)
        if(zrho(jz).gt.zero) then
          act=' *  '
        else
          act='    '
        endif
          write(6,16) act,znam(jz),zx0(jz),zrho(jz),type,'zero'
          write(4,16) act,znam(jz),zx0(jz),zrho(jz),type,'zero'
          ztyp(jz)=type
          jpz(jz)=j
        endif
        
        if(type(3:3).eq.'2') nexttyp=.not.nexttyp
  154 continue ! next j

      write(6,101)
      write(4,101)
  101 format(' parameterfile was not read to end')
      stop     
   14 jp=j-1

c  ***************************  init = 0:  system parameters were read
      else
c  ***************************  init < 0: determine anti-alias filter parameters  
      m10=m1i
      m20=m2i 
      m=0
      m0i=0
      m1i=0
      m2i=0
      mz1i=0
      mz2i=0
      do i=1,4
        x0(i)=zero
        rho(i)=zero
      enddo
      x0(1)=one
      j=5
      jp=j-1
      if(alias.lt.4.d0*dt) then
        write(6,103) alias
        write(4,103) alias
  103 format(' specified anti-alias corner period is too small:',f10.3)
        stop
      else
        malias=int(6.d0/dlog10(alias/dt)+one)
        mali=malias/2
        mal=malias-2*mali
        write(6,104) malias,alias
        write(4,104) malias,alias
  104   format(' anti- alias- filter of order',i2,', T=',f6.2,' s')
      endif ! ******************************  anti-alias filter is defined
      endif ! sign(init) 


c     *****************  the anti-alias filter is made part of the transfer function
      if(mal.eq.1) then
c       anti-alias filter has odd order. Store first-order filter module as 
c       parameter #5 after moving system modules to #6 and higher
        do 203 i=jp,5,-1
          x0(i+1)=x0(i)
          rho(i+1)=rho(i)
          typ(i+1)=typ(i)
          pnam(i+1)=pnam(i)
  203   continue
        j=j+1
        jpm=jp+1
        m1i=m1i+1
        x0(5)=alias
        rho(5)=zero
        typ(5)='lp1'
        pnam(5)='b05'
      endif ! mal.eq.1
 
 
      if (trace) then
        write(6,*)
        write(4,*)
        write(6,*) 
     &    'intermediate list of parameters:'
        write(6,*) ' #    name     value     range    type     jpole'
        write(4,*) 
     &  'list of all parameters including the anti-alias filter:'
        write(4,*) ' #    name     value     range    type     jpole'
        do i=1,jpm
          write(6,105) i,pnam(i),x0(i),rho(i),typ(i),jpole(i)
          write(4,105) i,pnam(i),x0(i),rho(i),typ(i),jpole(i)
        enddo
c  105  format(i3,5x,a3,2f10.3,5x,a3,5x,i5)
      endif
 
 
  204 format(a1,i2)
      wi=two*datan(one)/malias      
      m2i=m2i+mali
      do 214 i=1,mali
        l2i=j+2*i-2
        x0(l2i)=alias
        rho(l2i)=zero
        typ(l2i)='lp2'
        write(pnam(l2i),204) 'a',l2i
        x0(l2i+1)=dsin(wi*(2*i-1))
        rho(l2i+1)=zero
        typ(l2i+1)='lp2'
      write(pnam(l2i+1),204) 'b',l2i+1
  214 continue
  
c  now the zeros must be included in the parameter scheme ****************

      close(3)

      jpm=jp+malias ! number of parameters stored as x0, rho so far
          
      
      if(init.eq.0) then
      
      if(.not.pzmode) then
        mz1i=zm1
        mz2i=zm2
      endif
      
      write(6,15)
      write(6,13) m,m0i,m1i,m2i,mz1i,mz2i,maxit,qac,finac,alias
   13 format(7i5,1x,3f10.6)
      write(4,15)
      write(4,13) m,m0i,m1i,m2i,mz1i,mz2i,maxit,qac,finac,alias
   15 format(/,' control parameters for the iteration:'/
     &'    m   m0   m1   m2  mz1   mz2 maxit      qac     finac     alias
     &s')      
      
        if(trace) then
            write(6,*)
            write(4,*)
        write(6,*)'zeros at nonzero frequencies:'
        write(4,*)'zeros at nonzero frequencies:'
      endif
      do i=1,jz
        if(trace) then
        write(6,18) znam(i),zx0(i),zrho(i),ztyp(i)
        write(4,18) znam(i),zx0(i),zrho(i),ztyp(i)
   18   format(8x,a3,2f10.3,5x,a3,5x,i5)
        endif
        jpm=jpm+1
        x0(jpm)=zx0(i)
        rho(jpm)=zrho(i)
        typ(jpm)=ztyp(i)
        pnam(jpm)=znam(i)
        if(zrho(i).ne.zero) then
          ipar=ipar+1
          nam(ipar)=znam(i)
          x00(ipar)=zx0(i)
          r00(ipar)=zrho(i)
          atyp(ipar)=ztyp(i)
        endif
        typ(jpm)=ztyp(i)
        jpole(jpm)=jpz(i)+mal
      enddo
      
      if(trace) then
      write(6,*)
      write(6,*) 'active parameters:'
      write(4,*)
      write(4,*) 'active parameters:'
      do i=1,ipar
        write(6,18) nam(i),x00(i),r00(i),atyp(i)
        write(4,18) nam(i),x00(i),r00(i),atyp(i)
      enddo
      write(6,*) 'total of ',ipar,' active parameters'
      write(6,*)
      write(4,*) 'total of ',ipar,' active parameters'
      write(4,*)
      endif
c  how many parameters do we need?

c      write(*,*) 'mz1i,mz2i,zm1,zm2: ',mz1i,mz2i,zm1,zm2

      numpol=m1i+mz1i+2*m2i+2*mz2i
      if(ipar.ne.m.or.ipar.gt.mpar.or.jp+malias.ne.numpol+4.or.
     &   jz.ne.mz1i+2*mz2i) then
        write(6,102)
        write(4,102)
  102   format(' Parameter file is inconsistent. Check number of active
     & parameters and subsystems.')
        stop
      endif
      if (trace) then
        write(6,*)
        write(4,*)
        write(6,*) 
     &    'list of all parameters including the anti-alias filter:'
        write(6,*) ' #    name     value     range    type     jpole'
        write(4,*) 
     &  'list of all parameters including the anti-alias filter:'
        write(4,*) ' #    name     value     range    type     jpole'
        do i=1,jpm
          write(6,105) i,pnam(i),x0(i),rho(i),typ(i),jpole(i)
          write(4,105) i,pnam(i),x0(i),rho(i),typ(i),jpole(i)
        enddo
  105   format(i3,5x,a3,2f10.3,5x,a3,5x,i5)

      endif
      endif
      return
      end


c ==================================================================================
c ==================================================================================

      subroutine input(name,n,dt,x,tmin,tsec)
      dimension ix(60)
      double precision dt,x(n),tmin,tsec
      character iform*20,name*50,text*72,zeile*72
      character zeil*120,zarr(120),b,code1*6,code2*6
      logical seife

      b=' '
      do j=1,n
        x(j)=0.
      enddo

      write(6,*) 'Opening file ',trim(name)
      write(4,*) 'Opening file ',trim(name)
        open(7,file=name,status='old')
        read(7,'(a)') zeile
        read(7,'(a)') zeile
        seife=index(zeile,'%').gt.0.or.index(zeile,'(').gt.0
        if(seife) then
          write(6,*) 'file ',trim(name),' assumed to be in SEIFE format'
          write(4,*) 'file ',trim(name),' assumed to be in SEIFE format'
        else
          write(6,*) 'file ',trim(name),' assumed to be in ASL format'
          write(4,*) 'file ',trim(name),' assumed to be in ASL format'
        endif
        close(7)
        
      if(seife) then
c read data in SEIFE format      
        open(7,file=name,status='old')
        read(7,'(a)') text
        write(6,*) 'header: ',trim(text)
        write(4,*) 'header: ',trim(text)
   21   read(7,'(a)') zeile
        if(zeile(1:1).eq.'%') goto 21
   20   read(zeile,1) nn,iform,dt,tmin,tsec
    1   format(i10,a20,3f10.3)
        if(nn.gt.n) then
          write(6,*) 'sorry, too many data points. can handle only ', n
          write(6,*) 'for more, edit and recompile the source code'
          write(4,*) 'sorry, too many data points. can handle only ', n
          write(4,*) 'for more, edit and recompile the source code'
        else
          n=nn
        endif
        write(6,22) n,trim(name)
        write(4,22) n,trim(name)
   22   format(' reading',i7,' samples from file ',a)
        if(iform(1:3).eq.'fre'.or.index(iform,'i').gt.0) then
          read(7,*,err=25,end=23) (x(j),j=1,n)
        else
          read(7,iform,err=25,end=23) (x(j),j=1,n)
        endif
        close(7)
        return
      else
      
c read data in ASL format (such as written by Quanterra's Cimarron)
        open(7,file=name,status='old')
        read(7,'(a)') zeil
        text=zeil(1:72)
        write(6,*) 'header: ',trim(text)
        write(4,*) 'header: ',trim(text)
        read(zeil,'(120a)') zarr
        nn=0
        do i=1,119
        if(zarr(i).ne.b.and.zarr(i+1).eq.b) then
          nn=nn+1
          ix(nn)=i
        endif
        enddo
        read(zeil(1:ix(1)),'(a)') code1
        read(zeil(ix(1)+2:ix(2)),'(a)') code2
        read(zeil(ix(2)+2:ix(7)),*) iyear,iday,ithr,itmin,itsec
        read(zeil(ix(7)+2:ix(11)),*) t1000,srate,nn       
        thr=ithr
        tmin=itmin
        tsec=itsec
        dt=1./srate
        tmin=tmin+60.*thr
        tsec=tsec+t1000/1000.
        if(nn.gt.n) then
          write(6,*) 'sorry, too many data points. can handle only ', n
          write(6,*) 'for more, edit and recompile the source code'
          write(4,*) 'sorry, too many data points. can handle only ', n
          write(4,*) 'for more, edit and recompile the source code'
        else
          n=nn
        endif
        dt=1./srate
        tmin=tmin+60.*thr
        tsec=tsec+t100/100.
        
c        read(7,*,err=25,end=23) (x(j),j=1,n)
c free-format read doesn't always work - compiler bug? 
c we must write our own routine

        nz=0
        nval=0
        do
          write(zeil,'(120x)')
          read(7,'(a)',end=31) zeil
          nz=nz+1
c          read(zeil,'(120a)') zarr
          nn=0
          do i=1,119
c           if(zarr(i).ne.b.and.zarr(i+1).eq.b) nn=nn+1
            if(zeil(i:i).ne.b.and.zeil(i+1:i+1).eq.b) nn=nn+1
          enddo
          nn=min(nn,n-nval)
          read(zeil,*,err=32) (ix(j),j=1,nn)
          do j=1,nn
            x(nval+j)=ix(j)
          enddo
          nval=nval+nn
        enddo
   31   write(6,*) nval,' samples read from file ',trim(name)
        write(4,*) nval,' samples read from file ',trim(name)
        close(7)
        return
   32   write(6,*) 'error (maybe a non-numeric character) in data line '
     &  ,nz,' of file ',trim(name)
        write(4,*) 'error (maybe a non-numeric character) in data line '
     &  ,nz,' of file ',trim(name)
        stop
      endif
      
   23 n=j-1
      write(6,24) n
      write(4,24) n
   24 format('end of file after ',i8,' samples')
      close(7)
      return
   25 write(6,26) j
      write(4,26) j
   26 format(' Input error at sample # ',i8)
      stop
      end

      subroutine output(name,text,n,dt,x,tmin,tsec)
      double precision dt,x(n),xmax,tmin,tsec
      character name*(*),text*(*),form*20
      xmax=0.d0
c      do 2 j=1,n
c    2 xmax=max(xmax,abs(x(j)))
c      nvor=log10(max(xmax,2.d0))+1.
c      ndec=max(0,10-nvor)
c      write(form,3) ndec
      form='(5g15.7)'
c    3 format('(5f13.',i1,')')
      write(6,101) name
      write(4,101) name
  101 format(' writing file ',a)
      open(8,file=name)
      write(8,'(a)') text
      write(8,1) n,form,dt,tmin,tsec
    1 format(i10,a20,f10.6,2f10.3)
      write(8,form) (x(j),j=1,n)
      close(8)
      return
      end


      subroutine congrd(x,q,gnorm,iter,ein,aus,sum,einf,sta)
c  Method of conjugate gradients according to Fletcher und Reeves (1964)
      parameter (mpar=12,ns=800000)
      implicit double precision (a-h,o-z)
      dimension ein(ns),aus(ns),sum(ns),einf(ns),sta(ns)
      dimension x(mpar),g(mpar),d(mpar)
      dimension dd(mpar)
      common /aux/ step,finac,axi,qn,dt,ns1,ns2,mq,init,m
      data zero,two/0.d0,2.d0/

c  partial derivatives

      do 1 k=1,m
        x(k)=x(k)+finac
        call quadr(x,qq,ein,aus,sum,einf,sta)
        x(k)=x(k)-finac-finac
        call quadr(x,qqq,ein,aus,sum,einf,sta)
        x(k)=x(k)+finac
    1   g(k)=(qq-qqq)/two/finac

c  determine new direction of descent

      if(mod(iter-1,m).eq.0) then
        write(6,6)
        write(4,6)
    6   format(1x)
        gnorm=0.d0
        do 2 k=1,m
          gnorm=gnorm+g(k)**2
    2     d(k)=-g(k)
      else
        ga=gnorm
        gnorm=zero
        do 3 k=1,m
    3     gnorm=gnorm+g(k)**2
        beta=gnorm/ga
        do 4 k=1,m
    4     d(k)=-g(k)+beta*d(k)
      endif

      dlen=0.d0
      do 5 k=1,m
    5   dlen=dlen+d(k)**2
      dlen=dsqrt(dlen)

      do 8 k=1,m
    8 dd(k)=d(k)/dlen

c  search for minimum

      call mini(x,q,dd,ein,aus,sum,einf,sta)
      return
      end



      subroutine mini(x,q,d,ein,aus,sum,einf,sta)
c     new version, Oct. 2008, equivalent to the old version
c     x and q entered as start values, replaced by values of minimum
c     step size is between 1 and finac
      parameter (mpar=12,ns=800000)
      implicit double precision (a-h,o-z)
      logical exit3,exit4,loop4,interp
      dimension ein(ns),aus(ns),sum(ns),einf(ns),sta(ns)
      dimension x(mpar),xl(mpar),xm(mpar),xr(mpar),d(mpar)
      common /aux/ step,finac,axi,qn,dt,ns1,ns2,mq,init,m
      data one,two,eight /1.d0,2.d0,8.d0/
      
      call move(xm,qm,x,q)
      do k=1,m
         xl(k)=x(k)-step*d(k)
         xr(k)=x(k)+step*d(k)
      enddo
      call quadr(xl,ql,ein,aus,sum,einf,sta)
      call quadr(xr,qr,ein,aus,sum,einf,sta)

      if(ql.lt.qm.and.qr.lt.qm) then
c        qm is the maximum of ql, qm, qr. This should never happen.
         stop 'Something went wrong. Try different search ranges.'
      endif

      loop_3: do
         if(qm.le.ql.and.qm.le.qr) then
c           qm is the minimum of ql, qm, qr. We can leave loop 3.  
            if(step.lt.eight*finac) then
               exit3=.true.
               loop4=.false.
               interp=.true.
            else
               exit3=.false.
               step=step/eight
               do k=1,m
                  xl(k)=xm(k)-step*d(k)
                  xr(k)=xm(k)+step*d(k)
               enddo
               call quadr(xl,ql,ein,aus,sum,einf,sta)
               call quadr(xr,qr,ein,aus,sum,einf,sta)
            endif
         else
            exit3=.true.
            loop4=.true.
            if(ql.lt.qr) then
c!             reverse direction so we don't have to deal with two cases
               call move(x,q,xl,ql)
               call move(xl,ql,xr,qr)
               call move(xr,qr,x,q)
               do k=1,m
                  d(k)=-d(k)
               enddo
            endif
         endif
         if(exit3) exit loop_3
      enddo loop_3
    
      if(loop4) then
         loop_4: do
            if(step.gt.one) then
c!             don't interpolate
               exit4=.true.
               interp=.false.
               call move(x,q,xr,qr)
               axi=step
            else
c!             double interval beyond xr
               step=two*step
               call move(xm,qm,xr,qr)
               do k=1,m
                  xr(k)=xm(k)+step*d(k)
               enddo
               call quadr(xr,qr,ein,aus,sum,einf,sta)
               if(qr.lt.qm) then
                  exit4=.false.
               else
                  exit4=.true.
                  interp=.true.
               endif
            endif
            if(exit4) exit loop_4
         enddo loop_4
      endif
      
      if(interp) then
         xi=(ql-qr)/(ql-two*qm+qr)/two*step
         axi=dabs(xi)
         do k=1,m
            x(k)=xm(k)+xi*d(k)
         enddo
         call quadr(x,q,ein,aus,sum,einf,sta)
      endif
      return
      end


      subroutine mini_s(x,q,d,ein,aus,sum,einf,sta)
c  original 'spaghetti' version of subroutine mini (of around 1985)
c  x and q entered as start values, replaced by values of minimum
c  step size is between 1 and finac
      parameter (mpar=12,ns=800000)
      implicit double precision (a-h,o-z)
      dimension ein(ns),aus(ns),sum(ns),einf(ns),sta(ns)
      dimension x(mpar),xl(mpar),xm(mpar),xr(mpar),d(mpar)
      common /aux/ step,finac,axi,qn,dt,ns1,ns2,mq,init,m
      data one,two,eight /1.d0,2.d0,8.d0/
      call move(xm,qm,x,q)

      do 1 k=1,m
        xl(k)=x(k)-step*d(k)
    1   xr(k)=x(k)+step*d(k)

      call quadr(xl,ql,ein,aus,sum,einf,sta)
      call quadr(xr,qr,ein,aus,sum,einf,sta)

c! maximum encountered
    3 if(ql.lt.qm.and.qr.lt.qm) then
        stop 222
      endif
c! go on
      if(ql.lt.qm.or.qr.lt.qm) goto 2
c! interval too small
   11 if(step.lt.eight*finac) goto 8
c! divide interval
      step=step/eight

      do 7 k=1,m
        xl(k)=xm(k)-step*d(k)
    7   xr(k)=xm(k)+step*d(k)

      call quadr(xl,ql,ein,aus,sum,einf,sta)
      call quadr(xr,qr,ein,aus,sum,einf,sta)
      goto 3

    2 if(ql.lt.qr) then
c! reverse direction
        call move(x,q,xl,ql)
        call move(xl,ql,xr,qr)
        call move(xr,qr,x,q)
        do 9 k=1,m
    9     d(k)=-d(k)
      endif

    4 if(step.gt.one) then
c!  don't interpolate
        call move(x,q,xr,qr)
        axi=step
        return
      else
        step=two*step
        call move(xm,qm,xr,qr)
c! double interval
        do 5 k=1,m
    5     xr(k)=xm(k)+step*d(k)
        call quadr(xr,qr,ein,aus,sum,einf,sta)
        if(qr.lt.qm) goto 4
c! again?
      endif

    8 xi=(ql-qr)/(ql-two*qm+qr)/two*step
      axi=dabs(xi)
      do 6 k=1,m
    6   x(k)=xm(k)+xi*d(k)
      call quadr(x,q,ein,aus,sum,einf,sta)
      return
      end


      subroutine move(x2,q2,x1,q1)
      parameter (mpar=12)
      implicit double precision (a-h,o-z)
      dimension x1(mpar),x2(mpar)
      common /aux/ step,finac,axi,qn,dt,ns1,ns2,mq,init,m
      do 1 k=1,m
    1   x2(k)=x1(k)
      q2=q1
      return
      end


      subroutine quadr(x,quad,ein,aus,sum,einf,sta)
      
c  This routine calculates the synthetic output signal an subtracts
c  it from the observed one, in order to determine the energy of
c  the residual which will be minimized with the conjugate-gradient
c  method. The energy is normalized to the energy of the signal.

c  The system is modelled as a parallel arrangement (sum) of first-order 
c  and second-order filters, not as an arrangement in series (product).
c  For details see the publication by Schuessler.

      parameter (mpar=12,ns=800000,msys=36)
      implicit double precision (a-h,o-z)
      character*3 nam,typ,znam,ztyp,pnam
      dimension x(mpar)
      dimension ein(ns),aus(ns),sum(ns),einf(ns),sta(ns)
      common /aux/ step,finac,axi,qn,dt,ns1,ns2,mq,init,m
      common /sys/ syspar(0:msys),sr(msys),si(msys),resid(msys),
     & residi(msys),m0,m1,m2
      common /par/ x0(msys),rho(msys),x00(mpar),r00(mpar),
     &  typ(msys),nam(mpar),pnam(msys)
      common /zpar/ zx0(msys),zrho(msys),zx00(mpar),zr00(mpar),
     &  ztyp(msys),znam(mpar),mz1,mz2,jp,jpm,jpole(msys)
      data zero,half,grav/0.d0,5.d-1,9.81d-3/
      mq=mq+1
c      write(6,*) 'quadr Aufruf ', mq
      ns21=ns2-ns1+1
c  in order to be able to delay and advance the synthetics, we must
c  compute a few extra points outside the interval (ns1,ns2)
      ns14=ns1-24
      ns24=ns2+24

      call sysdef(x,dt,delay,compens1,compens2)

        if(dabs(delay/dt).ge.24d0) then
          write(6,101)
          write(4,101)
  101     format(' Delay exceeds 24*dt.'/
     & ' Use a smaller search range for the delay parameter.'/
     & ' A range smaller than the sampling interval is recommended.'/
     & ' If this does not help, then you must edit the time series.'/
     & ' Remove an appropriate number of samples from the beginning of'/
     & ' either the input or the output signal to reduce the delay.')
          stop
        endif

      call partl

      do 455 i=1,ns24
        sum(i)=zero
  455 continue

      ishift=ceiling(delay/dt)
      tau=delay-dt*ishift

      do 462 j=1,m1
        i=m0+j
        call rekf1(dt*sr(i),resid(i)*dexp(-tau*sr(i)),ns24,ein,sum)
  462 continue

      do 467 j=1,m2
        i=m0+m1+j
      if(si(i).eq.zero) then
        call rekf1(dt*sr(i),resid(i)*dexp(-tau*sr(i)),ns24,ein,sum)
        call rekf1(dt*sr(i+m2),resid(i+m2)*dexp(-tau*sr(i+m2)),ns24,ein,
     &  sum)
      else
        sre=-tau*sr(i)
        sim=-tau*si(i)
        zabs=dexp(sre)
        zre=zabs*dcos(sim)
        zim=zabs*dsin(sim)
        rre=resid(i)*zre-residi(i)*zim
        rim=resid(i)*zim+residi(i)*zre
        call rekf2(dt*sr(i),dt*si(i),rre,rim,ns24,ein,sum)
      endif
  467 continue

      if(init.lt.0) return

      if(ishift.gt.0) then
        do 482 i=ns2,ns1,-1
          sum(i)=sum(i-ishift)
  482   continue
      else if(ishift.lt.0) then
        do 483 i=ns1,ns2
          sum(i)=sum(i-ishift)
  483   continue
      endif

c   sum is now the synthetics. Correct for galvanic coupling or
c   shake-table tilt if required, form difference with observed 
c   output, and remove offset. 
c   sub is the fraction of the input signal to be subtracted 
c   from the output.
c   til ist the tilt of the shake table in microradians per
c   millimeter of motion. grav is the gravity in km/s^2. 
c   Note: motion and tilt as seen by the sensor, so if the 
c   motion is transverse, the tilt parameter may come out 
c   very large! 

      if(compens2.ne.zero) then
        sta(1)=half*sum(ns1)
        do i=ns1+1,ns2
          ii=i-ns1+1
          sta(ii)=sta(ii-1)+half*(sum(i-1)+sum(i))
        enddo
        call polytrend(1,sta,ns21)

        suma=sta(1)
        sta(1)=half*suma
        do i=ns1+1,ns2
          ii=i-ns1+1
          sumn=sta(ii)
          sta(ii)=sta(ii-1)+half*(suma+sumn)
          suma=sumn
        enddo
        call polytrend(2,sta,ns21)
      endif

      comp=compens2*grav*dt**2
      do i=ns1,ns2
        ii=i-ns1+1
        sum(i)=aus(i)-sum(i)-compens1*einf(i)+comp*sta(ii)
      enddo
      do i=ns1,ns2
        ii=i-ns1+1
        sta(ii)=sum(i)
      enddo
c      call polytrend(3,sta,ns21)
      call polytrend(2,sta,ns21)
c      call polytrend(1,sta,ns21)
      do i=ns1,ns2
        ii=i-ns1+1
        sum(i)=sta(ii)
      enddo
      offset=zero
      qua=zero
      do i=ns1,ns2
        offset=offset+sum(i)
      enddo
      offset=offset/ns21
      do i=ns1,ns2
        sum(i)=sum(i)-offset
        qua=qua+sum(i)**2
      enddo
      quad=qua/qn
      
c   At the end of the program, sum is the misfit (with its 
c   mean removed), not the synthetic. quad is the normalized 
c   energy of the misfit, as a fraction of the total energy.

      return
      end


      subroutine sysdef(x,dt,delay,compens1,compens2)
      parameter (mpar=12,msys=36)
      implicit double precision (a-h,o-z)
      dimension x(mpar),xx(msys)
      character*3 nam,typ,znam,ztyp,pnam
      common /par/ x0(msys),rho(msys),x00(mpar),r00(mpar),
     &  typ(msys),nam(mpar),pnam(msys)
      common /zpar/ zx0(msys),zrho(msys),zx00(mpar),zr00(mpar),
     &  ztyp(msys),znam(mpar),mz1,mz2,jp,jpm,jpole(msys)
      common /sys/ syspar(0:msys),sr(msys),si(msys),resid(msys),
     & residi(msys),m0,m1,m2
      common /inp/ m0i,m1i,m2i,m10,m20,mz1i,mz2i,m00
      data zero,one,two/0.d0,1.d0,2.d0/
      common /zer/ zeropar(msys),rez(msys),aiz(msys)
      
      common /temp/ icount
      data icount /0/
      icount=icount+1

c **********************************************************************
c **********************************************************************
c **********************************************************************
C   Definition of the transfer function (Laplace Transform):
C   syspar(0 : m0): coefficients of the nominator polynomial, order m0
C   syspar(m0+1 : m0+m1)  corner freq. of first-order partials
C   syspar(m0+m1+1 : m0+m1+m2)  corner freq. of second-order partials
C   syspar(m0+m1+m2+1 : m0+m1+2*m2)  numerical damping factors
C
C
C                     Sum  syspar(i) * s**i
C                   i=0,m0
C   T(s) = --------------------------------------------------------
C           Product (s+w1(i))*Product(s**2+2*s*w2(i)*h(i)+w2(i)**2)
C            i=1,m1            i=1,m2
C
C
C   where: w1(i) = syspar(m0+i)          , i = 1 ... m1
C          w2(i) = syspar(m0+m1+i)       , i = 1 ... m2
C           h(i) = syspar(m0+m1+m2+i)    , i = 1 ... m2

C   In addition the signal may be delayed by 'delay' (abs < +-dt),
C   diminished by a fraction 'compens1' of the input signal to compensate
C   for the voltage coupled into the coil of geophones
C   by the calibration current, and a fraction 'compens2' of the twice-
C   integrated signal may be removed in order to compensate for the tilt of 
C   shake tables.
c **********************************************************************
c **********************************************************************
c **********************************************************************

C  current system parameters (order is different from parameter file!)

      zpi=8.d0*datan(one)
      m0=m0i
      m1=m1i+mz1i
      m2=m2i+mz2i
      mz1=mz1i
      mz2=mz2i
      mp=4+m1+2*m2+mz1+2*mz2
      if(mp.ne.jpm) then
        stop 'programming error: parameter inconsistency'
      endif
      
c  update parameters
      ipar=0
      do 546 i=1,mp
        if(rho(i).gt.zero) then
          ipar=ipar+1
          xx(i)=x0(i)+x(ipar)*rho(i)
c         lpol=jpole(i)
c  move zeros with the associated poles
c  this appears not to work well
c         if(lpol.gt.0) xx(i)=xx(i)+xx(lpol)-x0(lpol)
        else
          xx(i)=x0(i)
        endif
  547  format(i2,2x,a3,3f10.3,3x,a3)
  546 continue

      gain=xx(1)*dt
        delay=xx(2)
        compens1=xx(3)
        compens2=xx(4)

c  Numerator factor for first-order filter subsystems

      do 563 i=4+1,4+m1
        if(typ(i).eq.'hp1') then
          m0=m0+1
        else if(typ(i).eq.'lp1') then
          gain=gain*zpi/xx(i)
        else if(typ(i).eq.'pz1') then
        else
          write(6,102) typ(i)
          write(4,102) typ(i)
  102     format(' illegal type 1: ',a)
          stop
        endif
  563 continue

c  Numerator factor for second-order filter subsystems

      do 571 i=m1+5,m1+2*m2+3,2
        if(typ(i).eq.'hp2') then
          m0=m0+2
        else if (typ(i).eq.'bp2') then
          m0=m0+1
          gain=gain*zpi/xx(i)
        else if (typ(i).eq.'lp2') then
          gain=gain*(zpi/xx(i))**2
        else if (typ(i).eq.'pz2') then
        else
          write(6,106) typ(i)
          write(4,106) typ(i)
          stop
        endif
  106   format(' illegal type 2: ',a)
  571 continue

      if(m0.gt.m1+2*m2-2) then
        write(6,105)
        write(4,105)
  105   format(' m0 is too large - stop')
        stop
      endif

      if(m0.lt.-1) then
        write(6,103)
        write(4,103)
  103   format(' m0 is too small - stop')
        stop
      endif
      
c  Here we only fill in the parameters of the denominator from which 
c  the poles will be calculated in subroutine partl, beginning with the
c  first-order subsystems.

        do 594 i=0,m0-1
          syspar(i)=zero
  594 continue
        syspar(m0)=gain
        do 598 i=1,m1
          syspar(m0+i)=zpi/xx(i+4)
  598 continue

c  second-order subsystems        
      do 611 i=1,m2
        syspar(m0+m1+i)=zpi/xx(2*i+m1+3)
        syspar(m0+m1+m2+i)=xx(2*i+m1+4)
  611 continue
      
c  finally we include the zeros of the pz type subsystems
c  by multiplying the numerator with a first- or second-order factor
c
c  the position of associated poles in the parameter list is stored in lpar(i)

      mpp=4+m1+2*m2
      mpz=mz1+2*mz2
      mpf=mpp+mpz
                 
c  move denominator parameters to make room for longer numerator
      do 575 i=mpp,m0+1,-1
  575 syspar(i+mpz)=syspar(i)
      
      m00=m0
c  first-order factors
      izer=0
      do 573 i=mpp+1,mpp+mz1
c        w0=zpi/xx(jpole(i))/(one+xx(i))
        w0=zpi/xx(i)
        m0=m0+1
        izer=izer+1
        zeropar(izer)=w0
        do j=m0,1,-1
          syspar(j)=syspar(j)*w0+syspar(j-1)
        enddo
        syspar(0)=syspar(0)*w0
  573 continue
  
c  second-order factors
      do 574 i=mpp+mz1+1,mpp+mz1+2*mz2,2
c        w0=zpi/xx(jpole(i))/(one+xx(i))
         w0=zpi/xx(i)
c        h=xx(jpole(i+1))*(one+xx(i+1))
         h=xx(i+1)
        w0q=w0**2
        wh=two*w0*h
        m0=m0+2
        izer=izer+2
        zeropar(izer-1)=w0        
        zeropar(izer)=h
        do j=m0,2,-1
          syspar(j)=syspar(j)*w0q+syspar(j-1)*wh+syspar(j-2)
        enddo
        syspar(1)=syspar(1)*w0q+syspar(0)*wh
        syspar(0)=syspar(0)*w0q
  574 continue
    
      return
      end


      subroutine findzeros
      parameter (msys=36)
      implicit double precision (a-h,o-z)
      dimension syspar(0:msys),sr(msys),si(msys)
      common /inp/ m0i,m1i,m2i,m10,m20,mz1i,mz2i,m00
      common /zer/ zeropar(msys),rez(msys),aiz(msys)
      data zero,one/0.d0,1.d0/

      do 639 i=1,mz1i
        rez(i)=-zeropar(i)
        aiz(i)=zero
  639 continue

      do 643 i=mz1i+1,mz1i+2*mz2i-1,2
        w=zeropar(i)
        h=zeropar(i+1)
        if(dabs(h-one).lt.1.d-6) then
          write(6,111) i-mz1i
          write(4,111) i-mz1i
  111     format(/' SORRY, the program fails when damping is exactly',
     &    ' critical (=1.000000)'/' Try again with a different start ',
     &    'value for damping of pz2 zero #',i2)
          stop
        endif
        if(h.lt.zero) then
          write(6,101) i-mz1i
          write(4,101) i-mz1i
  101     format(' Negative damping encountered. Reduce search range for
     & damping of pz2 zero #',i2)
          stop
        endif
        wh=w*h
        if(h.lt.one) then
          whh=w*dsqrt(one-h**2)
          rez(i)=-wh
          aiz(i)=whh
          rez(i+1)=-wh
          aiz(i+1)=-whh
        else
          whh=w*dsqrt(h**2-one)
          rez(i)=-wh+whh
          aiz(i)=zero
          rez(i+1)=-wh-whh
          aiz(i+1)=zero
        endif
  643 continue      
      
      return
      end

      subroutine polz
      parameter (msys=36)
      implicit double precision (a-h,o-z)
      common /inp/ m0i,m1i,m2i,m10,m20,mz1i,mz2i,m00
      common /sys/ syspar(0:msys),sr(msys),si(msys),resid(msys),
     & residi(msys),m0,m1,m2
      common /ali/ mali,malias,alias,mal
      common /zer/ zeropar(msys),rez(msys),aiz(msys)
      complex a,b
      real*4 omega,per,dmp,zpr
      
      data zero,one,two/0.d0,1.d0,2.d0/
      zpi=8.d0*datan(one)
      zpr=real(zpi)

C  first-order partials: poles sr and residues resid are real and have
c  indices m0+i with i=1 .. m1
C  second-order partials: poles (sr,si) and residues (resid,residi) are
c  pairwise complex-conjugate and stored in
c  sr(m0+m1+i), si(m0+m1+i) and sr(m0+m1+m2+i), si(m0+m1+m2+i), i=1 .. m2

      write(6,*)
      write(4,*)
      write(6,*) 'Transfer function (counts in ==> counts out):'
      write(4,*) 'Transfer function (counts in ==> counts out):'
      write(6,*) 'Poles and zeros are given as angular frequencies (rad/
     &s)'
      write(4,*) 'Poles and zeros are given as angular frequencies (rad/
     &s)'
      write(6,*)
      write(4,*)


c      write(6,*) 'm10,m20,mz1i,mz2i: ',  m10,m20,mz1i,mz2i
c      write(6,*) 'm0,m1,m2',m0,m1,m2

      call findzeros
      
c      do i=1,mz1i+2*mz2i
c         write(6,*) zeropar(i),rez(i),aiz(i)
c      enddo
c      write(6,*)
c      write(4,*)

      if (m0.eq.1) then
      write(6,*) 'The Laplace transform has',m0,' zero:'
      write(4,*) 'The Laplace transform has',m0,' zero:'
      else
      write(6,*) 'The Laplace transform has',m0,' zeros:'
      write(4,*) 'The Laplace transform has',m0,' zeros:'
      endif
      
      if (m00.eq.1) then
      write(6,*)
      write(4,*)
      write(6,*) m00,' zero at zero frequency (s=0)'
      write(4,*) m00,' zero at zero frequency (s=0)'
      else if (m00.gt.1) then
      write(6,*) m00,' zeros at zero frequency (s=0)'
      write(4,*) m00,' zeros at zero frequency (s=0)'
      endif 
      
      if(mz1i.eq.1) then
      write(6,*)
      write(4,*)
        write(6,*) mz1i,' real zero:                     period:'
        write(4,*) mz1i,' real zero:                     period:'
      else if (mz1i.gt.1) then
        write(6,*) mz1i,' real zeros:                    period:'
        write(4,*) mz1i,' real zeros:                    period:'
      endif
      do i=1,mz1i
      write(6,*)
      write(4,*)
        write(6,*) sngl(rez(i)),'               ',sngl(zpi/zeropar(i))
        write(4,*) sngl(rez(i)),'               ',sngl(zpi/zeropar(i))
      enddo
      
      if(mz2i.eq.1) then
      write(6,*)
      write(4,*)
        write(6,*) mz2i,' pair of complex-conjugate zeros:'
        write(4,*) mz2i,' pair of complex-conjugate zeros:'
        write(6,*) '      real           imag     period,dmp'
        write(4,*) '      real           imag     period,dmp'
      else if(mz2i.gt.1) then
        write(6,*) mz2i,' pairs of complex-conjugate zeros:'
        write(4,*) mz2i,' pairs of complex-conjugate zeros:'
        write(6,*) '      real           imag     period,dmp'
        write(4,*) '      real           imag     period,dmp'
      endif
      do i=mz1i+1,mz1i+2*mz2i-1,2
        write(6,*)
        write(4,*)
        write(6,*)  sngl(rez(i)),  sngl(aiz(i)),  sngl(zpi/zeropar(i))
        write(6,*)  sngl(rez(i+1)),  sngl(aiz(i+1)),  sngl(zeropar(i+1))
        write(4,*)  sngl(rez(i)),  sngl(aiz(i)),  sngl(zpi/zeropar(i))
        write(4,*)  sngl(rez(i+1)),  sngl(aiz(i+1)),  sngl(zeropar(i+1))

      enddo
     
      mm1=m10+mz1i
      write(6,*)
      write(4,*)
      if(mm1.eq.1) then
        write(6,*) 'The Laplace transform has 1 real pole:'
        write(4,*) 'The Laplace transform has 1 real pole:'
      else
        write(6,*) 'The Laplace transform has',mm1,' real poles:'
        write(4,*) 'The Laplace transform has',mm1,' real poles:'
      endif
c     mal=malias-2*mali = 1 when the anti-alias filter has a real pole
      do i=1,mm1
        omega=real(sr(m0+i+mal))
        write(6,*)
        write(4,*)
        write(6,*) omega,'               ',-zpr/omega
        write(4,*) omega,'               ',-zpr/omega
      enddo
      write(6,*)
      write(4,*)
      mm2=m20+mz2i
      if(mm2.eq.1) then
        write(6,*) 'and 1 pair of complex-conjugate poles:'
        write(4,*) 'and 1 pair of complex-conjugate poles:'
        write(6,*) '      real           imag     period,dmp'
        write(4,*) '      real           imag     period,dmp'
      else if(mm2.gt.1) then
        write(6,*) 'and',mm2,' pairs of complex-conjugate poles:'
        write(4,*) 'and',mm2,' pairs of complex-conjugate poles:'
        write(6,*) '      real           imag     period,dmp'
        write(4,*) '      real           imag     period,dmp'
      endif
      do i=1,mm2
        in1=m0+m1+i
        in2=in1+m2
        a=cmplx(sr(in1),si(in1))
        b=cmplx(sr(in2),si(in2))
        omega=sqrt(real(a*b))
        dmp=-real(a+b)/omega/2.
        per=zpr/omega
        write(6,*)
        write(4,*)
        write(6,*)  sngl(sr(in1)), sngl(si(in1)),per
        write(6,*)  sngl(sr(in2)), sngl(si(in2)),dmp
        write(4,*)  sngl(sr(in1)), sngl(si(in1)),per
        write(4,*)  sngl(sr(in2)), sngl(si(in2)),dmp
      enddo
      write(6,*)
      write(4,*)
      return
      end



      subroutine partl
      parameter (msys=36)
      implicit double precision (a-h,o-z)
      common /sys/ syspar(0:msys),sr(msys),si(msys),resid(msys),
     & residi(msys),m0,m1,m2

      data zero,one/0.d0,1.d0/

C  factorization of the denominator of transfer function.
C  poles und residues (real and imag.parts) stored like system parameters
C  first-order partials: poles sr and residues resid are real and have
c  indices m0+i with i=1 .. m1
C  second-order partials: poles (sr,si) and residues (resid,residi) are
c  pairwise complex-conjugate and stored in
c  sr(m0+m1+i), si(m0+m1+i) and sr(m0+m1+m2+i), si(m0+m1+m2+i), i=1 .. m2
C  residues are stored in resid(m0+m1+i), residi(mo+m1+i), i=1 .. 2*m2

C  determine the complex poles

      do 639 i=1,m1
        sr(m0+i)=-syspar(m0+i)
        si(m0+i)=zero
  639 continue

      do 643 i=1,m2
        w=syspar(m0+m1+i)
        h=syspar(m0+m1+m2+i)
        if(dabs(h-one).lt.1.d-6) then
          write(6,111) i
          write(4,111) i
  111     format(/' SORRY, the program fails when damping is exactly',
     &    ' critical (=1.000000)'/' Try again with a different start ',
     &    'value for damping #',i2)
          stop
        endif
        if(h.lt.zero) then
          write(6,101) i
          write(4,101) i
  101     format(' Negative damping encountered. Reduce search range for
     & damping #',i2)
          stop
        endif
        wh=w*h
        if(h.lt.one) then
          whh=w*dsqrt(one-h**2)
          sr(m0+m1+i)=-wh
          si(m0+m1+i)=whh
          sr(m0+m1+m2+i)=-wh
          si(m0+m1+m2+i)=-whh
        else
          whh=w*dsqrt(h**2-one)
          sr(m0+m1+i)=-wh+whh
          si(m0+m1+i)=zero
          sr(m0+m1+m2+i)=-wh-whh
          si(m0+m1+m2+i)=zero
        endif
  643 continue

C  calculate residues

      do 1 i=m0+1,m0+m1+2*m2
        cresr=syspar(0)
        cresi=zero
        spowr=one
        spowi=zero
        do 2 k=1,m0
          spowre=spowr*sr(i)-spowi*si(i)
          spowi=spowr*si(i)+spowi*sr(i)
          spowr=spowre
          cresr=cresr+syspar(k)*spowr
    2     cresi=cresi+syspar(k)*spowi
        do 3 k=m0+1,m0+m1+2*m2
          if(k.ne.i) then
c           cres=cres/(s(i)-s(k))
            sdr=sr(i)-sr(k)
            sdi=si(i)-si(k)
            sdq=sdr*sdr+sdi*sdi
            
            if(sdq.lt.1.d-12) then
              write(6,120) 
  120 format(/' WARNING: some complex poles are identical or too close.'
     &,/,' Loss of accuracy or failure may result. Change start values,'
     &,/,' avoiding coincidence with the Butterworth anti-alias filter.'
     &/)
            endif
            cresre=(cresr*sdr+cresi*sdi)/sdq
            cresi=(cresi*sdr-cresr*sdi)/sdq
            cresr=cresre
          endif
    3   continue
        resid(i)=cresr
    1   residi(i)=cresi

      return
      end



      subroutine rekf1(dt,r,n,ein,sum)
      parameter(ns=800000)
      implicit double precision (a-h,o-z)
      dimension ein(ns),sum(ns)
      z=dexp(dt)
      aus0=r*ein(1)
      sum(1)=sum(1)+aus0

      do 691 i=2,n
        aus0=r*ein(i)+z*aus0
        sum(i)=sum(i)+aus0
  691 continue

      return
      end


      subroutine rekf2(sre,sim,rre,rim,n,ein,sum)
      parameter(ns=800000)
      implicit double precision (a-h,o-z)
      dimension ein(ns),sum(ns)
      data two/2.d0/
      zabs=dexp(sre)
      zre=zabs*dcos(sim)
      zim=zabs*dsin(sim)
      f0=two*rre
      f1=-two*(rre*zre+rim*zim)
      g1=two*zre
      g2=-zabs**2
      aus0=f0*ein(1)
      sum(1)=sum(1)+aus0
      aus1=aus0
      aus0=f0*ein(2)+f1*ein(1)+g1*aus1
      sum(2)=sum(2)+aus0

      do 719 i=3,n
        aus2=aus1
        aus1=aus0
        aus0=f0*ein(i)+f1*ein(i-1)+g1*aus1+g2*aus2
        sum(i)=sum(i)+aus0
  719 continue

      return
      end

      subroutine polytrend(mm,x,n)
c  remove polynomial trend
      parameter(ndi=8,ns=800000)
      implicit real*8 (a-h,o-z)
      dimension b(ndi),c(ndi,ndi),a(ndi),x(ns)
      m=min(mm,ndi-1)
      fnh=n/2.
      zero=0.d0
      one=1.d0
      do j=1, m+1
        do k=1, m+1
          c(j,k)=zero
          do i=1, n
            c(j,k)=c(j,k)+(dble(i)/fnh-one)**(j+k-2)
          enddo
        enddo
        b(j)=zero
        do i=1,n
          b(j)=b(j)+(dble(i)/fnh-one)**(j-1)*x(i)
        enddo
      enddo
      call gauss(c,m+1,ndi,b,a)
  100 format(i5,e15.6)
      do i=1,n
        xpol=a(m+1)
        do j=m,1,-1
          xpol=xpol*(dble(i)/fnh-one)+a(j)
        enddo
        x(i)=x(i)-xpol
      enddo
      return
      end


      subroutine gauss(aik,m,n,rs,f)
c  solve linear equations
      implicit real*8 (a-h,o-z)
      parameter(ndi=8,ndi1=ndi+1)
      dimension aik(n,n),rs(n),f(n),h(ndi1),imax(ndi)
      do 1401 j=1,m
      aikmax=0.d0
      do 1402 k=1,m
      h(k)=aik(j,k)
      if(abs(h(k)).le.aikmax) go to 1402
      aikmax=abs(h(k))
      index=k
 1402 continue
      h(m+1)=rs(j)
      do 1403 k=1,m
      q=aik(k,index)/h(index)
      do 1404 l=1,m
 1404 aik(k,l)=aik(k,l)-q*h(l)
 1403 rs(k)=rs(k)-q*h(m+1)
      do 1405 k=1,m
 1405 aik(j,k)=h(k)
      rs(j)=h(m+1)
 1401 imax(j)=index
      do 1406 j=1,m
      index=imax(j)
 1406 f(index)=rs(j)/aik(j,index)
      return
      end
