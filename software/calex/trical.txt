﻿Program description TRICAL

This is a combination of the programs TRIAX and CALEX for evaluating the calibration of homogeneous-triaxial seismometers with the method of Peter Davis. See section 5.6.7 of the NMSOP for a general explanation, and the program description calex.doc for details. 

TRICAL uses the same parameter file as CALEX except for an obvious modification: trical.par specifies a total of four file names, one for the stimulus (the input to the calibration coil)  and three for the X, Y, Z output signals. The screen output and the trical.out file contain the parameters of the transfer function both for the U, V, W sensors and for the X, Y, Z outputs. TRICAL generates three plot-parameter files, one each for  the U, V, and W calibration. A small batch file plot3.bat allows convenient plotting.

The parameter file for the test case:

calibration of a triaxial 20 s seismometer (synthetic signals)

data\rtein.f   input to seismo (file name)
data\rtaus.x   x output from seismo (file name)
data\rtaus.y   y output from seismo (file name)
data\rtaus.z   z output from seismo (file name)

0.5  alias
3    m
0    m0
0    m1
1    m2
60   maxit
1e-6 qac
1e-4 finac
201  ns1
0    ns2
    
amp  1.         0.5 
del  0.         0.
sub  0.         0. 
til  0.         0.

bp2
per  20.        2.
dmp  0.7        0.1

end


Download the software, test data, and chapter 5 of the NMSOP from

http://www.software-for-seismometry.de/   or

http://www.geophys.uni-stuttgart.de/~erhard/downloads/

