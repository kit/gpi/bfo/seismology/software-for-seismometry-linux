c this is <makecalexhelp.f>
c ----------------------------------------------------------------------------
c
c Copyright (c) 2011 by Thomas Forbriger (BFO Schiltach) 
c
c provide online help for calex
c
c REVISIONS and CHANGES
c    13/09/2011   V1.0   Thomas Forbriger
c    07/07/2015   V1.1   provide comments from calex source code too
c
c ============================================================================
      program makecalexhelp
      open(8,file='calexinfo.f',status='new', err=96)
      write(8, 50, err=95) 'subroutine calexinfo'
      write(8, 52, err=95) 
     &  'Notice: This file is created automatically from calex.f'
      write(8, 52, err=95) 
     &  'Do not include this file in a software repository!'
      write(8, 52, err=95) 
     &  'makecalexhelp can be used to recreate calexinfo.f'
      call dumpcomments(8, 'calex.f','comments from source code')
      write(8, 51, err=95) ' '
      write(8, 51, err=95) '========================================='
      write(8, 51, err=95) ' '
      call dumpfile(8, 'calex.par','standard parameter file')
      write(8, 51, err=95) ' '
      write(8, 51, err=95) '========================================='
      write(8, 51, err=95) ' '
      call dumpfile(8, 'calex-pz.par',
     & 'not to be used together with calex10')
      write(8, 50, err=95) 'return'
      write(8, 50, err=95) 'end'
      close(8, err=94)
      stop
   96 stop 'ERROR: opening calexinfo.f for writing'
   95 stop 'ERROR: writing to calexinfo.f'
   94 stop 'ERROR: closing calexinfo.f'
c 
   50 format(6x,(a))
   51 format(6x,('write(6,''(a)'') ''',(a),''''))
   52 format('C',1x,(a))
      end

c----------------------------------------------------------------------
c 
      subroutine dumpfile(lu,fname,comment)
      character*(*) fname,comment
      integer lu
c 
      integer iu
      parameter(iu=7)
      character*78 info
      character*160 infoout
      integer i, j, k
      logical hot
c 
      write(lu, 53, err=95) 'Example file ',fname,':'
      print *,'dump file ',fname
      write(lu, 53, err=95) '(',comment,')'
      write(8, 51, err=95) '-----------------------------------------'
      open(iu,file=fname,status='old', err=99)
      hot=.true.
      do while (hot)
        read(iu,'(a)',end=178, err=98) info
        j=0
        k=1
        do i=1,78
          j=j+1
          if (info(i:i).eq.'''') then
            infoout(j:j)=''''
            j=j+1
            infoout(j:j)=''''
          else
            infoout(j:j)=info(i:i)
          endif
          if (info(i:i).ne.' ') k=j
        enddo
        write(lu, 51, err=95) infoout(1:k)
      enddo
  178 continue
      close(iu, err=97)
      return
   99 stop 'ERROR: opening example file for reading'
   98 stop 'ERROR: reading from example file'
   97 stop 'ERROR: closing example file'
   95 stop 'ERROR: writing to calexinfo.f'
c 
c   50 format(6x,(a))
   51 format(6x,('write(6,''(a)'') ''',(a),''''))
c   52 format('C',1x,(a))
   53 format(6x,('write(6,''(a)'') ''',(aaa),''''))
      end

c ----------------------------------------------------------------------

      subroutine dumpcomments(lu,fname,comment)
      character*(*) fname,comment
      integer lu
c 
      integer iu
      parameter(iu=7)
      character*78 info
      character*160 infoout
      integer i, j, k
      logical hot
c
      write(lu, 53, err=95) 'Contents of file ',fname,':'
      print *,'dump file ',fname
      write(lu, 53, err=95) '(',comment,')'
      write(lu, 51, err=95) '-----------------------------------------'
      open(iu,file=fname,status='old', err=99)
      hot=.true.
c 
      do while (hot)
        read(iu,'(a)',end=178, err=98) info
        j=0
        k=1
        do i=1,78
          j=j+1
          if (info(i:i).eq.'''') then
            infoout(j:j)=''''
            j=j+1
            infoout(j:j)=''''
          else
            infoout(j:j)=info(i:i)
          endif
          if (info(i:i).ne.' ') k=j
        enddo
        write(lu, 51, err=95) infoout(1:k)
        if (info(1:8).eq.'c end of') hot=.false.
      enddo
  178 continue
      close(iu, err=97)
      return
   99 stop 'ERROR: opening source code for reading'
   98 stop 'ERROR: reading from source code'
   97 stop 'ERROR: closing source code'
   95 stop 'ERROR: writing to calexinfo.f'
c 
c   50 format(6x,(a))
   51 format(6x,('write(6,''(a)'') ''',(a),''''))
c   52 format('C',1x,(a))
   53 format(6x,('write(6,''(a)'') ''',(aaa),''''))
      end
c
c ----- END OF makecalexhelp.f ----- 
