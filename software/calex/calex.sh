#!/bin/sh
# this is <calex.sh>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2016 by Thomas Forbriger (BFO Schiltach) 
# 
# run calex and display results
#
# ----
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version. 
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# ----
# 
# REVISIONS and CHANGES 
#    11/10/2016   V1.0   Thomas Forbriger
# 
# ============================================================================
#
#
# indicate version
VERSION=2016-10-19
# 
# ============================================================================
# **** define usage functions                                             ****
# ============================================================================
#
usage() {
cat >&2 << HERE
usage: calex.sh [-v|--verbose] [--debug] [-D|--date date] [-u|--units units]
                [-b|--outbase basename] [-i|--iterations n] [file] 
   or: calex.sh --help|-h
HERE
}
#
# ----------------------------------------------------------------------------
#
longusage() {
cat >&2 <<HERE
Run calex with a given control file and produce a report on the results.

  file      calex control file (optional; default: calex.par)

  -v|--verbose          report plot parameters to the terminal
  -D|--date date        set date of seife data
  -u|--units units      set units of data samples
  -b|--outbase basename set basename for output files
  -i|--iterations n     set the number of iterations to be reported in the
                        summary

The shell script expects the following programs/scripts to be installed:
 
calex:                  calex binary
calexoutextract.awk:    extracts a summary from the calex report file
stuploxx:               plot program from Seitosh
a2ps:                   ASCII to Postscript converter/formatter
HERE
}
#
# ============================================================================
# execute
# ============================================================================
#
# echo usage information in any case
echo "calex.sh (version $VERSION)"
usage
#
# read command line options
# -------------------------
TEMP=$(getopt -o vhd:D:u:b:i: --long \
  help,verbose,device:,debug,date:,units:,outbase:,iterations: \
  -n $(basename $0) -- "$@") || {
    echo >&2
    echo >&2 "ERROR: command line parameters are incorrect!"
    echo >&2 "aborting $0..."; exit 2
}
eval set -- "$TEMP"
#
# extract command line options
# ----------------------------
VERBOSE=0
PARAFILE=calex.par
OUTBASE=NSP
UNITS="counts"
NLINES=6
DATE="1970/01/01"
while true; do
  case "$1" in
    --help|-h) longusage; exit 1;;
    --) shift; break;;
    -v|--verbose)     VERBOSE=1;;
    -u|--units)       UNITS="$2"; shift;;
    -b|--outbase)     OUTBASE="$2"; shift;;
    -D|--date)        DATE="$2"; shift;;
    -i|--iterations)  NLINES="$2"; shift;;
    --debug) set -x ;;
    *) echo >&2 "ERROR: option $1 unprocessed!";
       echo >&2 "aborting $0..."; exit 2;;
  esac
  shift
done

#
# find parameter file
# -------------------
if test $# -gt 0
then
  PARAFILE=$1
fi
if test ! -r $PARAFILE
then
  echo >&2 "Missing parameter file $PARAFILE"
  echo >&2 "aborting..."
  exit 2
fi
echo using parameter file $PARAFILE

COMMENT="$(head -n 1 $PARAFILE)"

CALEXBASE=$(basename $PARAFILE .par)
if test "$OUTBASE" = NSP
then
  OUTBASE=${CALEXBASE}
fi

calex $PARAFILE

# generate result plot
stuploxx --dev=${OUTBASE}_signals.ps/cps \
  -title="${COMMENT}" -tstitle=0.6 \
  -labh=0.22 -labr -labe -labc -labu \
  calex.einf p:1 ci:1 sf:1.1 f:seife:date=${DATE} u:${UNITS} \
    n:"signal proportional to current in calibration coil" \
  calex.ausf p:2 ci:1 sf:1.1 f:seife:date=${DATE} u:${UNITS} \
    n:"output of seismometer" \
  calex.synt p:3 ci:4 sf:1.1 f:seife:date=${DATE} u:${UNITS} \
    n:"simulated output" \
  calex.rest p:4 ci:2 sf:1.1 f:seife:date=${DATE} u:${UNITS} \
    n:"residual" \
  calex.ausf p:5 ci:1 sf:1.1 f:seife:date=${DATE} u:${UNITS} \
    n:"output of seismometer" \
  calex.synt p:5 ci:4 sf:1.1 f:seife:date=${DATE} u:${UNITS} \
    n:"simulated output" \
  calex.rest p:5 ci:2 sf:1.1 f:seife:date=${DATE} u:${UNITS} \
    n:"residual" 

# result data
calexoutextract.awk -v INI=${NLINES} ${CALEXBASE}.out > ${OUTBASE}_summary.out

a2ps -o ${OUTBASE}_summary.ps --center-title="${COMMENT}" ${OUTBASE}_summary.out

echo results are present in:
ls -1 ${CALEXBASE}.out ${OUTBASE}_summary.out ${OUTBASE}_summary.ps \
  ${OUTBASE}_signals.ps

# ----- END OF calex.sh ----- 
