#!/bin/gawk -f
# this is <calexoutextract.awk>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2013 by Thomas Forbriger (BFO Schiltach) 
# 
# Extract initial and final model parameters from calex output
#
# ----
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version. 
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# ----
#
# This program reads one or more output log files of calex and passes the
# essential lines to the standard output while skipping most iterations.
# Actually the file is passed to the output including the first INI
# iterations. Further iteration are skipped until the last iteration. The
# last iteration and all following lines are passed to the output. The default
# for INI is 3.
#
# To print all calex results from subdirectories:
#
#   calexoutextract.awk */calex.out 
#
# To print the calex output including the first 20 iterations:
#
#   calexoutextract.awk -v INI=20 */calex.out 
#
# If variable PSOUT is set, Postscript output files are produced:
#
#   calexoutextract.awk -v PSOUT=1 */calex.out 
# 
# REVISIONS and CHANGES 
#    20/12/2013   V1.0   Thomas Forbriger
#    18/10/2016   V1.1   start counting in interation list only
# 
# ============================================================================
#
BEGIN {
#
# hot indicates that a part of the input file is scanned which has to be
# reported to the output
  hot=1;
#
# switch to a2ps output is PSOUT != 0
  if (PSOUT)
  {
    output="a2ps -o calex.out.ps --center-title=calex.out";
  }
  else
  {
    output="cat";
  }
#
# if INI != 0 set a user selected number of iterations to be reported
  if (INI)
  {
    FIRSTITER=INI+1;
  }
  else
  {
    FIRSTITER=4;
  }
  FIRSTITERLINE=sprintf("^%5d", FIRSTITER);
#
# reset iteration counter
  ITER=0;
}
#
# ============================================================================
# main body of awk script (initial settings are done)
# ---------------------------------------------------
#
# in case we start a new file (FNR == 1)
# --------------------------------------
FNR == 1 {
  hot=1;
  ITER=0;
  if (PSOUT) {
    close (output);
    output=sprintf("a2ps -o %s.ps --center-title=%s", FILENAME, FILENAME);
  }
  print "\nNew file: " FILENAME;
  print "--------------------------------------------------";
}
# 
# ----------------------------------------------------------------------------
# stop reporting at largest iteration as selected in BEGIN block
# --------------------------------------------------------------
#
$0 ~ FIRSTITERLINE {
  if (ITER) 
  { 
    hot=0; 
    print "\n .\n .\n .\n" | output;
  } 
}
# 
# ----------------------------------------------------------------------------
# restart reporting after iteration has finished and RMS is reported
# ------------------------------------------------------------------
#
/^ iter         RMS/ { 
  ITER=1; 
  if (!hot) { hot=1; }
}
# 
# ----------------------------------------------------------------------------
# report any content if hot
# -------------------------
#
// {
  if (hot) { print | output; }
  }
# ----- END OF calexoutextract.awk ----- 
