# this is <Makefile>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2015 by Thomas Forbriger (BFO Schiltach) 
# 
# tests to run on calex
# 
# REVISIONS and CHANGES 
#    07/07/2015   V1.0   Thomas Forbriger
# 
# ============================================================================
#
# targets
# -------
#
#  bptest
#    tests the definition of the bp2 filter in calex

all:

flist: Makefile README $(wildcard *.par *.gpt)
	echo $^ | tr ' ' '\n' | sort > $@

.PHONY: edit
edit: flist; vim $<

.PHONY: clean
clean: ; 
	-find . -name \*.bak | xargs --no-run-if-empty /bin/rm -v
	-/bin/rm -vf flist
	-/bin/rm -vf *.sfe *.sff *.pdf *.ps *.dat

# ----------------------------------------------------------------------
# provide test data
#
test.sff: ; /bin/rm -fv $@; tesiff -r 4. -n 1000000 $@

step.sfe: test.sff; any2any --itype sff --otype seife --overwrite $@ $< t:1
spike.sfe: test.sff; any2any --itype sff --otype seife --overwrite $@ $< t:2
sine20s.sfe: test.sff; any2any --itype sff --otype seife --overwrite $@ $< t:3
sine200s.sfe: test.sff; any2any --itype sff --otype seife --overwrite $@ $< t:4
gauss.sfe: test.sff; any2any --itype sff --otype seife --overwrite $@ $< t:5
dmpsine.sfe: test.sff; any2any --itype sff --otype seife --overwrite $@ $< t:6
whitenoise.sfe: test.sff; any2any --itype sff --otype seife --overwrite $@ $< t:7
boxcar.sfe: test.sff; any2any --itype sff --otype seife --overwrite $@ $< t:8

%.ps: %.sfe; stuplox -ty seife -g -Y 0.8 -c dtiF -E -d $@/ps $<

%.pdf: %.ps; ps2pdf $<

%.pdp: %.pdf; okular $<; /bin/rm -fv $<

# ======================================================================
# test amplitude normalization of bandpass
#
# if f is frequency in Hz then a filter stage
#
# bp2
# per
# dmp
#
# in calex is defined by the response function
#
#   T(f) = per*f/(1+2*dmp*i*per*f-per*per*f*f),
#
# where i is the imaginary unit.

BPTESTSIGNAL=gauss.sfe
BPTESTSIGNAL=boxcar.sfe
BPTESTSIGNAL=spike.sfe

testdata.bp.sfe testdata.sfe: calex.bp.par $(BPTESTSIGNAL)
	/bin/cp -fv $(word 2,$^) testdata.sfe
	/bin/cp -fv $< calex.par
	calex
	/bin/mv calex.synt testdata.bp.sfe
	/bin/mv calex.einf testdata.sfe

bpdeconv.sff bpdeconv.dat: testdata.sfe testdata.bp.sfe
	deconv -v -o -transform=bpdeconv.dat -type seife bpdeconv.sff $^

bptest: bpdeconv.gpt bpdeconv.dat
	gnuplot -persist $<

# ----- END OF Makefile ----- 
