c  Drehung von Seismogramm-Komponenten

      program azimut12
      use nas_system
      parameter(ndim=400000)
c
      dimension x(ndim),y(ndim),u(ndim)
      character text*72,rest*16
      character nam0*24,nam1*24,nam2*24,nam3*24,nam4*4
      n=ndim

      if (iargc().ne.1) then
      write(6,1)
  1   format(' component rotation in the horizontal plane'/
     &' input file names must be *.n, *.e'/
     &' Azimut from E to N (ccw, mathematical definition)'/
     &' T is also ccw from R'//
     &' call: AZIMUT12 filename_without_extension')
      stop
      endif
      call getarg(1,nam0)
      nam1=trim(nam0)//'.e'
      nam2=trim(nam0)//'.n'

        call inpasc(nam1,text,n,dt,tmin,tsec,rest,x)

      if(n.eq.0.or.dt.eq.0.) then
      write(6,10)
   10 format('n or dt unspecified - check data header')
      stop
      endif

       call inpasc(nam2,text,n,dt,tmin,tsec,rest,y)

      if(n.eq.0.or.dt.eq.0.) then
      write(6,11)
   11 format('n or dt unspecified - check data header')
      stop
      endif

      open(8,file='aziplot.par')
      write(8,12)
   12 format('1,12,24,16,0,0,0.8')

      do iaz=0,11
      write(nam4,'(i4)') 1000+15*iaz
      nam4(1:1)='.'
      nam3=trim(nam0)//nam4
      azi=15.*iaz/45.*atan(1.)
      co=cos(azi)
      si=sin(azi)

c Koordinatendrehung

      do 100 i=1,n
      u(i)=x(i)*co+y(i)*si
  100 continue

c Ausgabe

      write(text(1:10),'("R az=",i4,1x)') nint(azig)
      call output(nam3,text,n,dt,tmin,tsec,u)
      write(8,'(a)') nam3
      enddo
      write(8,*) 'azimuth from east to north (mathematical definition)'
      close(8)
      
      write(6,'(a)') ' A plot-parameter file aziplot.par was generated'
      write(6,'(a)') ' type winplot aziplot.par to plot 12 directional s
     seismograms'

      stop
      end
c
      subroutine inpasc(name,text,n,dt,tmin,tsec,rest,x)
      dimension x(n)
      character iform*20,name*24,text*72,rest*16,pro*76
      open(7,file=name,status='old')
      read(7,'(a72)') text
   21 read(7,'(a76)') pro
      if(pro(1:1).ne.'%') goto 20
      goto 21
   20 read(pro,1) nn,iform,dt,tmin,tsec,rest
    1 format(i10,a20,3f10.3,a16)
      write(6,5) trim(name),nn,n
    5 format('reading file ',a,i7,' /',i7,' samples')
      if(n.lt.nn.or.nn.eq.0) then
      write(6,33) n
   33 format('number of samples was limited to ',i7)
      nn=n
      endif
      n=nn
      read(7,*) (x(j),j=1,n)
      return
      end
c
      subroutine output(name,text,n,dt,tmin,tsec,x)
      dimension x(n)
      character name*24,text*72,iform*20
      xmax=0.
      do 2 j=1,n
    2 xmax=max(xmax,abs(x(j)))
      nvor=log10(max(xmax,2.d0))+1.d0
      ndec=max(0,10-nvor)
      write(iform,'("(5f13.",i1,")")') ndec
      write(6,5) name
    5 format("writing data into file ",a)
      open(9,file=trim(name))
      write(9,'(a72)') text
      write(9,1) n,iform,dt,tmin,tsec
    1 format(i10,a20,f10.6,2f10.3)
      write(9,iform) (x(j),j=1,n)
      close(9)
      return
      end


