
      program summe
c
c   sum of three signals
c
c   convention for the input and output files:
c   - one header line, up to 72 characters
c   - up to 65 lines signal description and processing log
c     these lines have " }}   " as the first five characters
c     (the processing log is updated by the present program)
c   - one line specifying the number of samples, the format, the samplin
c     interval, and the start time in min and sec after midnight. this
c     line may be repeated after the data when its contents is not known
c     at the time when the file header is written; however the format
c     must always be specified.
c   - data in the specified format
c
      parameter(ndim=72000)
      implicit real*8 (a-h,o-z)
      dimension x(ndim),y(ndim),z(ndim),w(ndim)
      character text*72,rest*16
      character*24 nam1,nam2,nam3,nam4

        write(6,'(a)') 'Name des ersten Inputfiles?'
        read(5,'(a)') nam1
        write(6,'(a)') 'Amplitudenfaktor?'
        read(5,*) fac1 
        write(6,'(a)') 'Name des zweiten Inputfiles?'
        read(5,'(a)') nam2
        write(6,'(a)') 'Amplitudenfaktor?' 
        read(5,*) fac2
        write(6,'(a)') 'Name des dritten Inputfiles?'
        read(5,'(a)') nam3
        write(6,'(a)') 'Amplitudenfaktor?' 
        read(5,*) fac3
        write(6,'("Name des Outputfiles?")')
        read(5,'(a)') nam4

      n1=ndim
      n2=ndim
      n3=ndim

      call input(nam1,text,n1,dt1,tmin,tsec,rest,x)
      call input(nam2,text,n2,dt2,tmin,tsec,rest,y)
      call input(nam3,text,n3,dt3,tmin,tsec,rest,z)
      if(n2.ne.n1.or.n3.ne.n1.or.dt2.ne.dt1.or.dt3.ne.dt1) stop
      dt=dt1
      n=min(n1,n2,n3)
      write(6,5) fac1,nam1,fac2,nam2,fac3,nam3
    5 format('calculate ',f10.6,'*',a," + ",f10.6,'*',a," + ",f10.6,
     &'*',a)

c  add
      do 1 i=1,n
    1   w(i)=fac1*x(i)+fac2*y(i)+fac3*z(i)
      call output(nam4,text,n,dt,tmin,tsec,rest,w)
      stop
      end

c     ftn subroutines for reading and writing standard data files
c
      subroutine input(name,text,n,dt,tmin,tsec,rest,x)
      implicit real*8 (a-h,o-z)
      dimension x(n)
      character iform*20,name*24,text*72,rest*16,pro*80
      open(7,file=name,status='old')
      read(7,'(a72)') text
   21 read(7,'(a80)') pro
      if(pro(1:1).eq.'%') goto 21
   20 read(pro,1) nn,iform,dt,tmin,tsec,rest
    1 format(i10,a20,3f10.3,a16)
      write(6,5) name,nn,n
    5 format("file ",a," wird gelesen:",i7," /",i7," punkte")
      if(n.lt.nn.or.nn.eq.0) then
      write(6,'("punktzahl auf",i7," festgesetzt")') n
      nn=n
      endif
      n=nn
      do 97 j=1,n
   97 x(j)=0.d0
      nin=0
      read(7,iform,end=2,err=7) (x(j),j=1,n)
      goto 3
    7 read(7,1,end=2,err=7) n,iform,dt,tmin,tsec,rest
      goto 3
    2 n=j+nin-1
      write(6,'("punkzahl durch datenende bestimmt")')
    3 close(7)
      write(6,'("daten sind eingelesen:",i7," punkte")') n
      return
      end
      
      subroutine output(name,text,n,dt,tmin,tsec,rest,x)
c  formatted output
      implicit real*8 (a-h,o-z)
      dimension x(n)
      character name*24,text*72,iform*20,rest*16
      xmax=0.
      do 2 j=1,n
    2 xmax=max(xmax,abs(x(j)))
      nvor=log10(max(xmax,2.))+1.
      ndec=max(0,10-nvor)
        write(iform,3) ndec
    3   format('(5f13.',i1,')')
      write(6,5) name,n,iform
    5 format('File ',a,' wird geschrieben,',i7,' Punkte im Format ',a)
      open(8,file=name)
        write(8,'(a)') text
        write(8,1) n,iform,dt,tmin,tsec,rest
    1   format(i10,a20,f10.3,2f10.3,a16)
        write(8,iform) (x(j),j=1,n)
      close(8)
      write(6,'("fertig!")')
      return
      end
