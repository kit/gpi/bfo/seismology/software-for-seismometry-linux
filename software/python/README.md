# Python framework for `calex`, `dispcal`, and `tiltcal`
This subdirectory contains [Juypter](https://jupyter.org/) notebooks as used in the Observatory Course at [Black Forest Observatory (BFO)](https://www.gpi.kit.edu/english/61.php).
They use [ObsPy](http://obspy.org) for time series data I/O and display of signals. 

## Contents of the directory
### seife format I/O module
Practically all programs provided by Erhard Wielandt read and write
time series data in 'seife' format.
In order to use these programs in an [ObsPy](http://obspy.org) environment, 
a module is needed, which is able to read and write time series data
in 'seife' format.

|file          |purpose                                  |
|:-------------|:----------------------------------------|
|`seifeio.py`  |seife format I/O of `obspy.Trace` objects|
|`testseife.py`|program to test `seifeio.py`             |

### A lesson example for `calex`

|file                               |purpose                                               |
|:----------------------------------|:-----------------------------------------------------|
|`calex_create_parameter_file.ipynb`|a Jupyter notebook to prepare a `calex` parameter file|
|`calex_run_program.ipynb`          |a Jupyter notebook to run a `calex` lesson example    |
|`calex.par`                        |a fully prepared parameter file for the toy example   |
|`calexiterextract.awk`             |a copy of the awk.script from subdirectory calex      |


### A lesson example for `dispcal`

|file                                 |purpose                                                 |
|:------------------------------------|:-------------------------------------------------------|
|`dispcal_create_parameter_file.ipynb`|a Jupyter notebook to prepare a `dispcal` parameter file|
|`dispcal_run_program.ipynb`          |a Jupyter notebook to run a `dispcal` lesson example    |
|`dispcal.par`                        |a fully prepared parameter file for the toy example     |

### A lesson example for `tiltcal`

|file                                 |purpose                                             |
|:------------------------------------|:---------------------------------------------------|
|`tiltcal_run_program.ipynb`          |a Jupyter notebook to run a `tiltcal` lesson example|
|`tiltcal.par`                        |a fully prepared parameter file for the toy example |
