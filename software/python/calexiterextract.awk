#!/bin/gawk -f
# this is <calexiterextract.awk>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2014 by Thomas Forbriger (BFO Schiltach) 
# 
# extract values for all iterations from a calex.out file
#
# ----
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version. 
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# ----
#
# This program reads one calex.out log file and extracts the numerical values
# for the progress obtained in all inversion parameters during iteration. This
# values are provided on the terminal as an ASCII table appropriate for
# gnuplot script sg056iter.gpt or similar. The standard invocation is
#
#   calexiterextract.awk calex.out > sg056iter.dat
#
# Invoking the program by
#
#   calexiterextract.awk -v MODE="persearchrange" calex.out > sg056iter.dat
#
# provides the inversion progress in realtive parameter change per search
# range. This might be of interest, when checking appropriateness of search
# ranges. Notice the these values cannot be used to calculate the location of
# poles and zeroes!
# 
# REVISIONS and CHANGES 
#    20/01/2014   V1.0   Thomas Forbriger
#    25/03/2023   V1.1   bug fix: ignore second line with final system
#                        parameters as is output by calex since version
#                        2020-07-03
# 
# ============================================================================
#
# function to check derived final parameters
function abs(a) {
  rv=a;
  if (a<0) { rv = -1 * a; }
  return rv;
}
#
function amax(a,b) {
  rv = abs(a);
  if (rv < abs(b)) { rv = abs(b); }
  return rv;
}
#
function dev(a, b) {
  deno=amax(a,b);
  if (deno < 1.e-20) 
  {
    rv = 0;
  }
  else
  {
    rv=(a - b) / deno;
  }
  return rv;
}
#
# ----------------------------------------------------------------------------
#
BEGIN {
#
# set to 1 for DEBUG output
  DEBUG=0;
#
# set current section to undefined
  SEC=-1;
# accepted section index values:
# 1: initial column headers
# 2: initial values
# 3: search ranges
# 4: iteration block
# 5: final column headers
# 6: final values
# 7: after first report of final values
#
# set current iteration
  ITER=0;
#
# number of columns in calex output
  NCOL=0;
#
# initial iteration line pattern with non-matching character sequence
  ITERPATTERN="^XXXXXXXXXXXX";
}
#
# ============================================================================
# start scanning
# --------------
#
# detect first line of section
# ============================
#
# header keys initial or final
# ----------------------------
/^ iter         RMS/ {
  if (SEC<0) 
  { 
    SEC=1;
    NCOL=NF;
# extract column headers of first header line
    for (i=1; i<=NF; ++i)
    {
      COLHEAD[i]=$i;
    }
  } 
  else
  {
    SEC=5;
# last iteration will be reported next, but with full final values
    ITER -= 1;
    ITERPATTERN=sprintf("^%5d ", ITER);
  }
  if (DEBUG>1) { print; }
  next;
}
#
# initial values
# --------------
/^    0 / {
  SEC=2;
  for (i=1; i<=NF; ++i)
  {
    INIVAL[i]=$i;
  }
  NINI=NF;
#  printf "# ";
  for (i=1; i<=NCOL; ++i)
  {
    printf("%s ",COLHEAD[i]);
  }
  printf"\n";
  next;
}

# search ranges
# -------------
/^               \+- / {
  if (DEBUG>1) { print "search range!"; }
  if (SEC == 2)
  {
    NSR=2;
    SEARCHRANGE[1]=0;
    SEARCHRANGE[2]=0;
# output initial values
    printf("%d ",INIVAL[1]);
    for (i=2; i<=NCOL; ++i)
    {
      printf("%f ",INIVAL[i]);
    }
    printf "\n";
  }
  for (i=2; i<=NF; ++i)
  {
    SEARCHRANGE[NSR + i - 1]=$i;
  }
  NSR += NF - 1;
  SEC=3;
  ITER=1;
  ITERPATTERN=sprintf("^%5d ", ITER);
  if (DEBUG) { print "ITERPATTERN: \"" ITERPATTERN "\""; }
  next;
}
# iteration
# ---------
$0 ~ ITERPATTERN {
  if (SEC < 5)
  {
    SEC=4;
    if (DEBUG>1) { print; print "in iter"; }
    for (i=1; i<=NF; ++i)
    {
      VAL[i]=$i;
    }
    NVAL=NF;
    ITER += 1;
    ITERPATTERN=sprintf("^%5d ", ITER);
  }
  else if (SEC == 5)
  {
    SEC=6;
    if (DEBUG>1) { print; print "in iter, capture final values"; }
    for (i=1; i<=NF; ++i)
    {
      FINVAL[i]=$i;
    }
    NFINVAL=NF;
  }
  else
  {
    SEC=7;
    # if non of the above conditionals matches, the block was triggered by the
    # second report of final values as is output by calex since version
    # 2020-07-03. This line is ignored. Disable any further parsing by setting
    # SEC to 7 and ITERPATTERN to a non-matching sequence.
    ITERPATTERN="^XXXXXXXXXXXX";
  }
}

# operate on values within section
# ================================
# (no specific initial sequence of line)
# calex puts values or column header strings for five parameters on one single
# line. If there are more parameters it puts the rest on additional lines with
# no specific initial sequence (first 10 characters are whitespace). This
# block is triggered by such lines. How to handle the lines is indicated by
# the variable SEC which is set previously on the first line of the section.
/^                 / {
  switch (SEC) {
# initial column headers
    case 1:
      for (i=1; i<=NF; ++i)
      {
        COLHEAD[NCOL + i]=$i;
      }
      NCOL += NF;
      break;
# initial values
    case 2:
      for (i=1; i<=NF; ++i)
      {
        INIVAL[NINI + i]=$i;
      }
      NINI += NF;
    break;
# search ranges
    case 3:
    print "ERROR: search range line should not match!";
    next;
    break;
# iteration block
    case 4:
      for (i=1; i<=NF; ++i)
      {
        VAL[NVAL + i]=$i;
      }
      NVAL += NF;
    break;
# final column headers
    case 5:
    next;
    break;
# final values
    case 6:
      for (i=1; i<=NF; ++i)
      {
        FINVAL[NFINVAL + i]=$i;
      }
      NFINVAL += NF;
    break;
    default:
    print "WARNING: missed block type " SEC "!";
    next;
  }
}
# ----------------------------------------------------------------------------
# check whether all values are read
# should be triggered also in cases where only one line per iteration is
# written
// {
  if (DEBUG>2) { print "in SEC " SEC; }
  if (SEC == 2)
  {
    if (NINI == NCOL)
    {
      if (MODE == "persearchrange")
      {
# this might appear silly, but in 'persearchrange' mode the initial values are
# just zero
        printf("%d ",0);
        printf("%f ",0);
        for (i=3; i<=NCOL; ++i)
        {
          printf("%f ",0);
        }
        printf "\n";
      }
      else
      {
        printf("%d ",0);
        printf("%f ",INIVAL[2]);
        for (i=3; i<=NCOL; ++i)
        {
          printf("%f ",INIVAL[i]);
        }
        printf "\n";
      }
    }
  }
  else if (SEC == 4)
  {
    if (NVAL == NCOL)
    {
      if (MODE == "persearchrange")
      {
        printf("%d ",VAL[1]);
        printf("%f ",VAL[2]);
        for (i=3; i<=NCOL; ++i)
        {
          printf("%f ",VAL[i]);
        }
      }
      else
      {
        printf("%d ",VAL[1]);
        printf("%f ",VAL[2]);
        for (i=3; i<=NCOL; ++i)
        {
          printf("%f ",VAL[i] * SEARCHRANGE[i] + INIVAL[i]);
        }
      }
      printf "\n";
# increase NVAL by 1 to avoid retriggering of this section
      NVAL += 1;
    }
  }
  else if (SEC == 6)
  {
    if (NFINVAL == NCOL)
    {
      printf("#        %3s %6s %11s %11s %8s\n", "no.", "par",
             "value", "deriv. val.", "residual");
      printf("# final: %3d %6s %11.7f %11.7f   %5.2f%\n", 2,
             COLHEAD[2], FINVAL[2], VAL[2],
             100*dev(VAL[2], FINVAL[2]));
      for (i=3; i<=NCOL; ++i)
      {
        printf("# final: %3d %6s %11.7f %11.7f   %5.2f%\n", i,
               COLHEAD[i], FINVAL[i],
               VAL[i] * SEARCHRANGE[i] + INIVAL[i], 
               100*dev(VAL[i] * SEARCHRANGE[i] + INIVAL[i], FINVAL[i]));
      }
      printf "\n";
      NFINVAL = 0;
    }
  }
}
#
# ============================================================================
END {
  if (DEBUG)
  {
    for (i=1; i<=NCOL; ++i)
    {
      print "COLHEAD[" i "]: " COLHEAD[i];
    }
  }
}
# ============================================================================
# Format of the iterations block in the file to be scanned. The hash tag must
# be deleted, i.e. each line starts with a single blank.
#
# -----
#
# misfit from start model:
# writing file rest
# 
# iter         RMS         amp         del         plp         hlp         plp
#                          hlp         plp         hlp         plp         hlp
# 
#    0    0.051710    1.000000    0.000000    9.099180    0.988060    8.833920
#                     0.893550    8.258500    0.703380    7.390980    0.404802
#               +-    0.500000    0.010000    1.000000    0.010000    1.000000
#               +-    0.010000    1.000000    0.010000    1.000000    0.010000
# 
# 
#    1    0.047760   -0.039046    0.000052    0.001695    0.000142    0.001593
#                     0.000144    0.001332    0.000146    0.000742    0.000144
#    2    0.044152   -0.005157    0.008957    0.286338    0.023581    0.270220
#                     0.024013    0.227982    0.024580    0.130224    0.024404
#    3    0.040857   -0.038128    0.009001    0.287762    0.023702    0.271562
#                     0.024136    0.229113    0.024706    0.130871    0.024527
# 
# .
# .
# .
# 
# iter         RMS         amp         del         plp         hlp         plp
#                          hlp         plp         hlp         plp         hlp
#  108    0.003147    0.982706    0.000682   11.099974    0.989818   10.726266
#                     0.895326    9.878787    0.705166    8.423401    0.406519
# 
# QUAD called 2670 times: converged
# output signal for final model...
# writing file synt
# residual error...
#
# -----
#
# ----- END OF calexiterextract.awk ----- 
