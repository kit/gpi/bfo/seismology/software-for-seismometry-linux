#!/usr/bin/env python3
# this is <testseife.py>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2023 by Thomas Forbriger (KIT, GPI, BFO) 
# 
# test seife I/O
# 
# REVISIONS and CHANGES 
#    27/02/2023   V1.0   Thomas Forbriger
# 
# ============================================================================
#

VERSION="2023-02-27"
#
import argparse
import os
from obspy import UTCDateTime
import numpy as np
import seifeio

def main():
    parser = argparse.ArgumentParser(
            description="Test seife I/O, version %s" % VERSION)

    parser.add_argument("file",
            default=None, nargs=1,
            help="read data from file")

    parser.add_argument(
            "--verbose", action='store_true',
            help="produce verbose progress report to terminal"
            )

    parser.add_argument(
            "--write", default=None,
            metavar='file',
            help="write time series to 'file'"
            )

    args = parser.parse_args()

    for infile in args.file:
        print("read from %s" % infile)

        tr=seifeio.read(infile, debug=True)
        tr=seifeio.read(infile, date="2023-02-12",
                        network="XX", station="BFO", channel="LHZ")
        print(tr.stats)
        tr.plot()

    if args.write is not None:
        print("write to file %s" % args.write)
        seifeio.write(args.write, tr, debug=True)

if __name__ == "__main__":
    main()

# ----- END OF testseife.py ----- 
