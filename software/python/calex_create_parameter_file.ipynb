{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Create a parameter file for `calex`\n",
    "\n",
    "<table style=\"width:100%\"> \n",
    "  <tr>       \n",
    "    <td style=\"text-align:left\">\n",
    "        This work is licensed under a \n",
    "        <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\">Creative Commons \n",
    "            Attribution-ShareAlike 4.0 International License</a>.\n",
    "    </td>    \n",
    "    <td width=120px rowspan=\"3\">\n",
    "        <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"><img alt=\"Creative Commons License\" style=\"border-width:0\" src=\"https://i.creativecommons.org/l/by-sa/4.0/88x31.png\" /></a>\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "     <td style=\"text-align:left\">\n",
    "        Copyright (c) 2022 by \n",
    "        <a href=\"https://www.gpi.kit.edu/english/62_117.php\">Mike Lindner (KIT, GPI,BFO)</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td style=\"text-align:left\">\n",
    "        Copyright (c) 2023 by \n",
    "        <a href=\"https://www.gpi.kit.edu/english/62_102.php\">Thomas Forbriger (KIT, GPI,BFO)</a>\n",
    "    </td>\n",
    "  </tr>  \n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Background\n",
    "This Jupyter notebook shall support you in creating a configuration file `calex.par` for program `calex`.\n",
    "You may as well use any text editor to create and modify `calex.par`.\n",
    "\n",
    "The seismometer calibration program `calex` is an original Fortran-code by Erhard Wieland.\n",
    "This JN page is in large parts a copy of the [`calex` manual](https://gitlab.kit.edu/kit/gpi/bfo/seismology/software-for-seismometry-linux/-/blob/master/software/calex/calex.doc)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Current working directory\n",
    "The Jupyter notebook and all programs called from within are operating on files in the current working directory.\n",
    "The following command prints the path to this directory.\n",
    "If your files should be located somewhere else, copy them from this directory or change the working directory by calling `os.chdir()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"The program will operate on files in %s\" % os.getcwd())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Format of the parameter file\n",
    "\n",
    "`calex.par` contains different types of lines:\n",
    "* a header line. Printed but not evaluated, except if it begins with `<pzmode>`, indicating a slightly enhanced version of the parameter file where the number of first-order pole-zero pairs, `mz1`, and that of second-order pairs, `mz2`, are listed after parameters `m1` and `m2`. These two numbers are only used for a consistency check. Otherwise, `<pzmode>` makes no difference.      \n",
    "* two lines with the names (paths) of the input (stimulus) and output files. File names containing non-alphanumeric characters (if accepted by the operating system) should be enclosed in single quotes beginning in the first column.\n",
    "* An optional line with the keyword `trace` (without quotes), causing verbose echoing of the input parameters    \n",
    "* four lines with required parameters for the inversion          \n",
    "* a variable number of lines describing the system as a chain of first-order and second-order modules (subsystems), each module requiring a keyword line defining its type (such as `hp2` for a second-order high-pass) and one, two, or four lines with three parameters each, and finally,            \n",
    "* the end line (with the keyword `end`, without quotes)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Create content dictionary `cont`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont = {}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Header information\n",
    "Printed but not evaluated, except if it begins with `<pzmode>`, indicating a slightly enhanced version of the parameter file where the number of first-order pole-zero pairs, `mz1`, and that of second-order pairs, `mz2`, are listed after parameters `m1` and `m2`. These two numbers are only used for a consistency check. Otherwise, `<pzmode>` makes no difference.      \n",
    "\n",
    "##### Note     \n",
    "For this course, we will restrict to the standard inversion routine. `pzmode` is therefore not relevant for us. \n",
    "\n",
    "Define a header line containing basic information about the used seismometer, e.g. sensor type and serial number."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['header'] = 'calibration of STS-1V SN1828'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data files\n",
    "`calex` takes two data files as input:          \n",
    "* synthetic stimulus (forcing) signal (e.g. downsweep), the input to the seismometer        \n",
    "* the recorded seismometer output                 \n",
    "\n",
    "Both files must be provided in the [seife](https://www.black-forest-observatory.de/ThomasForbriger/doc/libdatrwxx/html/page_seife_format.html) format.\n",
    "The names of these files must be consistent with the names used in the corresponding Juypter notebook\n",
    "[calex_run_program.ipynb](calex_run_program.ipynb).\n",
    "\n",
    "### Input file name \n",
    "The file contains the synthetic stimulus (forcing) signal. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['input'] = 'input.sfe'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Output file name\n",
    "\n",
    "The file contains the recorded output signal of the seismometer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['output'] = 'output.sfe'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Control Parameter\n",
    "\n",
    "### Anti-alias corner period\n",
    "#### `alias`   \n",
    "The corner period of the numerical anti-alias filter that is part of the CALEX routine. Must be at least four times larger than the sampling interval. This filter is required because the bandwidth of the simulated system must be smaller than the Nyquist bandwidth. The program determines the order (steepness) of the filter so that the Nyquist condition is satisfied.    \n",
    "\n",
    "##### Note    \n",
    "The parameter only needs to be changed if you should choose data with a sampling interval of 1s or larger."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['alias'] = 2.0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Number of active inversion parameters\n",
    "#### `m`    \n",
    "The number of active (unknown) parameters in the inversion. The set of parameters comprises a gain factor, a constant delay, two parameters for the correction of  experimental biases, plus the unknown corner periods and damping constants. Any selection of these may be declared active, that is, be specified with a nonzero uncertainty. The program will fit only the active parameters and leave the passive unchanged. m is an integer with m $\\in [0,6]$.\n",
    "\n",
    "##### Note    \n",
    "We usually use `amp`, `del`, `per`, and `dmp` as free parameters of the model.\n",
    "The value `m` needs to be changed only if you choose a different set of inversion parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['m'] = 4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Number of system parameters\n",
    "\n",
    "#### `m0`\n",
    "Number of additional powers of the Laplace variable s in the numerator of the transfer function, equivalent to forming the m0-th time derivative of the signal. \n",
    "    \n",
    "#### `m1`\n",
    "Number of first-order high-pass or low-pass subsystems in the transfer function. A corner period must subsequently be specified for each of these.\n",
    "\n",
    "#### `m2`\n",
    "Number of second-order high-pass, low-pass or band-pass subsystems in the transfer function. A corner period and a damping constant (fraction of critical damping) must subsequently be specified for each of these.\n",
    "\n",
    "##### Note\n",
    "These settings need only be changed, if you change the system description (add additional filters, etc)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['m0'] = 0\n",
    "cont['m1'] = 0\n",
    "cont['m2'] = 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `mz1` and `mz2`\n",
    "Number of  first- and second-order pole-zero pairs. \n",
    "Set to `None` if pz mode is not used!\n",
    "\n",
    "##### Note         \n",
    "We do not make use of these parameters in the current course."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['mz1'] = None\n",
    "cont['mz2'] = None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Maximum number of iterations\n",
    "#### `maxit`       \n",
    "The maximum number of iterations in the conjugate-gradient optimization procedure.\n",
    "\n",
    "##### Note\n",
    "You may in/decrease this parameter if needed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['maxit'] = 100"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Termination criteria\n",
    "\n",
    "#### `qac`\n",
    "The iteration stops when the improvement in the rms misfit in a certain number of steps becomes less than qac.      \n",
    "#### `finac`\n",
    "The iteration stops when the the normalized parameters change by less than finac.\n",
    "\n",
    "Both criteria must be fulfilled to terminate the iteration.\n",
    "\n",
    "##### Note \n",
    "It is not necessary to change these parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['qac'] = 1.e-4\n",
    "cont['finac'] = 1.e-3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Residual time window definition\n",
    "#### `ns1`    \n",
    "First sample relative to begin of full time window.     \n",
    "#### `ns2`  \n",
    "Last sample relative to begin of full time window.\n",
    "\n",
    "The residual is minimised in a time window from sample ns1 to sample ns2. Values 0 mean first resp. last sample. If the system was not quiet at the beginning of the record, ns1 must be chosen so that the transient response to all previous input is ignored.\n",
    "\n",
    "##### Note\n",
    "If you find transients at the beginning of the residual, you might like to get rid of the effect on the calibration result by setting `ns1` to a finite value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['ns1'] = 800\n",
    "cont['ns2'] = 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## System parameter\n",
    "\n",
    "\n",
    "##### Note\n",
    "A system parameter needs two input values:    \n",
    "* initial value of the parameter             \n",
    "* estimated uncertainty (the search range)   \n",
    "\n",
    "If the parameter should be treated as a passive one, set uncertanty to zero. \n",
    "Parameters with a finite search range are active (free) parameters of the inversion."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Gain factor\n",
    "\n",
    "#### `amp`\n",
    "A gain factor (not the generator constant of the sensor). The start value may be \n",
    "estimated from the amplitude ratio between output and input.\n",
    "The meaning of the 'amp' parameter is different for different experiments\n",
    "(electrical or mechanical input, etc.). When a broadband-velocity seismometer\n",
    "with the asymptotic (practically, mid-band) generator constant G is tested \n",
    "on a shake table that has a displacement transducer, two possible cases are:              \n",
    "\n",
    "**a)** the displacement transducer has an electric output of D volts per meter\n",
    "and its output signal is recorded with the same digitizer as the seismo- \n",
    "meter output. Then amp = G / D.          \n",
    "\n",
    "**b)** The displacement transducer is digital, or has a separate digitizer, with\n",
    "a responsivity A counts per meter. The seismometer output is digitized \n",
    "with C counts per volt. Then amp = C * G / A.    \n",
    "\n",
    "##### Note\n",
    "Because the gain of the calibration coil is unknown in our case, the value of `amp` is meaningless with respect to the seismometer.\n",
    "Nevertheless this parameter is needed to properly adjust the amplitude of the simulated signal.\n",
    "The initial value should be chosen as close as possible to the final value.\n",
    "This should always be an active (free) parameter with finite search range."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['amp'] = [3.7,0.50]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Time delay\n",
    "\n",
    "#### `del`  \n",
    "A time delay is normally used to describe the delay of low-pass filters in the system that\n",
    "are not explicitly specified. Any skew in the sampling of the digitizer channels, or other differential delays, may also be included here. If you don’t want to use this parameter, set its initial value and uncertainty to zero. Its use is however recommended because this improves the mathematical model of your system.\n",
    "\n",
    "##### Note \n",
    "Depending on the starting solution, you may run into issues with this parameter. The code gives you a hint on how to solve these issues by reducing its search range. The following lines are a likely displayed error message on the `terminal` window:         \n",
    "\n",
    "`\n",
    " Delay exceeds 24*dt.\n",
    " Use a smaller search range for the delay parameter.\n",
    " A range smaller than the sampling interval is recommended.\n",
    " If this does not help, then you must edit the time series.\n",
    " Remove an appropriate number of samples from the beginning of either the input or the output signal to reduce the delay.\n",
    "`            \n",
    "\n",
    "Avoid changing this parameter and adjust the settings for `maxit` and below filter system if this error message arises. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['del'] = [0.014,0.01]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Subtraction fraction \n",
    "\n",
    "#### `sub`          \n",
    "The fraction ‘sub’ of the input is subtracted from the output, to numerically simulate a \n",
    "full bridge when a half-bridge circuit was used. Set start value and uncertainty to zero if no bridge circuit was used. This parameter becomes relevant for electro dynamic seismometers (e.g. Geophone).\n",
    "\n",
    "##### Note\n",
    "This parameter is used only in calibration of passive electrodynamic seismometers (geophones) and is not used in the current example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['sub'] = [0.,0.]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Subtrace tilting effect of shake table\n",
    "\n",
    "#### `til`       \n",
    "This parameter permits to subtract ‘til’ times the twice-integrated synthetic output from the original synthetic output signal, to compensate for the tilt of a shake table. Set start value and uncertainty to zero if you did not use a shake table. til is in units of microradians per millimeter of table motion provided that the motion of the table, the gravitational force coupled in by tilt, and the sensitive axis of the seismometer have the same direction. More generally, til is (tilt along the sensitive axis) per (table displacement along the sensitive axis), in the above units.\n",
    "\n",
    "##### Note   \n",
    "This parameter is used only if a horizontal seismometer is excited by actual motion.\n",
    "It is not used in the context of this course."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['til'] = [0.,0.]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Filter system\n",
    "\n",
    "\n",
    "The aim of a CALEX inversion is the relative modeling of two descriptive system parameters of the seismometer response function:\n",
    "* Eigenperiod $T_0$           \n",
    "* Damping $h$           \n",
    "\n",
    "As CALEX is using a non-linear inversion, the quality of the starting parameters is crucial for the routine to find the global minima of the error function. CALEX hereby allows you to define a compilation of different filter systems (multiple eigenperiods and damping) to describe differently complex devices. \n",
    "\n",
    "The broadband output (BRB) of a velocity broad-band feedback seismometer presents a signal which is a band-passed filtered version of the input acceleration.\n",
    "Consider the amplitude response of the output voltage of a seismometer with electrodynamic transducer being displayed with respect to input acceleration.\n",
    "This type of system can then be described by a second-order bandpass subsystem (bp2).\n",
    "\n",
    "For detailed information of different implemented filter systems see the official [calex manual](https://gitlab.kit.edu/kit/gpi/bfo/seismology/software-for-seismometry-linux/-/blob/master/software/calex/calex.doc).\n",
    "\n",
    "##### Note    \n",
    "This is the setting you want to study. Vary only one starting parameter or step width at a time to keep your study straightforward."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['bp2'] = {            # second-order bandpass\n",
    "            'per':[        # eigenperiod\n",
    "                    20.,   # T0\n",
    "                    0.5    # dT0\n",
    "                  ],  \n",
    "            'dmp':[        # damping\n",
    "                    0.707, # h\n",
    "                    0.05   # dh\n",
    "                  ]\n",
    "               } "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Write `calex.par`\n",
    "Define a function to write the parameters to file `calex.par`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def write_calex_par(content):\n",
    "    '''\n",
    "        write calex parameter file calex.par \n",
    "        \n",
    "        calex.f uses Fortran free format when parsing lines in calex.par\n",
    "        Just make sure to separate different entries in one line by whitespace. \n",
    "\n",
    "        Parameters\n",
    "        ----------\n",
    "        content : dictionary\n",
    "            dictionary with parameters to be written to file\n",
    "    '''\n",
    "    # the directory to which the parameter file shall be written\n",
    "    path2file='./'\n",
    "    # order of block 1 control parameters\n",
    "    param_b1 = ['alias','m','m0','m1','m2','mz1','mz2'\n",
    "                ,'maxit','qac','finac','ns1','ns2']\n",
    "    # oder of block 2 iteration parameters\n",
    "    param_b2 = ['amp','del','sub','til','Set']\n",
    "    # order of filter settings\n",
    "    param_b3 = ['lp1', 'hp1', 'pz1', 'lp2', 'bp2', 'hp2', 'pz2']\n",
    "    # order of filter subsettings\n",
    "    #param_b3_sub = {'pz1':['tp1','tz1'],'pz2':['tp2','tz2','dp2','dz2']}\n",
    "    \n",
    "    # write file\n",
    "    with open(path2file+'calex.par', \"w\") as f:\n",
    "        f.write('%s \\n' %(content['header'])) # header information\n",
    "        f.write('\\n') # empty line after header info\n",
    "        f.write('{:<20s} input signal (forcing) to the seismometer\\n'.format(content['input'])) # input filename\n",
    "        f.write('{:<20s} recorded output signal of the seismometer\\n'.format(content['output'])) # output filename\n",
    "        f.write('\\n') # empty line after file info\n",
    "        \n",
    "        # parameter block 1\n",
    "        for key in param_b1:\n",
    "            try:\n",
    "                if content[key] != None:\n",
    "                    if key in ['finac','qac']:\n",
    "                        # Fortran does not accept a leading zero in the exponent\n",
    "                        mantissa, exponent=(\"{:<7.1e}\".format(content[key])).split(\"e\")\n",
    "                        if (exponent[0:1]==\"-\" or exponent[0:1]==\"+\"):\n",
    "                            exponent=exponent[0:1]+exponent[1:].lstrip(\"0\")\n",
    "                        else:\n",
    "                            exponent=exponent.lstrip(\"0\")\n",
    "                        f.write(\"{:<7s} {:<s}\\n\".format(mantissa+\"e\"+exponent, key))\n",
    "                    elif key in ['m','m0','m1','m2','mz1','mz2','maxit','ns1','ns2']:\n",
    "                        f.write(\"{:<7d} {:<s}\\n\".format(content[key], key))\n",
    "                    else:\n",
    "                        f.write(\"{:<7s} {:<s}\\n\".format(str(content[key]), key))\n",
    "                                                \n",
    "            except:\n",
    "                continue\n",
    "        \n",
    "        # empty line after parameter block 1                    \n",
    "        f.write('\\n') \n",
    "                            \n",
    "        # parameter block 2\n",
    "        for key in param_b2:\n",
    "            try:\n",
    "                if content[key] != None:\n",
    "                    val = content[key]\n",
    "                    f.write(\"{0} {1} {2}\\n\".format(key.ljust(4),\n",
    "                                        str(val[0]).ljust(10),\n",
    "                                        str(val[1]).ljust(10)))\n",
    "            except:\n",
    "                continue\n",
    "        \n",
    "        # empty line after parameter block 2                    \n",
    "        f.write('\\n')\n",
    "        \n",
    "        # filter info\n",
    "        for key in param_b3:\n",
    "            try:\n",
    "                if content[key] != None:\n",
    "                    f.write('%s\\n' %(key))\n",
    "                    for skey in content[key]:\n",
    "                        val = content[key][skey]\n",
    "                        f.write(\"{0} {1} {2}\\n\".format(skey.ljust(4),\n",
    "                                            str(val[0]).ljust(10),\n",
    "                                            str(val[1]).ljust(10)))\n",
    "                    # empty line after filter info                    \n",
    "                    f.write('\\n') \n",
    "            except:\n",
    "                continue\n",
    "                            \n",
    "        # empty line after filter info                    \n",
    "        f.write('\\n') \n",
    "                            \n",
    "        # end calex.par\n",
    "        f.write('end\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Call the function to create `calex.par`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "write_calex_par(cont)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
