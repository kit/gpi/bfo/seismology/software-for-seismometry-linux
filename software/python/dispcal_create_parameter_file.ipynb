{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Create a parameter file for `dispcal`\n",
    "\n",
    "<table style=\"width:100%\"> \n",
    "  <tr>       \n",
    "    <td style=\"text-align:left\">\n",
    "        This work is licensed under a \n",
    "        <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\">Creative Commons \n",
    "            Attribution-ShareAlike 4.0 International License</a>.\n",
    "    </td>    \n",
    "    <td width=120px rowspan=\"3\">\n",
    "        <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"><img alt=\"Creative Commons License\" style=\"border-width:0\" src=\"https://i.creativecommons.org/l/by-sa/4.0/88x31.png\" /></a>\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "     <td style=\"text-align:left\">\n",
    "        Copyright (c) 2022 by \n",
    "        <a href=\"https://www.gpi.kit.edu/english/62_117.php\">Mike Lindner (KIT, GPI,BFO)</a>\n",
    "    </td>\n",
    "  </tr>\n",
    "  <tr>\n",
    "    <td style=\"text-align:left\">\n",
    "        Copyright (c) 2023 by \n",
    "        <a href=\"https://www.gpi.kit.edu/english/62_102.php\">Thomas Forbriger (KIT, GPI,BFO)</a>\n",
    "    </td>\n",
    "  </tr>  \n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Background\n",
    "This Jupyter notebook shall support you in creating a configuration file `dispcal.par` for program `dispcal`.\n",
    "You may as well use any text editor to create and modify `dispcal.par`.\n",
    "\n",
    "The seismometer calibration program `dispcal` is an original Fortran-code by Erhard Wieland.\n",
    "This JN page is in large parts a copy of the [`dispcal` manual](https://gitlab.kit.edu/kit/gpi/bfo/seismology/software-for-seismometry-linux/-/blob/master/software/dispcal/dispcal.doc)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Current working directory\n",
    "The Jupyter notebook and all programs called from within are operating on files in the current working directory.\n",
    "The following command prints the path to this directory.\n",
    "If your files should be located somewhere else, copy them from this directory or change the working directory by calling `os.chdir()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"The program will operate on files in %s\" % os.getcwd())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Format of the PARAMETER FILE\n",
    "The parameter file `dispcal.par` has the following structure:\n",
    "\n",
    "    calibration of TC SN 5322 on 1 Jun. 2021\n",
    "    data.sfe        name of data file\n",
    "    TC20s SN 5332   type and serial number of seismometer\n",
    "    20.00000        eigenperiod of the seismometer / s\n",
    "    0.70700         damping as a fraction of critical\n",
    "    2.50000         digitizer gain / microvolts per count\n",
    "    0.88150         displacement per step / mm\n",
    "    0.50000         safety distance from pulse flanks in seconds - see NOTE\n",
    "    0.00000    0    corner period (s) and order of low-pass filter (none if 0.,0)\n",
    "    .true.          print list of pulses\n",
    "    1.002           correction factor for voltage divider\n",
    "    \n",
    "            \n",
    "    NOTE: safety distance is also the minimum averaging length. The entry 0 will \n",
    "          cause the program to try five values from 0.125 to 2.000.              \n",
    "    \n",
    "    1.0025         correction factor for TC20s at Centaur\n",
    "    1.013          correction factor for STS-1 at Q330HR without Monitor\n",
    "    1.014          correction factor for STS-1 at Q330HR plus Monitor 25 V\n",
    "    1.022          correction factor for STS-1 at Q330HR plus Monitor 2.5 V\n",
    "\n",
    "\n",
    "The first four numerical parameters and the last one must be known before `dispcal` is run. \n",
    "The free period and damping of the seismometer can be obtained from an analysis with `calex` ore some other \n",
    "system-identification software. \n",
    "The microvolts per count refer to the calibration of the digital recorder and the millimeters of displacement to the stepwise-motion experiment. \n",
    "The correction factor compensates for the signal loss due to the source impedance of the sensor; it is normally slightly larger than unity. \n",
    "The other parameters help the program to decide which parts of the data record represent motion and which ones rest. \n",
    "If you are uncertain about the \"safety distance\", enter zero and the program will try five values between 0.125 and 2 seconds. \n",
    "Select the one that produces the smallest rms scatter. \n",
    "The program expects to find at least 5 seconds without motion at the beginning of the input signal. \n",
    "\n",
    "When the signal-to-noise ratio is poor, the program may not be able to separate the intervals where motion occurs from those without. \n",
    "You can help the program by inserting a second parameter 'perc' after the \"safety distance\", in the same line and separated by a blank. \n",
    "It specifies the percentage of the total duration that should be considered as motion-free (typically about 80%). "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Create content dictionary `cont`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont = {}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Header information\n",
    "Printed but not evaluated. This line is mainly informal as it gives information about the conducted test."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['header'] = 'calibration of TC SN 5322 on 1 Jun. 2021'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Input file name \n",
    "The input file must be available in the [seife](https://www.black-forest-observatory.de/ThomasForbriger/doc/libdatrwxx/html/page_seife_format.html) format (extension .sfe).\n",
    "The JN [Calibration](http://localhost:8888/notebooks/src/Calibration.ipynb) features the function `convert_mseed2seife` which allows you to convert a miniSEED file to seife. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['input'] = 'data.sfe'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Seismometer ID\n",
    "\n",
    "Change Serial Number (SN) to match your respective device."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['seismo'] = 'TC20s SN 5332'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Eigenperiod\n",
    "\n",
    "Eigenperiod of the eximined seismometer. Add here the value dervied by CALEX."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['eigenperiod'] = 20.0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Damping\n",
    "\n",
    "Damping of the eximined seismometer. Add here the value dervied by CALEX."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['damping'] = 0.707"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Sensitivity\n",
    "\n",
    "Sensitivity of the used data logger. This parameter has the physical unit $[S_{\\text{Centaur}}] = \\frac{\\mu V}{\\text{counts}}$. You can find this value in the official Centaur user guide. Check section 7.1.3 Digitizer Performance for more information. In this course, we will be working with an input voltage range of 40 Vpp into the logger (compare [TC20s](Trillium_Compact_20s.ipynb) first table).\n",
    "\n",
    "$\\underline{\\text{Note}}$:     \n",
    "Following default settings are choosen in accordance to a Centaur data logger. It is not necessary to change this parameter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['sensitivity'] = 2.5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Displacement\n",
    "\n",
    "Displacment of the calibration-table in mm per step. \n",
    "This parameter needs to be measured before and after the absolute calibration using the mechanical dial gauges attached to the step table."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['displacement'] = 0.8815 "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Safety distance\n",
    "\n",
    "Safety distance from the transitions in seconds. The \n",
    "entry 0 will cause the program to try five values from \n",
    "0.125 to 1 s.\n",
    "      \n",
    "$\\underline{\\text{Note}}$:     \n",
    "Following default settings are choosen in accordance to a TC20s. It is not necessary to change this parameter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['safety_distance'] = 0.5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Filter\n",
    "The here introduced filter is optional and only needed for the calibration of an accelerometer.\n",
    "Input information hereby are the corner period in seconds and the oder of the low-pass filter. If set to 0., 0 (float,integer) no filter is used. \n",
    "\n",
    "$\\underline{\\text{Note}}$:     \n",
    "Following default settings are choosen in accordance to a TC20s. It is not necessary to change this parameter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['filter'] = [0.,0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Print list of pulses\n",
    "\n",
    "It is not necessary to change this parameter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['print_pulses'] = True"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Correction factor\n",
    "\n",
    "The connection of the seismometer with a datalogger results in a enlarged electrical system. \n",
    "In addition of the individual characteristics of the two devices, we now also need to consider the full circuit.\n",
    "The voltage applied to the logger $U_S(t)$ experiences a drop compared to the initial voltage at the seismometer $U_C(t)$ which can be described via the impedance of both devices:\n",
    "\\begin{equation}\n",
    "U_S(t) = U_C(t) \\cdot \\frac{R_{\\text{S}}}{R_{\\text{S}} + R_{\\text{I}}}\n",
    "\\end{equation}\n",
    "Both values are listed in the respective user guides:\n",
    "* $R_{\\text{I}}$: TC20s Section 9.1\n",
    "* $R_{\\text{S}}$: Centaur Section 7.1.1 (Standard digital recorder model) \n",
    "\n",
    "$\\underline{\\text{Note}}$:     \n",
    "Following default settings are choosen in accordance to a TC20s and Centaur data logger. It is not necessary to change this parameter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cont['correction_factor'] = 1.0025"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Write dispcal.par\n",
    "Define a function to write the parameters to file `dispcal.par`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def write_dispcal_par(content):\n",
    "    '''\n",
    "        write dispcal parameter file dispcal.par \n",
    "        \n",
    "        dispcal.f uses Fortran free format when parsing lines in dispcal.par\n",
    "        Just make sure to separate different entries in one line by whitespace. \n",
    "\n",
    "        Parameters\n",
    "        ----------\n",
    "        content : dictionary\n",
    "            dictionary with parameters to be written to file\n",
    "    '''\n",
    "\n",
    "    # write file\n",
    "    with open('dispcal.par', \"w\") as f:\n",
    "        # input information\n",
    "        f.write(\"%s\\n\" %(content['header'])) # header information\n",
    "        f.write(\"{:<15s} name of data file\\n\".format(content['input'])) # input filename\n",
    "        f.write(\"{:<15s} type and serial number of seismometer\\n\".format(content['seismo'])) # seismometer ID\n",
    "        # seismometer information\n",
    "        f.write(\"{:<15.5f} eigenperiod of the seismometer / s\\n\".format(content['eigenperiod']))\n",
    "        f.write(\"{:<15.5f} damping as a fraction of critical\\n\".format(content['damping']))\n",
    "        # logger information\n",
    "        f.write(\"{:<15.5f} digitizer gain / microvolts per count\\n\".format(content['sensitivity']))\n",
    "        # activation information\n",
    "        f.write(\"{:<15.5f} displacement per step / mm\\n\".format(content['displacement']))\n",
    "        # inversion information\n",
    "        f.write(\"{:<15.5f} safety distance from pulse flanks in seconds - see NOTE\\n\".format(content['safety_distance'])) # seismometer ID\n",
    "        f.write(\"{:<10.5f} {:<4d} corner period (s) and order of low-pass filter (none if 0.,0)\\n\".format(content['filter'][0],content['filter'][1])) # low-pass filter\n",
    "        if content['print_pulses']: # print\n",
    "            boolstring=\".true.\"\n",
    "        else:\n",
    "            boolstring=\".false.\"\n",
    "        f.write(\"{:<15s} print list of pulses\\n\".format(boolstring))\n",
    "        # combined system information\n",
    "        f.write(\"{:<15.4g} correction factor for voltage divider\\n\".format(content['correction_factor'])) # correction factor (impedance)\n",
    "        f.write(\"\"\"\n",
    "        \n",
    "NOTE: safety distance is also the minimum averaging length. The entry 0 will \n",
    "      cause the program to try five values from 0.125 to 2.000.              \n",
    "\n",
    "1.0025         correction factor for TC20s at Centaur\n",
    "1.013          correction factor for STS-1 at Q330HR without Monitor\n",
    "1.014          correction factor for STS-1 at Q330HR plus Monitor 25 V\n",
    "1.022          correction factor for STS-1 at Q330HR plus Monitor 2.5 V\n",
    "        \"\"\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Call the function to create `dispcal.par`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "write_dispcal_par(cont)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
