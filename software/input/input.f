     subroutine input(name,n,dt,x,tmin,tsec)
c this subroutine reads data in the SEIFE and ASL formats
c dt, x, tmin, tsec are double precision (may be changed)
c name: name of the data file
c n: maximum number of samples
c dt: sampling interval
c x: the time series to be read
c tmin: time of first sample, minutes after midnight
c tsec: time of first sample, seconds after midnight
c date cannot be encoded except as minutes
      dimension ix(60)
      double precision dt,x(n),tmin,tsec
      character iform*20,name*50,text*72,zeile*72
      character zeil*120,zarr(120),b,code1*6,code2*6
      logical seife

      b=' '
      do j=1,n
        x(j)=0.
      enddo

      write(6,*) 'Opening file ',trim(name)
      write(4,*) 'Opening file ',trim(name)
        open(7,file=name,status='old')
        read(7,'(a)') zeile
        read(7,'(a)') zeile
        seife=index(zeile,'%').gt.0.or.index(zeile,'(').gt.0
        if(seife) then
          write(6,*) 'file ',trim(name),' assumed to be in SEIFE format'
        else
          write(6,*) 'file ',trim(name),' assumed to be in ASL format'
        endif
        close(7)
        
      if(seife) then
c read data in SEIFE format      
        open(7,file=name,status='old')
        read(7,'(a)') text
        write(6,*) ' header: ',trim(text)
   21   read(7,'(a)') zeile
        if(zeile(1:1).eq.'%') goto 21
   20   read(zeile,1) nn,iform,dt,tmin,tsec
    1   format(i10,a20,3f10.3)
        if(nn.gt.n) then
          write(*,*) 'sorry, too many data points. can handle only ', n
          write(*,*) 'for more, edit and recompile the source code'
        else
          n=nn
        endif
        write(6,22) n,trim(name)
   22   format(' reading',i7,' samples from file ',a)
        if(iform(1:3).eq.'fre'.or.index(iform,'i').gt.0) then
          read(7,*,err=25,end=23) (x(j),j=1,n)
        else
          read(7,iform,err=25,end=23) (x(j),j=1,n)
        endif
        close(7)
        return
      else
      
c read data in ASL format (such as written by Quanterra's Cimarron)
        open(7,file=name,status='old')
        read(7,'(a)') zeil
        text=zeil(1:72)
        write(6,*) 'header: ',trim(text)
        read(zeil,'(120a)') zarr
      nn=0
      do i=1,119
        if(zarr(i).ne.b.and.zarr(i+1).eq.b) then
          nn=nn+1
          ix(nn)=i
        endif
      enddo
        read(zeil(1:ix(1)),'(a)') code1
        read(zeil(ix(1)+2:ix(2)),'(a)') code2
        read(zeil(ix(2)+2:ix(7)),*) iyear,iday,ithr,itmin,itsec
        read(zeil(ix(7)+2:ix(11)),*) t1000,srate,nn       
        thr=ithr
        tmin=itmin
        tsec=itsec
        dt=1./srate
        tmin=tmin+60.*thr
        tsec=tsec+t1000/1000.
        if(nn.gt.n) then
          write(*,*) 'sorry, too many data points. can handle only ', n
          write(*,*) 'for more, edit and recompile the source code'
        else
          n=nn
        endif
        dt=1./srate
        tmin=tmin+60.*thr
        tsec=tsec+t100/100.
        
c        read(7,*,err=25,end=23) (x(j),j=1,n)
c free-format read doesn't always work - compiler bug? 
c we must write our own routine

        nz=0
        nval=0
        do
          write(zeil,'(120x)')
          read(7,'(a)',end=31) zeil
          nz=nz+1
          read(zeil,'(120a)') zarr
          nn=0
          do i=1,119
            if(zarr(i).ne.b.and.zarr(i+1).eq.b) nn=nn+1
          enddo
          nn=min(nn,n-nval)
          read(zeil,*,err=32) (ix(j),j=1,nn)
          do j=1,nn
            x(nval+j)=ix(j)
          enddo
          nval=nval+nn
        enddo
   31   write(6,*) nval,' samples read from file ',trim(name)
        write(4,*) nval,' samples read from file ',trim(name)
        close(7)
        return
   32   write(6,*) 'error (maybe a non-numeric character) in data line '
     &  ,nz,' of file ',trim(name)
        stop
      endif
      
   23 n=j-1
      write(6,24) n
      write(3,24) n
   24 format('end of file after ',i8,' samples')
      close(7)
      return
   25 write(6,26) j
      write(3,26) j
   26 format(' Input error at sample # ',i8)
      stop
      end

