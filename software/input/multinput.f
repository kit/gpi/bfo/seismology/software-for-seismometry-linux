      subroutine inpasc(name,lin,text,prolog,npro,n,dt,tmin,tsec,x,ndim)
      dimension x(ndim),y(60),ix(60)
      character iform*20,name*48,text*72,pro*80,code*4,rest*40
      character prolog(48)*60,code1*6,code2*6
      character zeile*120,zarr(120),b
      common /asl/ code1,code2,year,day,thr,tm,ts,t1000,srate,cpv
      logical first
c
      b=' '
      cpv=1.000
      do j=1,n
        x(j)=0.
      enddo

      write(6,*) '==='
      write(6,*) ' Opening file ',name
      open(7,file=name,status='old')
c  ASCII files with header
      if(dt.gt.0.) then
        write(6,22) trim(name),0,ndim,dt
        do i=1,lin
          read(7,'(a)') text
          write(6,*) ' header line: ',trim(text)
          prolog(i)=text
        enddo
        npro=lin
        text=name
c ??? read(7,*,err=31,end=31) (x(j),j=1,n)
c   sch÷n wr's, geht aber nicht zuverlssig - Bug im Compiler?
        nz=lin
        nval=0
        do
          read(7,'(a)',end=131) zeile
          read(zeile,'(120a)') zarr
          nn=0
          do i=1,119
            if(zarr(i).ne.b.and.zarr(i+1).eq.b) nn=nn+1
          enddo
          nz=nz+1
          read(zeile,*,err=132) (y(j),j=1,nn)
          do j=1,nn
            x(nval+j)=y(j)
          enddo
          nval=nval+nn
        enddo
  131   write(6,*) nval,' samples read from file ',trim(name)
        n=min(n,nval)
        close(7)
        return
  132   write(6,*) 'error (maybe a non-numeric character) in line ',nz,
     &  ' of file ',trim(name)
        stop

c  read data in SEIFE format
      else if(dt.eq.0.) then
        read(7,'(a)') text
        write(6,*) ' header line: ',trim(text)
        npro=0
   21   read(7,'(a)') pro
        if(pro(1:1).ne.'%'.and.pro(1:1).ne.'}') goto 20
        npro=npro+1
        if(npro.gt.48) stop 48
        prolog(npro)=pro(3:62)
        goto 21
   20   read(pro,1) nn,iform,dt,tmin,tsec
    1   format(i10,a20,3f10.3)
        write(6,22) trim(name),nn,ndim,dt
   22   format('  reading file ',a,' ',i7,'/',i7,' samples','  dt=',
     & f10.6)
        if(n.lt.nn.or.nn.eq.0) then
c         write(6,'(" number of samples limited to",i7)') n
          nn=n
        endif
        n=nn
        if(iform(1:3).eq.'fre'.or.index(iform,'i').gt.0) then
          write(6,*) ' data will be read in free-format'
          read(7,*,err=25,end=23) (x(j),j=1,n)
        else
          read(7,iform,end=23) (x(j),j=1,n)
        endif

c  read data in MARS88 format
      else if(dt.eq.-1.) then
        read(7,'(a)') text
        text(61:72)='            '
        read(text(15:17),*) idt
        dt=idt/1000.
        read(text(53:54),*) thr
        read(text(56:57),*) tmin
        read(text(59:60),*) tsec
        tmin=tmin+60.*thr
        write(6,22) trim(name),n,ndim,dt
        do j=1,n
          read(7,*,err=23,end=23) x(j)
        enddo

      else if(dt.eq.-2.) then
c read data in ASL format (such as written by Quanterra's Cimarron)
        read(7,'(a)') zeile
        text=zeile(1:72)
        write(6,*) 'header: ',trim(text)
        read(zeile,'(120a)') zarr
      nn=0
      do i=1,119
        if(zarr(i).ne.b.and.zarr(i+1).eq.b) then
          nn=nn+1
          ix(nn)=i
        endif
      enddo
        read(zeile(1:ix(1)),'(a)') code1
        read(zeile(ix(1)+2:ix(2)),'(a)') code2
        read(zeile(ix(2)+2:ix(7)),*) iyear,iday,ithr,itmin,itsec
        read(zeile(ix(7)+2:ix(11)),*) t1000,srate,n,cpv        
        thr=ithr
        tmin=itmin
        tsec=itsec
        dt=1./srate
        tmin=tmin+60.*thr
        tsec=tsec+t1000/1000.
        write(6,*) 'header says ',cpv,' counts per volt - please check'
c  ???  read(7,*) (x(j),j=1,n)
c   sch÷n wr's, geht aber nicht zuverlssig - Bug im Compiler?
        nz=1
        nval=0
        do
          read(7,'(a)',end=31) zeile
          read(zeile,'(120a)') zarr
          nn=0
          do i=1,119
            if(zarr(i).ne.b.and.zarr(i+1).eq.b) nn=nn+1
          enddo
          read(zeile,*,err=32) (ix(j),j=1,nn)
          do j=1,nn
            x(nval+j)=ix(j)
          enddo
          nval=nval+nn
        enddo
   31   write(6,*) nval,' samples read from file ',trim(name)
        n=min(n,nval)
        close(7)
        return
   32   write(6,*) 'error (maybe a non-numeric character) in line ',nz,
     &  ' of file ',trim(name)
        stop

      else if(dt.eq.-3.) then
c read data in BDF format
        first=.true.
        nnn=0
   40   read(7,'(a)',end=23) text
        code=text(1:4)
        rest=text(5:44)
c        write(*,*) code,rest
        if(code.eq.'DATA') then
          goto 41
        else if(code.eq.'RATE') then
          read(rest,*) srate
          dt=1./srate
        else if(first.and.code.eq.'TIME') then
          read(rest(1:4),*) year
          read(rest(6:8),*) day
          read(rest(10:11),*) thr
          read(rest(13:14),*) tmin
          read(rest(16:22),*) tsec
          tmin=tmin+60.*thr
          first=.false.
        else if(code.eq.'NSAM') then
          read(rest,*) nsam
        endif
        goto 40
   41   read(7,*,err=26,end=23) (x(j),j=nnn+1,nnn+nsam)
        nnn=nnn+nsam
        goto  40
      endif
      close(7)
      return
   23 n=j-1
      write(6,24) n
   24 format(' end of file after ',i8,' samples')
      close(7)
      return
   25 write(6,26) trim(name),j
   26 format(' Input error reading file ',a,' at sample # ',i7)
      stop
      end
