      subroutine output(name,text,n,dt,x,tmin,tsec)
c this subroutine writes data in SEIFE format
c observe the declaration of argument variables!
c name: name of the output file
c text: the header line
c n: number of samples
c dt: sampling interval
c x: the time series
c tmin: time of first sample, minutes after midnight
c tsec: time of first sample, seconds after midnight
c date cannot be encoded except as minutes

      double precision dt,x(n),xmax,tmin,tsec
      character name*(*),text*(*),form*20
      xmax=0.d0
      do 2 j=1,n
    2 xmax=max(xmax,abs(x(j)))
      nvor=log10(max(xmax,2.d0))+1.
      ndec=max(0,10-nvor)
      write(form,3) ndec
    3 format('(5f13.',i1,')')
      write(6,101) name
      write(4,101) name
  101 format(' writing file ',a)
      open(8,file=name)
      write(8,'(a)') text
      write(8,1) n,form,dt,tmin,tsec
    1 format(i10,a20,f10.6,2f10.3)
      write(8,form) (x(j),j=1,n)
      close(8)
      return
      end
