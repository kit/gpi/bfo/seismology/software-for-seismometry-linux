c   .    1    .    2    .    3    .    4    .    5    .    6    .    7
c  Demultiplexen von mehrspaltigen Datensaetzen, max. 7 Spalten
c
      program demux
      use nas_system
      real*4 x(7)
      character*72 text
      character*24 runs,infile
      character*7 outfile(7)

      narg=iargc()
      if(narg.lt.3) then
        write(6,'(/"Aufruf: demux infile nskip nch"/
     & "nskip: Anzahl Headerzeilen (ueberspringen)"/
     & "nch:   Anzahl Spalten (Signalkanaele)")')
       stop
      endif
      call getarg(1,infile)
      call getarg(2,runs)
      read(runs,*) nskip
      call getarg(3,runs)
      read(runs,*) nch
      icount=0

      open(7,file=infile,status='old')
      write(6,*) 'reading file ',infile
      do i=1,nskip
        read(7,'(a72)') text
        write(6,*) text
      enddo
      do j=1,nch
        write(outfile(j),'("demux.",i1)') j
        open(7+j,file=outfile(j))
      enddo

      do
        read(7,*,end=999) (x(j),j=1,nch)
        icount=icount+1
        do j=1,nch
          write(7+j,*) x(j)
        enddo
      enddo

 999   write(6,*) icount,' lines of data were read'
       end
