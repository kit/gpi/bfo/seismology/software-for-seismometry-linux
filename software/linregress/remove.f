c   .    1    .    2    .    3    .    4    .    5    .    6    .    7    .    8
c
c  kohaerenzanalyse und anschliessende signalreinigung
c
c     max. ndim punkte je serie
c
c     created: <840118.1411>
c     Einfach-Version fuer SUN 1991
c     an NAS-Fortran angepasst Nov. 2005
      program unicrosp
      use nas_system
      parameter (nexp=20)
      parameter(ndim=2**nexp)
      parameter(nadim=256)
      real*4 x(ndim),y(ndim),amp(nadim),pha(nadim)
      integer nub(nadim),nob(nadim)
      character text1*72,text2*72,filein1*24,filein2*24,
     & form1*20,form2*20,fileout1*24,fileout2*24,fileout*24
      logical repeat

      narg=iargc()
      if (narg.eq.0) then
      write(6,'(" File 1: Stoerung? ")')
      read(5,'(a)') filein1
      write(6,'(" File 2: gestoertes Signal? ")')
      read(5,'(a)') filein2
      write(6,'("Name fuer das Resultat: Signal minus Stoerung? ")')
      read(5,'(a)') fileout
      write(6,5)
    5 format(' Anzahl Baender pro Dekade ? ( nur ein Durchg. wenn <0) ')
      read(5,*) nband
      else if(narg.eq.4) then
      call getarg(1,filein1)
      call getarg(3,filein2)
      call getarg(4,text1)
      read(text1,*) nband
      else
      write(6,7)
    7 format('Aufruf: remove (interaktiv), oder: remove STOERSIG from NUT
     &ZSIG nband')
      stop
      endif
      repeat=(nband.gt.0)
      nband=abs(nband)
      ind1=index(filein2,'.')
      ind2=len(trim(filein2))
      fileout1=filein2(1:ind1-1)//'ung'//filein2(ind1:ind2)
      fileout2=filein2(1:ind1-1)//'ger'//filein2(ind1:ind2)

      open(8,file='crospout')

c  zwei zeitserien einlesen
      n=ndim
      call input(filein1,text1,n,dt,tmin,tsec,x)
      write(6,'(a)') text1
      write(6,1) n,form1,dt
    1 format(1x,i6," punkte  ",a,"   dt=",f8.3)
      if(filein2.eq.filein1) then
        text2=text1
        form2=form1
        dt2=dt
        do j=1,ndim
          y(j)=x(j)
        enddo
      else
        n2=ndim
        call input(filein2,text2,n2,dt2,tmin2,tsec2,y)
        write(6,'(a)') text2
        write(6,1) n2,form2,dt2
        if(n.ne.n2.or.dt.ne.dt2.or.tmin.ne.tmin2.or.tsec.ne.tsec2)
     &    stop 12
      endif
      np=n
      np2=0
c  serienlaenge 2**n fuer die schnelle fouriertransformation
      do while (2**np2 .lt. np)
        np2=np2+1
      end do
      np2=min(np2,nexp)
      nmax=2**np2
      t=nmax*dt
      write(6,111) np2,nmax
  111 format(' FFT 2**',i2,' = ',i6,' Punkte')
c  vorbereitung
      write(8,'(1x," 1. inputfile: ",a)') filein1
      write(8,'(1x,a)') text1
      write(8,1) n,form1,dt
      write(8,'(1x," 2. inputfile: ",a)') filein2
      write(8,'(1x,a)') text2
      write(8,1) n,form2,dt2
c  beginn der analyse
      do i=np+1,nmax
        x(i)=0.
        y(i)=0.
      end do
      call trend(x,n,ndim)
      call trend(y,n,ndim)
      call sft(x,y,np2,-2,ndim)
      call trenn2(x,y,x,y,nmax,ndim)
      fa=sqrt(2.*float(nmax)/float(n))
      x(1)=0.
      y(1)=0.
      do i=2,nmax
        x(i)=x(i)*fa
        y(i)=y(i)*fa
      end do
      tshift=0.
      fak=1.
    2 call bandf(x,y,nmax,dt,tshift,fak,nband,ndim,amp,pha,nub,nob,
     & nk,nadim)
      if(repeat) then
      write(6,'(/" Transfer-Faktor, Verzoeg. Sig2? [0,0=fertig]: _")')
      faka=fak
      read(5,*) fak,tshift
      write(8,3) fak,tshift
    3 format(//" Signal 2 div. durch ",f10.3," und verschoben um",f10.3)
      if(fak.ne.0.) goto 2
      endif
      close(8)
      write(6,'(" Resultate wurden auf File crospout geschrieben"/)')

      call rein(x,y,nmax,ndim,amp,pha,nub,nob,nk,nadim,faka,dt)

      call misch2(x,y,x,y,nmax,ndim)
      do i=2,nmax
        x(i)=x(i)/fa
        y(i)=y(i)/fa
      end do

      call sft(x,y,np2,1,ndim)

      call output(fileout1,text1,n,dt,tmin,tsec,y)
      write(6,4) trim(fileout1)
    4 format(" ungereinigtes Signal wurde als File ",a," ausgegeben")
      call output(fileout2,text1,n,dt,tmin,tsec,x)
      write(6,6) trim(fileout2)
    6 format(" gereinigtes Signal wurde als File   ",a," ausgegeben")
      stop
      end

c ---------------------------------------------------------------------

      subroutine sft(x,y,n,is,ndim)
c     schnelle fouriertransformation der 2**n komplexen werte (x,y)
c     is negativ - in den frequenzbereich / positiv - in den zeitbereich
c     normierung - abs(is) =1 - ohne / 2 - mit 1/ng / 3 - mit 1/sqrt(ng)
c                            4 - ohne normierung,ohne kontrollausdruck
      real*4 x(ndim),y(ndim)
      integer   zh(21)
      piz=8.*atan(1.)
c     tabelle der zweierpotenzen
      zh(1)=1
      do 1 l=1,n
    1 zh(l+1)=2*zh(l)
      ng=zh(n+1)
      gn=1./float(ng)
c     kernprogramm.dreifache schleife ueber schritt/index/teilserie
      do 2 m=1,n
      nar=zh(m)
      lar=ng/nar
      larh=lar/2
      alpha =  piz/float(isign(lar,is))
      do 3 jr=1,larh
      beta=alpha*float(jr-1)
      excos = cos(beta)
      exsin = sin(beta)
      ja=jr-lar
      do 4 nr=1,nar
      ja=ja+lar
      jb=ja+larh
      zx = x(ja)-x(jb)
      zy = y(ja)-y(jb)
      x(ja) = x(ja)+x(jb)
      y(ja) = y(ja)+y(jb)
      x(jb) = zx*excos-zy*exsin
      y(jb) = zx*exsin+zy*excos
    4 continue
    3 continue
    2 continue
c     normierung
      if(iabs(is).eq.1.or.iabs(is).eq.4) go to 10
      if(iabs(is).eq.3) gn=sqrt(gn)
    5 do 6  l=1,ng
      y(l) = y(l)*gn
    6 x(l)=x(l)*gn
c     umordnung nach "bitreversed" indizes
   10 do 7 j=1,ng
      js=j-1
      k=1
      nny=n+1
      do 8 ny=1,n
      nny=nny-1
      if(js.lt.zh(nny)) go to 8
      js=js-zh(nny)
      k=k+zh(ny)
    8 continue
      if(j-k) 9,7,7
    9 zx = x(j)
      zy = y(j)
      x(j) = x(k)
      y(j) = y(k)
      x(k) = zx
      y(k) = zy
    7 continue
      return
      end

c ---------------------------------------------------------------------

      subroutine trenn2(x,y,xt,yt,ng,ndim)
      real x(ndim),y(ndim),xt(ndim),yt(ndim)
      ngh = ng/2
      ngh1 = ngh+1
      xre = x(1)
      yt(1)=y(1)
      xt(1) = xre
      do 30 l=2,ngh
      m=ng-l+2
      xre=(x(l)+x(m))*0.5
      xim=(y(m)-y(l))*0.5
      yre=(y(l)+y(m))*0.5
      yim=(x(l)-x(m))*0.5
      xt(l)=xre
      xt(m)=xim
      yt(l)=yre
   30 yt(m)=yim
      xre = x(ngh1)
      yt(ngh1) = y(ngh1)
      xt(ngh1) = xre
      return
      end

c----------------------------------------------------------------------

      subroutine misch2(x,y,xm,ym,ng,ndim)
      real x(ndim),y(ndim),xm(ndim),ym(ndim)
      ngh = ng/2
      ngh1 = ngh+1
      xll = x(1)
      ym(1)=y(1)
      xm(1) = xll
      do 30 l=2,ngh
      m=ng-l+2
      xll = x(l)+y(m)
      xmm = x(l)-y(m)
      yll = -x(m)+y(l)
      ymm = y(l)+x(m)
      xm(l)=xll
      xm(m)=xmm
      ym(l)=yll
   30 ym(m)=ymm
      xll = x(ngh1)
      ym(ngh1) = y(ngh1)
      xm(ngh1) = xll
      return
      end

c ---------------------------------------------------------------------

      subroutine trend(x,n,ndim)
      dimension x(ndim)
      gn = float(n)
      alpha = 0.5*gn*(gn+1.)
      beta = (2.*gn+1.)*(gn+1.)*gn/6.
      det = gn*beta-alpha*alpha
      sx = 0.
      sjx = 0.
      do 1001 j=1,n
      sx = sx+x(j)
 1001 sjx = sjx+x(j)*float(j)
      a = (sx*beta-sjx*alpha)/det
      b = (sjx*gn-sx*alpha)/det
      do 1002 j=1,n
 1002 x(j) = x(j)-a-b*float(j)
      return
      end

c----------------------------------------------------------------------

      subroutine bandf(x,y,n,dt,tshift,fak,nband,ndim,amp,pha,nub,nob,
     & nk,nadim)
      character*72 form
      real x(ndim),y(ndim),amp(nadim),pha(nadim)
      integer nob(nadim),nub(nadim)
      complex cy
      nk=1
      w=45./atan2(1.,1.)
      n2=n+2
      t=n*dt
      domega=360./w/t
      xx=0.
      xy=0.
      yx=0.
      yy=0.
      ncoef=0
      xxtot=0.
      xytot=0.
      yxtot=0.
      yytot=0.
      u=10.**(1./nband)
      ico=int(1024.+nband*alog10(2.*dt))-1023
      c0=ico/float(nband)
      co=10.**c0
      c1=u*co
      jo=t/co
      nub(1)=ndim
      nob(1)=0

        write(6,25)
        write(6,26)
        write(8,25)
        write(8,26)
25    format(/"  unicrosp: effektive amplituden in counts"/
     & "  noise1, noise2 gueltig wenn beide systeme gleich sind"/
     & "  nois21 gueltig wenn system 1 noise-freie referenz ist"/
     & "  in jedem fall sollte 1 das rauschaermere system sein und die"/
     & "  uebertragungsfunktionen sollen aneinander angepasst werden"/ 
     & "  bei kohaer>0.5 ist dann nois21 auszuwerten, darunter noise2"//
     & "  bandm.  amp 1  noise1   amp 2  noise2  nois21   kr.ph   transf
     &  kohaer")
26    format("  (m)Hz   -------------   counts  -------------   grad "/)

c Schleife ueber Fourierkoeff. (absteigend)

      do i=2,jo
        j=jo+2-i
        jj=n2-j

        if(t/(j-1).ge.c1) then

c Nur wenn ein Band voll ist:
          if(ncoef.ge.25) then
c   normales band (mindestens 25 Koeffizienten)
            bmitte=1./sqrt(co*c1)
          if(bmitte.lt.0.01) bmitte=bmitte*1000.
            bbreit=1./co-1./c1
          if(bbreit.lt.0.01) bbreit=bbreit*1000.
            xxtot=xxtot+xx
            xytot=xytot+xy
            yxtot=yxtot+yx
            yytot=yytot+yy
          else
c   Zusammenfassung nach dem letzten Band
            nub(nk)=0
            nob(nk)=0
            xx=xxtot
            xy=xytot
            yx=yxtot
            yy=yytot
            bmitte=1e12
            bbreit=1e12
          endif
          xyxy=xy*xy+yx*yx
          wxy=sqrt(xyxy)
          rn1=dim(xx,wxy)
          rn2=dim(yy,wxy)
          phi=-atan2(yx,xy)*w
          renz=min(wxy/sqrt(xx*yy),1.)
c         srld=max(wxy/bbreit,1e-11)
c         srld=10.*log10(srld)
          r21=sqrt(xx*(1.-renz**2)/renz**2)
          sxx=sqrt(xx)
          srn1=sqrt(rn1)
          syy=sqrt(yy)
          srn2=sqrt(rn2)
          amax=max(sxx,srn1,syy,srn2)
          if(amax.lt.1000) then
            form='(f7.3,1x,5(f7.3,1x),f7.1,1x,2f8.4)'
          else if(amax.lt.10000) then
            form='(f7.3,1x,5(f7.2,1x),f7.1,1x,2f8.4)'
          else if(amax.lt.100000) then
            form='(f7.3,1x,5(f7.1,1x),f7.1,1x,2f8.4)'
          else
            form='(f7.3,1x,5(f7.0,1x),f7.1,1x,2f8.4)'
          endif

          write(6,form) bmitte,sxx,srn1,
     &    syy,srn2,r21,phi,wxy/xx,renz
          write(8,form) bmitte,sxx,srn1,
     &    syy,srn2,r21,phi,wxy/xx,renz
          if(ncoef.lt.7) return
          pha(nk)=phi
          amp(nk)=wxy/xx
          nk=nk+1
          nub(nk)=ndim
          nob(nk)=0
          xx=0.
          xy=0.
          yx=0.
          yy=0.
          co=c1
          c1=co*u
          ncoef=0
        end if

c nur die folgenden Zeilen werden fuer jede Frequenz ausgefuehrt

        omega=(j-1)*domega
        cy=cmplx(y(j),-y(jj))*cexp(cmplx(0.,-omega*tshift))
        yre=real(cy)/fak
        yim=-aimag(cy)/fak
        xx=xx+x(j)*x(j)+x(jj)*x(jj)
        xy=xy+x(j)*yre +x(jj)*yim
        yx=yx+x(j)*yim -x(jj)*yre
        yy=yy+yre *yre +yim  *yim
        ncoef=ncoef+1
        nub(nk)=min(nub(nk),j)
        nob(nk)=max(nob(nk),j)
        if(bmitte.gt.1e11) return
      end do
      end

c ----------------------------------------------------------------------

      subroutine input(name,text,n,dt,tmin,tsec,x)
      dimension x(n)
      character iform*20,name*24,text*72,zeile*72,code1*12,code2*12
      logical seife

      write(6,*) 'Opening file ',trim(name)
        open(7,file=name,status='old')
        read(7,'(a)') zeile
        read(7,'(a)') zeile
        seife=index(zeile,'%').gt.0.or.index(zeile,'(').gt.0
        if(seife) then
          write(6,*) 'file ',trim(name),' assumed to be in SEIFE format'
        else
          write(6,*) 'file ',trim(name),' assumed to be in ASL format'
        endif
        close(7)
        
      if(seife) then
c read data in SEIFE format      
        open(7,file=name,status='old')
        read(7,'(a)') text
        write(6,*) ' header: ',trim(text)
   21   read(7,'(a)') zeile
        if(zeile(1:1).eq.'%') goto 21
   20   read(zeile,1) nn,iform,dt,tmin,tsec
    1   format(i10,a20,3f10.3)
        if(nn.gt.n) then
          write(*,*) 'sorry, too many data points. can handle only ', n
          write(*,*) 'for more, edit and recompile the source code'
        else
          n=nn
        endif
        write(6,22) n,trim(name)
   22   format(' reading',i7,' samples from file ',a)
        if(iform(1:3).eq.'fre'.or.index(iform,'i').gt.0) then
          read(7,*,err=25,end=23) (x(j),j=1,n)
        else
          read(7,iform,err=25,end=23) (x(j),j=1,n)
        endif
        close(7)
        return
      else
c read data in ASL format (such as written by Quanterra's Cimarron)
        open(7,file=name,status='old')
        read(7,'(a)') text
        write(6,*) 'header: ',trim(text)
        read(text,*) code1,code2,year,day,thr,tmin,tsec,t100,srate,nn
        if(nn.gt.n) then
          write(*,*) 'sorry, too many data points. can handle only ', n
          write(*,*) 'for more, edit and recompile the source code'
        else
          n=nn
        endif
        dt=1./srate
        tmin=tmin+60.*thr
        tsec=tsec+t100/100.
        read(7,*,err=25,end=23) (x(j),j=1,n)
        write(*,*) n,' samples read from file ',trim(name)
        close(7)
        return
      endif
      
   23 n=j-1
      write(6,24) n
      write(3,24) n
   24 format('end of file after ',i8,' samples')
      close(7)
      return
   25 write(6,26) j
      write(3,26) j
   26 format(' Input error at sample # ',i8)
      stop
      end
c=======================================================================

      subroutine output(name,text,n,dt,tmin,tsec,x)
      dimension x(n)
      character name*24,text*72,iform*20
      iform='                    '
      xmax=0.
      do 2 j=1,n
    2 xmax=max(xmax,abs(x(j)))
      nvor=log10(max(xmax,2.))+1.
      ndec=max(0,10-nvor)
        write(iform,3) ndec
    3   format('(5f13.',i1,')')
      write(6,5) name,iform(1:15),n
    5 format(/' writing file ',a,' in format',a,i7,' samples')
      open(8,file=name)
        write(8,'(a)') text
        write(8,1) n,iform,dt,tmin,tsec
    1   format(i10,a20,f10.5,2f10.3)
        write(8,iform) (x(j),j=1,n)
      close(8)
      write(6,'(" finished!")')
      return
      end

c====================================================================

      subroutine  rein(x,y,nmax,ndim,amp,pha,nub,nob,nk,nadim,fak,dt)
c  Entfernen des durch die Kohaerenzanalyse ermittelten Stoersignals
      dimension x(ndim),y(ndim),amp(nadim),pha(nadim),
     &  nub(nadim),nob(nadim)
      complex cx

      torad=atan(1.)/45.
      write(6,*)
      if(nk.lt.3) then
        write(6,*) 'zu wenig Baender - stop'
        stop
      endif
      f2=nob(1)/(nmax*dt)*1000.
      f1=nub(nk-2)/(nmax*dt)*1000.
      write(6,2) f1,f2
    2 format(' Bandbreite wurde begrenzt: ',f7.2,' bis ',f9.2,' mHz')
      do k=1,nk-2
        write(6,1) k,nob(k),nub(k),pha(k),amp(k)
    1   format(" k=",i3,"   nob=",i6,"   nub=",i6,"   pha=",f8.1,
     &"   amp=",f10.4)
        arg=-torad*pha(k)
        a=amp(k)
        do j=nub(k),nob(k)
          jj=nmax+2-j
          cx=cmplx(x(j),x(jj))*cexp(cmplx(0.,arg))
          xre=real(cx)/fak
          xim=aimag(cx)/fak
          x(j)=y(j)-a*xre
          x(jj)=y(jj)-a*xim
        enddo
      enddo
      do j=2,nub(k-2)-1
      jj=nmax+2-j
      x(j)=0
      x(jj)=0
      y(j)=0
      y(jj)=0
      enddo
      do j=nob(1)+1,nmax+1-nob(1)
      x(j)=0
      y(j)=0
      enddo
      return
      end
