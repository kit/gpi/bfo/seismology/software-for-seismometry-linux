c this is <makeseifehelp.f>
c ----------------------------------------------------------------------------
c   ($Id$)
c
c Copyright (c) 2011 by Thomas Forbriger (BFO Schiltach) 
c
c produce a Fortran function to be called by seife to provide online help
c
c REVISIONS and CHANGES
c    05/09/2011   V1.0   Thomas Forbriger
c
c ============================================================================
c
      program makeseifehelp
      character*78 info
      character*160 infoout
      integer i, j, k
      logical hot
      open(7,file='seife.f',status='old', err=99)
      open(8,file='seifeinfo.f',status='new', err=96)
      write(8, 50, err=95) 'subroutine seifeinfo'
      write(8, 52, err=95) 
     &  'Notice: This file is created automatically from seife.f'
      write(8, 52, err=95) 
     &  'Do not include this file in a software repository!'
      write(8, 52, err=95) 
     &  'makeseifehelp can be used to recreate seifeinfo.f'
      hot=.true.
      do while (hot)
        read(7,'(a)',end=178, err=98) info
        j=0
        k=1
        do i=1,78
          j=j+1
          if (info(i:i).eq.'''') then
            infoout(j:j)=''''
            j=j+1
            infoout(j:j)=''''
          else
            infoout(j:j)=info(i:i)
          endif
          if (info(i:i).ne.' ') k=j
        enddo
        write(8, 51, err=95) infoout(1:k)
        if (info(1:8).eq.'c end of') hot=.false.
      enddo
  178 continue
      write(8, 50, err=95) 'return'
      write(8, 50, err=95) 'end'
      close(8, err=94)
      close(7, err=97)
      stop
   99 stop 'ERROR: opening seife.f for reading'
   98 stop 'ERROR: reading from seife.f'
   97 stop 'ERROR: closing seife.f'
   96 stop 'ERROR: opening seifeinfo.f for writing'
   95 stop 'ERROR: writing to seifeinfo.f'
   94 stop 'ERROR: closing seifeinfo.f'
c 
   50 format(6x,(a))
   51 format(6x,('write(6,''(a)'') ''',(a),''''))
   52 format('C',1x,(a))
      end
c
c ----- END OF makeseifehelp.f ----- 
