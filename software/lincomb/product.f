
      program produkt
c
c   product of two signals
c
c   convention for the input and output files:
c   - one header line, up to 72 characters
c   - up to 65 lines signal description and processing log
c     these lines have "%" as the first character
c     (the processing log is updated by the present program)
c   - one line specifying the number of samples, the format, the samplin
c     interval, and the start time in min and sec after midnight. this
c     line may be repeated after the data when its contents is not known
c     at the time when the file header is written; however the format
c     must always be specified.
c   - data in the specified format
c
      parameter(ndim=120000)
      dimension x(ndim),y(ndim),z(ndim)
      character text*72,rest*16
      character*40 prolog(24)
      character*24 nam1,nam2,nam3
        write(6,'(a)') 'Name des ersten Inputfiles?'
        read(5,'(a)') nam1
        write(6,'(a)') 'Name des zweiten Inputfiles?'
        read(5,'(a)') nam2
        write(6,'("Name des Outputfiles? _")')
        read(5,'(a)') nam3
      ny=ndim
      nx=ndim
      call input(nam2,text,prolog,npro,ny,dt1,tmin,tsec,rest,y)
      call input(nam1,text,prolog,npro,nx,dt2,tmin,tsec,rest,x)
      if(nx.eq.0.or.dt1.eq.0.d0.or.dt2.eq.0.d0.or.dt1.ne.dt2) stop
      dt=dt1
      npro=0
      nz=min(nx,ny)

c  multiply
      ymax=0.
      do i=1,nz
        ymax=max(ymax,abs(y(i)))
      enddo
      do i=1,nz
        z(i)=x(i)*y(i)/ymax
      enddo

      call output(nam3,text,prolog,npro,nz,dt,tmin,tsec,rest,z)
      stop
      end

c     ftn subroutines for reading and writing standard data files
c
      subroutine input(name,text,prolog,npro,n,dt,tmin,tsec,rest,x)
      dimension x(n)
      character iform*20,name*24,text*72,rest*16,pro*80
      character*40 prolog(24)
      open(7,file=name,status='old')
      read(7,'(a72)') text
      npro=0
   21 read(7,'(a80)') pro
      if(pro(1:1).ne.'%') goto 20
      npro=npro+1
      if(npro.gt.24) stop 24
      prolog(npro)=pro(3:42)
      goto 21
   20 read(pro,1) nn,iform,dt,tmin,tsec,rest
    1 format(i10,a20,3f10.3,a16)
      write(6,5) name,nn,n
    5 format("file ",a," wird gelesen:",i7," /",i7," punkte")
      if(n.lt.nn.or.nn.eq.0) then
      write(6,'("punktzahl auf",i7," festgesetzt")') n
      nn=n
      endif
      n=nn
      do 97 j=1,n
   97 x(j)=0.d0
      nin=0
      read(7,iform,end=2,err=7) (x(j),j=1,n)
      goto 3
    7 read(7,1,end=2,err=7) n,iform,dt,tmin,tsec,rest
      goto 3
    2 n=j+nin-1
      write(6,'("punkzahl durch datenende bestimmt")')
    3 close(7)
      write(6,'("daten sind eingelesen:",i7," punkte")') n
      return
      end

      subroutine output(name,text,prol,npro,n,dt,tmin,tsec,rest,x)
c  formatted output
      dimension x(n)
      character name*24,text*72,iform*20,rest*16
      character*40 prol(24)
      if(npro.gt.24) stop 24
      xmax=0.
      do 2 j=1,n
    2 xmax=max(xmax,abs(x(j)))
      nvor=log10(max(xmax,2.))+1.
      ndec=max(0,10-nvor)
        write(iform,3) ndec
    3   format('(5f13.',i1,')')
      write(6,5) name,n,iform
    5 format('File ',a,' wird geschrieben,',i7,' Punkte im Format ',a)
      open(8,file=name)
        write(8,'(a)') text
        write(8,1) n,iform,dt,tmin,tsec,rest
    1   format(i10,a20,f10.3,2f10.3,a16)
        write(8,iform) (x(j),j=1,n)
      close(8)
      write(6,'("fertig!")')
      return
      end
