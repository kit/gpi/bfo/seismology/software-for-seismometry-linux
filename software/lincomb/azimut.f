c  Drehung von Seismogramm-Komponenten

      program azimut
      use nas_system
      parameter(ndim=120000)
c
      dimension x(ndim),y(ndim),u(ndim),v(ndim)
      character text*72,rest*16
      character*24 nam0,nam1,nam2,nam3,nam4
      n=ndim

      write(6,1)
  1   format(' component rotation in the horizontal plane'/
     &' file names must conform to the scheme name.e, name.n'/
     &' Azimut from E to N (ccw, mathematical definition)'/
     &' T is also ccw from R'//
     &' call: AZIMUT filename_without_extension azimut_in_degrees')

      if(iargc().ne.2) stop 2
      call getarg(1,nam0)
      nam1=trim(nam0)//".e"
      nam2=trim(nam0)//'.n'
      nam3=trim(nam0)//'.r'
      nam4=trim(nam0)//'.t'
    
      call getarg(2,rest)
      read(rest,*) azig    

        call inpasc(nam1,text,n,dt,tmin,tsec,rest,x)

      if(n.eq.0.or.dt.eq.0.) then
      write(6,10)
   10 format('n or dt unspecified - check data header')
      stop
      endif

       call inpasc(nam2,text,n,dt,tmin,tsec,rest,y)

      if(n.eq.0.or.dt.eq.0.) then
      write(6,11)
   11 format('n or dt unspecified - check data header')
      stop
      endif

      azi=azig/45.*atan(1.)
      co=cos(azi)
      si=sin(azi)
      write(6,21) co, si
   21 format('coefficients E ',f6.3,'   N ',f6.3)

c Koordinatendrehung

      do 100 i=1,n
      u(i)=x(i)*co+y(i)*si
      v(i)=y(i)*co-x(i)*si
  100 continue

c Ausgabe

      write(text(1:10),'("R az=",i4,1x)') nint(azig)
      call output(nam3,text,n,dt,tmin,tsec,u)

      write(text(1:10),'("T az=",i4,1x)') nint(azig+90.)
      call output(nam4,text,n,dt,tmin,tsec,v)

      open(8,file='aziplot.par')
      write(8,12)
   12 format('1,4,24,16,0,0.8')
      write(8,'(a)') nam1
      write(8,'(a)') nam2
      write(8,'(a)') nam3
      write(8,'(a)') nam4
      write(8,'(a,f6.1,a)') ' Azimuth ',azig,' degrees from E to N (ccw,
     & mathematical definition)'
      close(8)
      write(6,'(a)') ' A plot-parameter file aziplot.par was generated'
      write(6,'(a)') ' type winplot aziplot.par to plot directional sei
     &smograms'

      stop
      end
c
      subroutine inpasc(name,text,n,dt,tmin,tsec,rest,x)
      dimension x(n)
      character iform*20,name*24,text*72,rest*16,pro*76
      open(7,file=name,status='old')
      read(7,'(a72)') text
   21 read(7,'(a76)') pro
      if(pro(1:1).ne.'%') goto 20
      goto 21
   20 read(pro,1) nn,iform,dt,tmin,tsec,rest
    1 format(i10,a20,3f10.3,a16)
      write(6,5) name
    5 format(' reading file ',a)
      if(n.lt.nn.or.nn.eq.0) then
      write(6,33) n
   33 format(' end of data after ',i7,' samples')
      nn=n
      endif
      n=nn
      read(7,iform) (x(j),j=1,n)
      return
      end
c
      subroutine output(name,text,n,dt,tmin,tsec,x)
      dimension x(n)
      character name*24,text*72,iform*20
      xmax=0.
      do 2 j=1,n
    2 xmax=max(xmax,abs(x(j)))
      nvor=log10(max(xmax,2.d0))+1.d0
      ndec=max(0,10-nvor)
      write(iform,'("(5f13.",i1,")")') ndec
      write(6,5) name
    5 format(" writing file ",a10)
      open(8,file=name)
      write(8,'(a72)') text
      write(8,1) n,iform,dt,tmin,tsec
    1 format(i10,a20,f10.6,2f10.3)
      write(8,iform) (x(j),j=1,n)
      close(8)
      return
      end


