#!/bin/sh
# this is <winplot.sh>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2015 by Thomas Forbriger (BFO Schiltach) 
# 
# Linux replacement for winplot
#
# ----
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version. 
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# ----
#
# REVISIONS and CHANGES 
#    16/06/2015   V1.0   Thomas Forbriger
#    19/07/2015   V1.1   first version fully implemented
# 
# ============================================================================
#
# indicate version
VERSION=2015-07-19
# 
# ============================================================================
# **** define usage functions                                             ****
# ============================================================================
#
usage() {
cat >&2 << HERE
usage: winplot.sh [-v|--verbose] [-D|--debug] [-d|--device dev] [--setsize]
                  [-X|--Xoption para] [-f|--parafile file]
   or: winplot.sh --help|-h
HERE
}
#
# ----------------------------------------------------------------------------
#
longusage() {
cat >&2 <<HERE

This shell script is a replacement for the DOS plot program winplot.g32.
It produces a multi-trace screen plot of ASCII data such as read or written by
my programs CALEX, DISPCAL, TILTCAL, SEIFE etc (“SEIFE format”). WINPLOT first
looks for a file 'plop’, 'plop.txt’, or 'winplot.par’, supposed to contain a
few numerical parameters and the names of the data files. winplot.sh uses
stuplox as provided in Seitosh (see Seitosh repository:
https://gitlab.kit.edu/kit/thomas.forbriger/seitosh).

The 'winplot.par’ file has the following format:

---- snip ----
1, 4, 24, 16, 1, 12000, 0.8
eing
ausg
synt
rest
---- snip ----

The seven numerical parameters in the first line mean:
  first parameter iavg: 
    =0, plot original data; 
    =1, remove average; 
    =2, use full width for nonnegative data. 
    Line width lw can be increased by adding 10*lw to iavg
  second parameter nt:
    number of traces to be plotted (i.e. number of file names)
  third parameter w: 
    approximate width of the plot in cm on a 12” screen
  fourth parameter h:
    approximate height of the plot in cm on a 12”  screen
    These numbers (w and h) are only used for Postscript plots.
  fifth parameter n1:
    index of first plotted sample, = 0 means first input sample
  sixth parameter n2: 
    index of last plotted sample, = 0 means last  input sample
  seventh parameter s: 
    scaling. 
    If positive: the peak amplitude of each trace is scaled to s times Full
      Scale. (I normally use 0.8).
    If negative: the amplitude ± s is scaled to Full Scale (the same scale for
      all traces)
    In both cases amplitudes beyond Full Scale are clipped.

Filenames – here eing, ausg, synt, rest - are valid to first blank, the rest
is considered as a comment.

A figure caption (one line) may be appended in a new line after the last file
name.

Zooming is possible by editing n1, n2 and s in the plop file, but is easier
with stuplox or stuploxx.

winplot.sh accepts additional parameters on the command line:

  -f|--parafile file    preferrably read plot parameters from file "file"
  -v|--verbose          report plot parameters to the terminal
  -D|--debug            make shell echo commands being executed
  -d||--device dev      select other output device than graphics screen
                        e.g. use '-d plotfile.ps/ps' to plot into Postscript
                        file 'plotfile.ps'
                        run 'stuplox -help' for a list of output devices
  --setsize             use width and height parameter read from parameter
                        file (affects only Postscript output); both parameters
                        are ignored otherwise
  -X|--Xoption para     pass additional option to stuplox; this option can be
                        used multiple times in order to pass several options;
                        if the option used in 'para' takes a parameter string
                        consisting of several words, they must by enclosed in
                        escaped quotes; e.g. to set the time axis label to
                        'time on 19.7.2015' you have to pass
                          --Xoption='-X "time on 19.7.2015"'
                        or
                          --Xoption="-X \"time on 19.7.2015\""
HERE
}
#
# ============================================================================
# execute
# ============================================================================
#
# echo usage information in any case
echo "winplot.sh (version $VERSION)"
usage
#
# read command line options
# -------------------------
TEMP=$(getopt -o vhX:d:Df: --long \
  help,verbose,Xoption:,device:,debug,parafile:,setsize \
  -n $(basename $0) -- "$@") || {
    echo >&2
    echo >&2 "ERROR: command line parameters are incorrect!"
    echo >&2 "aborting $0..."; exit 2
}
eval set -- "$TEMP"
#
# extract command line options
# ----------------------------
VERBOSE=0
SETSIZE=0
DEVICE=x11
PARAFILE=plop
XSTUPLOXOPT=""
while true; do
  case "$1" in
    --help|-h) longusage; exit 1;;
    --) shift; break;;
    -X|--Xoption)  XSTUPLOXOPT="${XSTUPLOXOPT} ${2}"; shift;;
    -v|--verbose)  VERBOSE=1;;
    --setsize)     SETSIZE=1;;
    -d|--device)   DEVICE="$2"; shift;;
    -f|--parafile) PARAFILE="$2"; shift;;
    -D|--debug) set -x ;;
    *) echo >&2 "ERROR: option $1 unprocessed!";
       echo >&2 "aborting $0..."; exit 2;;
  esac
  shift
done

#
# find parameter file
# -------------------
if test ! -r $PARAFILE
then
  PARAFILE=plop
fi
if test ! -r $PARAFILE
then
  PARAFILE=plop.txt
fi
if test ! -r $PARAFILE
then
  PARAFILE=winplot.par
fi
if test ! -r $PARAFILE
then
  echo >&2 "Missing parameter file (plop, plop.txt, or winplot.par)"
  echo >&2 "aborting..."
  exit 2
fi
echo using parameter file $PARAFILE

# parse parameter file
# -------------------
{
  read -d ',' Piavg
  read -d ',' Pnt
  read -d ',' Pw
  read -d ',' Ph
  read -d ',' Pn1
  read -d ',' Pn2
  read Pscale
  declare -a filenames
  mapfile -t -O 0 filenames
} < $PARAFILE

if test $VERBOSE -gt 0
then
  echo parameters read from $PARAFILE
  echo '  iavg =' $Piavg
  echo '  nt   =' $Pnt
  echo '  w    =' $Pw
  echo '  h    =' $Ph
  echo '  n1   =' $Pn1
  echo '  n2   =' $Pn2
  echo '  s    =' $Pscale
fi

# evaluate parameters
# ===================

# accumulate stuplox parameters 
STUPLOXOPT="-ty seife -V -t -a -i -s x"

let avgmode=$Piavg%10
let lw=1+$Piavg/10

if test $VERBOSE -gt 0
then
  case $avgmode in
    0) echo 'do not remove average' ;;
    1) echo 'remove average' ;;
    2) echo ' use full width for nonnegative data' ;;
    *) echo 'iavg =' $Piavg '; mode unkown!!'
  esac
  echo 'line width =' $lw
fi

if test $VERBOSE -gt 0
then
  case $avgmode in
    1) STUPLOXOPT="$STUPLOXOPT -ra" ;;
    2) STUPLOXOPT="$STUPLOXOPT -py" ;;
  esac
fi

if test $Pn1 -gt 0
then
  STUPLOXOPT="$STUPLOXOPT -n1 $Pn1"
fi

if test $Pn1 -gt 0
then
  STUPLOXOPT="$STUPLOXOPT -n2 $Pn2"
fi

# height and width can be specified in 1/1000 inch in PGPLOT
# swap width and height, since PGPLOT plots in landscape mode per default
#
# 1 inch = 2.54 cm
# WPGPLOT= Pw * 1000 / 2.54
if test $SETSIZE -gt 0
then
  export PGPLOT_PS_HEIGHT=$(echo "scale=0; $Pw * 1000 / 2.54" | bc)
  export PGPLOT_PS_WIDTH=$(echo "scale=0; $Ph * 1000 / 2.54" | bc)
  export PGPLOT_PS_HOFFSET=$(echo "scale=0; (29.7 - $Pw) * 500 / 2.54" | bc)
  export PGPLOT_PS_VOFFSET=$(echo "scale=0; (21. - $Ph) * 500 / 2.54" | bc)
fi

OPTIONS=""
if test $(echo "$Pscale < 0" | bc) -eq 1
then
  STUPLOXOPT="${STUPLOXOPT}"" -y $Pscale,$(echo $Pscale '*' '-1' | bc)"
else
  STUPLOXOPT="${STUPLOXOPT}"" -Y $Pscale"
fi

STUPLOXOPT="${STUPLOXOPT}"" -l 2,1,$lw"

# evaluate additional lines in parameter file
# ===========================================
ntitle=0
let nlines=${#filenames[@]}
if test $nlines -gt $Pnt
then
  let ntitle=$Pnt
  if test $VERBOSE -gt 0
  then
    echo 'title =' ${filenames[$ntitle]}
  fi
fi

if test $ntitle -gt 0
then
  STUPLOXOPT="${STUPLOXOPT}"" -c Afim -A \"${filenames[$ntitle]}\""
else
  STUPLOXOPT="${STUPLOXOPT} -c fim"
fi

# read $Pnt file names and convert DOS path names to Unix path names
declare -a datafiles
for ((i=0; i<$Pnt; i++))
do
  datafiles[$i]=$(echo ${filenames[$i]//\\//} | cut -f 1 -d ' ')
  let n=$i+1
  if test $VERBOSE -gt 0
  then
    echo file '#'$n: ${datafiles[$i]}
  fi
done

# This shell script makes use of stuplox from Seitosh
# https://gitlab.kit.edu/kit/thomas.forbriger/seitosh
# Check whether stuplox executable is available
STUPLOX=$(type -p stuplox)
if test -z "$STUPLOX"
then
  echo 'command stuplox is missing!'
  echo install plot program from Seitosh first
  echo Seitosh repository: https://gitlab.kit.edu/kit/thomas.forbriger/seitosh
  echo stuplox: https://gitlab.kit.edu/kit/thomas.forbriger/seitosh/-/blob/master/src/ts/plot/stuplo.f
  exit 2
else
  if test $VERBOSE -gt 0
  then
    echo command $STUPLOX will be used for plotting
  fi
fi

# report parameters
if test $VERBOSE -gt 0
then
  echo stuplox parameters set by winplot.sh
  echo "  ""${STUPLOXOPT}"
  echo additional stuplox parameters set on command line
  echo "  ""${XSTUPLOXOPT}"
fi

# run plot command
eval $STUPLOX -d "${DEVICE}" "${STUPLOXOPT}" "${XSTUPLOXOPT}" ${datafiles[@]}
#
# ----- END OF winplot.sh ----- 
