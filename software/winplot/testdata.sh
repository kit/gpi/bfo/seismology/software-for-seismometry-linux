#!/bin/sh
# this is <testdata.sh>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2015 by Thomas Forbriger (BFO Schiltach) 
# 
# a shell script to create test data for winplot.sh
#
# the shell script makes use of siggenx from Seitosh:
# https://gitlab.kit.edu/kit/thomas.forbriger/seitosh/-/blob/master/src/synt/misc/siggen.f
# 
# REVISIONS and CHANGES 
#    18/07/2015   V1.0   Thomas Forbriger
# 
# ============================================================================
#
siggenx 3 data1.sfe -T 600. -d 1. -ot seife -v -o \
  -a 70e2 -Ta 50. -Te 200. 
siggenx 6 data2.sfe -T 600. -d 1. -ot seife -v -o \
  -a -90. -Ta 100. -Tm 150. -Te 600. 
cat > plop <<HERE
20,  2, 24.0, 16.0,  1, 0, 0.8
data1.sfe
data2.sfe
example data for winplot.sh
HERE
./winplot.sh
# ----- END OF testdata.sh ----- 
