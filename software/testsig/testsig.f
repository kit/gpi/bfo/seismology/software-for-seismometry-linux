      program testsig
c
c  generate a test signal. Up to 60 sine waves can be summed
c  and white noise added.
      parameter(ndim=1200000)
      
      real*4 x(ndim)
      character name*10,par*20,text*40,noisfi*10
      character*50 prolog(64)
      character*1 ja
      data par/'                    '/
      text=' Generate a test signal in SEIFE format '
    6 write(6,'(a/" Enter a filename: ")') text
      read(5,'(a)') name
      write(6,'(" How many samples? ")')
      read(5,*) n
      if(n.gt.ndim) stop "n > ndim - stop"
      write(6,'(" Sampling interval [s]? ")')
      read(5,*) dt
      zpi=8.*atan(1.)
      npro=0
      do 4 j=1,n
    4 x(j)=0.
    1 write(6,'(" Period of sinewave [s]? [0=no more]: " )')
      read(5,*) per
      if(per.eq.0.) goto 2
      write(6,'(" Amplitude? ")')
      read(5,*) amp
      write(6,'(" Phase in degrees? ")')
      read(5,*) phi
      npro=npro+1
      write(prolog(npro),5) per,amp,phi
    5 format("%    per=",f10.3," amp=",f10.3," phi=", f10.3)
      phi=phi/360.*zpi
      omega=zpi/per
      do 3 j=1,n
      t=(j-1)*dt
    3 x(j)=x(j)+amp*cos(omega*t+phi)
      if(npro.le.60) goto 1

    2 write(6,'(" Add white noise with amplitude:")')
      read(5,*) amp
      if(amp.gt.0.) then
      do 18 j=1,n
      rn=-6.
      do k=1,12
      call random_number(y)
      rn=rn+y
      enddo
   18 x(j)=x(j)+amp*rn
      npro=npro+1
      write(prolog(npro),9) amp
    9 format("%    white noise, amp=",f10.3)
      endif
    7 write(6,'(" output format? [<cr>=(8f9.3)]: ")')
      read(5,'(a20)') par
      call output(name,prolog,npro,par,text,n,dt,x)
      write(6,'(" Done. Another file? [y/n]")')
      read(5,'(a)') ja
      if(ja.eq.'y') goto 6
      stop
      end

      subroutine output(name,prolog,npro,par,text,n,dt,x)
      real*4 x(n),z(16)
      character name*10,text*40,par*20,iform*20
      character*50 prolog(npro)
      iform='(8f9.3)'
      if(par(1:1).eq.'(') read(par,'(a20)') iform
      write(6,5) name
    5 format(" signal will be stored as file ",a10)
      open(8,file=name)
      write(8,'(a)') text
      do j=1,npro
      write(8,'(a)') prolog(j)
      enddo
      write(8,1) n,iform,dt
    1 format(i10,a20,3f10.3,2i5,a10)
      write(8,iform) (x(j),j=1,n)
      close(8)
      write(*,*) ' done!'
      return
      end
      
