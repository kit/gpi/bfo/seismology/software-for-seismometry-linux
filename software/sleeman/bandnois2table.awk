#!/bin/gawk -f
# this is <bandnois2table.awk>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2016 by Thomas Forbriger (BFO Schiltach) 
# 
# Convert bandnois.out to a raw ascii table (suitable for gnuplot)
#
# Run program as
#
#   bandnois2table.awk bandnois.out
#
# The initial frequency factor can be adjusted by
#
#   bandnois2table.awk -v ffac=1000. bandnois.out
#
# The Program bandnoise outputs the center frequency in the first column,
# where units are adjusted to the numerical value. This means that values less
# than 10 mHz are output in units of mHz while larger values are output in Hz.
# The current script detects changes in frequency units and ajusts them such
# that each value is presented in the same units. If the units of the first
# line are not Hz, an initial factor different from 1 can be set on the
# command line.
# 
# REVISIONS and CHANGES 
#    14/09/2016   V1.0   Thomas Forbriger
# 
# ============================================================================
#
BEGIN { 
  if (ffac == 0) { ffac=1.; }
  fwfac=ffac;
  hot=0;
  fprev=-1;
  fwprev=-1;
}
/^ *band/ { hot=1; 
  print "# " $0;
  getline; 
  print "# " $0;
  getline; 
  getline; }
{ 
  if (hot) { 
    fin=$1;
    if (fprev > 0) {
      if (fprev < fin) {
        ffac /= 1000.;
      }
    }
    fprev=fin;
    f=fin*ffac;
    fwin=$2;
    if (fwprev > 0) {
      if (fwprev < fwin) {
        fwfac /= 1000.;
      }
    }
    fwprev=fwin;
    fw=fwin*fwfac;
    printf "%9.6g %9.6g %s %s %s %s %s\n", f, fw, $3, $4, $5, $6, $7; 
  } 
}
# ----- END OF bandnois2table.awk ----- 
