#!/bin/gawk -f
# this is <tridb2table.awk>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2016 by Thomas Forbriger (BFO Schiltach) 
# 
# Convert tri2db.out to a raw ascii table (suitable for gnuplot)
#
# Run program as
#
#   tridb2table.awk tri2db.out
#
# The initial frequency factor can be adjusted by
#
#   tridb2table.awk -v ffac=1000. tri2db.out
#
# The Program bandnoise outputs the center frequency in the first column,
# where units are adjusted to the numerical value. This means that values less
# than 10 mHz are output in units of mHz while larger values are output in Hz.
# The current script detects changes in frequency units and ajusts them such
# that each value is presented in the same units. If the units of the first
# line are not Hz, an initial factor different from 1 can be set on the
# command line.
# 
# REVISIONS and CHANGES 
#    14/09/2016   V1.0   Thomas Forbriger
# 
# ============================================================================
#
BEGIN { 
  if (ffac == 0) { ffac=1.; }
  hot=0;
  fprev=-1;
}
/^ *band/ { hot=1; 
  print "# " $0;
  getline; 
  getline; }
{ 
  if (hot) { 
    fin=$1;
    if (fprev > 0) {
      if (fprev < fin) {
        ffac /= 1000.;
      }
    }
    fprev=fin;
    f=fin*ffac;
    printf "%9.6g %s %s %s %s %s %s %s\n", \
           f, $2, $3, $4, $5, $6, $7, $8; 
  } 
}
# ----- END OF tridb2table.awk ----- 
