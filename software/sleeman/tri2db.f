c 
c - 13/09/2016    T.F.: adjust code to satisfy gfortran
c 
      program tri2db
      parameter(nd=1001)
      dimension bandm(nd),amp(nd),rn1(nd),rn2(nd),rn3(nd)
      dimension bandm2(nd),bandbr(nd),amp1(nd)
      dimension rld(nd),rldm(nd),dif(nd),t2(nd),t3(nd)
      
      write(*,*) 'TRI2DB reads the output files of TRICROSP and'
      write(*,*) 'BANDNOIS and converts the signal levels into'
      write(*,*) 'decibels re.1 m^2 / s^3. Before calling TRI2DB,'
      write(*,*) 'Run TRICROSP with data files 1, 2 and 3 and '
      write(*,*) 'BANDNOIS with data file 1 (the reference signal)'
      
      write(*,*)
      write(*,*) 'opening file tricrosp.out'
      open(7,file='tricrosp.out')
      do i=1,17
        read(7,*)
      enddo
      do i=1,nd
        read(7,*,end=101)  bandm(i),amp(i),t2(i),t3(i),rn1(i),rn2(i),
     &  rn3(i)
c        write(*,100) bandm(i),amp(i),t2(i),t3(i),rn1(i),rn2(i),rn3(i)
c  100 format(7f9.3)
      enddo
  101 close(7)
      n=i-1
      write(*,*) 'opening file bandnois.out'
      open(7,file='bandnois.out')
      do i=1,12
        read(7,*)
      enddo
      do i=1,n
        read(7,*,end=102) bandm2(i),bandbr(i),amp1(i),wrld,rld(i),
     &  rldm(i),dif(i)
c        write(*,103) bandm2(i),bandbr(i),amp1(i),wrld,rld(i),
c     &  rldm(i),dif(i)
  103 format(2f8.3,5f8.1)
      if(bandm(i).ne.bandm2(i)) then
        write(*,*) 'frequency bands are inconsistent between tricrosp.ou
     &t and bandnois.out'
        write(*,*) 'last line read from bandnois.out'
        write(*,103) bandm2(i),bandbr(i),amp1(i),wrld,rld(i),
     &  rldm(i),dif(i)
        stop
      endif
      enddo
  102 close(7)
      n=i-1
      
      open(8,file='tri2db.out')
      write(6,*)
      write(6,*) 'tricrosp.out translated into decibels re 1 m^4 / s^3'
      write(6,*) 'using bandnois.out for the transfer fct. of system 1'
      write(6,*)
      write(6,110)
      write(8,*) 'tricrosp.out translated into decibels re 1 m^4 / s^3'
      write(8,*) 'using bandnois.out for the transfer fct. of system 1'
      write(8,*)
      write(8,110)
  110 format('   bandctr    NLNM   n1abs   n1rel   n2abs   n2rel   n3abs   
     &   n3rel')
      do i=1,n
        refamp=sqrt(rn1(i)**2+amp(i)**2)
        rld1=rld(i)+20.*alog10(rn1(i)/refamp)
        dif1=rld1-rldm(i)
        rld2=rld(i)+20.*alog10(rn2(i)/refamp/t2(i))
        dif2=rld2-rldm(i)
        rld3=rld(i)+20.*alog10(rn3(i)/refamp/t3(i))
        dif3=rld3-rldm(i)        
        write(6,111) bandm(i),rldm(i),rld1,dif1,rld2,dif2,rld3,dif3
        write(8,111) bandm(i),rldm(i),rld1,dif1,rld2,dif2,rld3,dif3
  111   format(f10.3,7f8.1)
      enddo
      write(6,*)
      write(6,*) 'results were written into file tri2db.out'
      stop
      end
   
