How to install Software for Seismometry (Linux)
===============================================

Quick guide
-----------
Compilation for some of the programs is supported by a Makefile.
Just run

    make all

Detailed instructions
---------------------
The main [Makefile](Makefile) delegates compilation to 
[software/Makefile](software/Makefile), where rules for the compilation and
installation of binary executables and additional shell scripts are setup.
The rules make use of some environment variables in order to find the location
where files are to be installed and to identify local compilers.
They are a subset of the variables used by
[Seitosh](https://gitlab.kit.edu/kit/thomas.forbriger/seitosh).
Defaults are set in [Makefile](Makefile) and can be overwritten by variables
exported from the shell's environment.

### make variables
Currently the Makefile sets defaults for three variables.

#### `LOCBINDIR`
Defines the destination path for the installation of binary executables.
The default is directory `bin` in the projects root directory.

#### `LOCSCRIPTDIR`
Defines the destination path for the installation of shell scripts.
The default is directory `bin` in the projects root directory.

#### `FC`
Defines the Fortran compiler to be used.
The default is `gfortran`.
