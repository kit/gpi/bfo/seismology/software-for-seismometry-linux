Software for Seismometry (Linux)
================================

Purpose
-------
This is a fork of 
[software for seismometry](https://gitlab.kit.edu/kit/gpi/bfo/seismology/software-for-seismometry).
This copy is complemented by code modifications for application of the
programs on a Linux platform. The repository contains software from the
software subdirectory at http://www.software-for-seismometry.de/.

Installation
------------
Compilation for some of the programs is supported by a Makefile.
Just run

    make all

Further details for compilation and installation are given in 
[INSTALL.md](INSTALL.md).

Status
------
See [software/README](software/README) for current status of maintained
programs.
See [CHANGELOG](CHANGELOG) for recent modifications (bug fixes, new features,
etc).

Layout
------
In order to make software history transparent and to support future vendor
updates, the source code is organized in a hierarchy of repository branches.
The order of ancestry is `vendor` -> `unix` -> `master`, where `vendor` refers
to the most recent import from the 
[vendor repository](https://gitlab.kit.edu/kit/gpi/bfo/seismology/software-for-seismometry).
The most recent additions for Linux users are available on branch `master`.

A second line of ancestry, where branch names end on `-with-examples`,
provides the same contents but with toy examples and example data added.

### branches
#### published branches
These branches contain the full Linux version including specific extensions
and Makefile  
`master`:                Primary branch on which the software together with
                         Linux specific extensions and Makefiles is 
                         presented.   
`master-with-examples`:  Contents of `unix-with-examples` with additions as
                         present on branch master

#### preparation branches
These branches represent the contents of the original repository after
conversion to UNIX file encoding and path formats  
`unix`:                  Contents of `vendor` after conversion to utf8 and
                         UNIX line endings.   
`unix-with-examples`:    Contents of `vendor-with-examples` after conversion
                         to utf8 and UNIX line endings; recent modifications
                         of master are provided here too.   
`workbench`:             Separate branch providing tools for vendor imports   

#### vendor branches
These branches represent the contents of the original repository  
`vendor-preview`:        Preview complete contents of
                         http://www.software-for-seismometry.de/
                         just before next vendor snapshot
                         (copied from upstream)  
`vendor`:                Vendor import branch (just code, no example data) -
                         (copied from upstream)    
`vendor-with-examples`:  Vendor import with data file examples
                         (copied from upstream)               

#### intermediate branches
Expect these branches to be moved, removed, or otherwise altered by a forced
update.

`toy`:                   Contents of `master-with-examples` in preparation of
                         toy examples.   
`im_*`:                  Any branch with a name starting with 'im_*' is used
                         for intermediate commits.   

### merge hierarchy
Branch `unix` occasionally will be merged into `master`.
Branch `master` occasionally will be merged into `master-with-examples`.
Branch `vendor` occasionally will be merged into `unix`.
Branch `vendor-with-examples` never shall be merged into `vendor` such that
users can easily discard the example data files from their clone, if they want
to.
Branch `unix-with-examples` similarly shall not be merge back into `unix` or
`master` or similar.
The same is true vor 'master-with-examples'.
Any branch '*-with-examples' never should be merged into a branch which does
not have '-with-examples' in its name.

Use branch `toy` to prepare toy examples.
Useful new code on `toy` should be transferred to `master` by appropriate
rebase or cherry-pick.

The branch `workbench` does not contain files from
http://www.software-for-seismometry.de/
and will always be kept separate (no merges).

Caretaker
---------
The git repository is maintained by  
Thomas Forbriger   
email: Thomas.Forbriger@kit.edu   
Black Forest Observatory (BFO), Heubach 206, 77709 Wolfach, Germany,   
Geophysical Institute, Karlsruhe Institute of Technology (KIT)   
https://www.gpi.kit.edu/english/61.php
