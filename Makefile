# this is <Makefile>
# ----------------------------------------------------------------------------
# 
# Copyright (c) 2015 by Thomas Forbriger (BFO Schiltach) 
# 
# support compilation of Fortran source code
# 
# REVISIONS and CHANGES 
#    15/06/2015   V1.0   Thomas Forbriger
# 
# ============================================================================
#

all:
	cd software; $(MAKE) all

EDITFORTRAN=$(patsubst ./%,%,$(shell find . -name \*.f))
EDITBASH=$(patsubst ./%,%,$(shell find . -name \*.sh))
EDITAWK=$(patsubst ./%,%,$(shell find . -name \*.awk))
EDITMAKE=$(patsubst ./%,%,$(shell find . -name Makefile)) \
	   $(filter-out %.bak,$(patsubst ./%,%,$(shell find . -name makefile\*)))
EDITREADME=$(patsubst ./%,%,$(shell find . -iname REA\*ME -o -name \*.md))

EDITCODE=$(EDITFORTRAN) $(EDITBASH) $(EDITAWK) 

flist: Makefile README.md COPYING CHANGELOG \
       $(EDITCODE) $(EDITMAKE) $(EDITREADME)
	echo $(filter-out $(EDITCODE) $(EDITMAKE),$^) \
	  | tr ' ' '\n' | sort > $@
	echo '----' >> $@
	echo $(EDITMAKE) | tr ' ' '\n' | sort >> $@
	echo '----' >> $@
	echo $(EDITCODE) | tr ' ' '\n' | sort >> $@

.PHONY: edit
edit: flist; vim $<

.PHONY: clean
clean: ; 
	-find . -name \*.bak | xargs --no-run-if-empty /bin/rm -v
	-find . -name \*.o | xargs --no-run-if-empty /bin/rm -v
	-/bin/rm -vf flist

# set Seitosh variables, if not yet set
ifndef LOCBINDIR
  LOCBINDIR=$(shell pwd)/bin
  export LOCBINDIR
endif
ifndef LOCSCRIPTDIR
  LOCSCRIPTDIR=$(shell pwd)/bin
  export LOCSCRIPTDIR
endif
ifeq ($(origin FC),default)
  FC=gfortran
  export FC
endif

# ----- END OF Makefile ----- 
